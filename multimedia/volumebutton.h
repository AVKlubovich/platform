#pragma once

class VolumeButton : public QToolButton
{
    Q_OBJECT
    Q_PROPERTY(int volume READ volume WRITE setVolume NOTIFY volumeChanged)

public:
    VolumeButton(QWidget *parent = nullptr);

    int volume() const;

public slots:
    void increaseVolume();
    void descreaseVolume();
    void setVolume(int volume);
    void stylize();

signals:
    void volumeChanged(int volume);

private:
    QMenu *menu;
    QLabel *label;
    QSlider *slider;
};
