﻿#pragma once


namespace network
{

    class BodyConverter
    {
    public:
        BodyConverter() = default;
        virtual ~BodyConverter() = default;

        virtual QVariant convert(const QByteArray& source) const = 0;
        virtual QByteArray convert(const QVariant& source) const = 0;

    public:
        static const QMap<QByteArray, BodyConverter *> converters;
    };

}
