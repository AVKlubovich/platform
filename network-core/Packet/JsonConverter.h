﻿#pragma once


#include "BodyConverter.h"


namespace network
{
    
    class JsonConverter
        : public BodyConverter
    {
    public:
        JsonConverter() = default;
        ~JsonConverter() = default;

        QVariant convert(const QByteArray& source) const override;
        QByteArray convert(const QVariant& source) const override;
    };

}
