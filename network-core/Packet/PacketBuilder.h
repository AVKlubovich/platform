﻿#pragma once

#include "../Headers/Headers.h"
#include "Packet.h"


namespace network
{

    class PacketBuilder
    {
    public:
        PacketBuilder() = default;
        ~PacketBuilder() = default;

        void setHeaders( const Headers& headers );
        void setBody( const QVariant& body );

        const Headers& headers() const;
        const QByteArray& body() const;

    private:
        Headers _headers;
        QByteArray _body;
    };

}
