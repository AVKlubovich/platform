﻿#include "Common.h"
#include "JsonConverter.h"


using namespace network;

QVariant JsonConverter::convert(const QByteArray& source) const
{
    const auto json = QJsonDocument::fromJson( source );
    const auto variant = json.toVariant();

    return variant;
}

QByteArray JsonConverter::convert(const QVariant& source) const
{
    const auto json = QJsonDocument::fromVariant( source );
    const auto data = json.toJson( QJsonDocument::Compact );

    // MAP: remake logger
    // qDebug() << "output:" << data;

    return data;
}
