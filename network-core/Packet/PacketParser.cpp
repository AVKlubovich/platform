﻿#include "Common.h"
#include "PacketParser.h"

#include "Packet.h"
#include "BodyConverter.h"


using namespace network;

void PacketParser::setHeaders(const Headers& headers)
{
    _headers = headers;
}

void PacketParser::setBody(const QByteArray& body)
{
    const auto contentType = _headers.header("CONTENT_TYPE");
    auto converter = BodyConverter::converters.value(contentType, nullptr);
    if (converter == nullptr)
    {
        Q_ASSERT(false);
        return;
    }

    _body = converter->convert(body);
}

Packet PacketParser::packet() const
{
    return Packet(_headers, _body);
}
