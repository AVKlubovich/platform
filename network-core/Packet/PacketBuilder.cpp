﻿#include "Common.h"
#include "PacketBuilder.h"

#include "Packet.h"
#include "BodyConverter.h"


namespace network
{

    void PacketBuilder::setHeaders( const Headers& headers )
    {
        _headers = headers;
    }

    void PacketBuilder::setBody( const QVariant& body )
    {
        const auto contentType = _headers.header( "CONTENT_TYPE" );
        auto converter = BodyConverter::converters.value( contentType, nullptr );
        if ( converter == nullptr )
        {
            Q_ASSERT( false );
            return;
        }

        _body = converter->convert( body );
        _headers.setHeader( "CONTENT_LENGTH", QByteArray::number( _body.size() ) );
    }

    const Headers& PacketBuilder::headers() const
    {
        return _headers;
    }

    const QByteArray& PacketBuilder::body() const
    {
        return _body;
    }

}
