﻿#include "Common.h"
#include "Packet.h"


namespace network
{

    Packet::Packet( const Headers& headers, const QVariant& body )
        : _headers( headers )
        , _body( body )
    {
    }

    const Headers& Packet::headers() const
    {
        return _headers;
    }

    const QVariant& Packet::body() const
    {
        return _body;
    }

}
