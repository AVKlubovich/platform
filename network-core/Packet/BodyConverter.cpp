﻿#include "Common.h"
#include "BodyConverter.h"

#include "JsonConverter.h"


namespace
{

    network::JsonConverter jsonConverter;

}


namespace network
{

    const QMap<QByteArray, BodyConverter *> BodyConverter::converters =
    {
        { "application/json", &jsonConverter },
    };

}
