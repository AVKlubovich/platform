﻿#pragma once

#include "../Headers/Headers.h"


namespace network
{

    class Packet
    {
    public:
        explicit Packet( const Headers& headers = Headers(), const QVariant& body = QVariant() );
        ~Packet() = default;

        const Headers& headers() const;
        const QVariant& body() const;
        void clearBody()
        {
            _body = QVariant();
        }

    private:
        Headers _headers;
        QVariant _body;
    };

}
