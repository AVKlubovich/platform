﻿#pragma once

#include "../Headers/Headers.h"
#include "Packet.h"


namespace network
{

    class PacketParser
    {
    public:
        PacketParser() = default;
        ~PacketParser() = default;

        void setHeaders(const Headers& headers);
        void setBody(const QByteArray& body);

        Packet packet() const;

    private:
        Headers _headers;
        QVariant _body;
    };

}
