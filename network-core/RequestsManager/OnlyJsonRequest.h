﻿#pragma once

#include "Request.h"


namespace network
{

    class OnlyJsonRequest : public Request
    {
    public:
        OnlyJsonRequest(const QString& json);
        virtual ~OnlyJsonRequest();

        QVariant toVariant() const override;

    protected:
        QVariant bodyToVariant() const override;
        void bodyFromVariant(const QVariant& body) override;

    private:
        QString _json;
    };

    typedef QSharedPointer<OnlyJsonRequest> OnlyJsonRequestShp;

}
