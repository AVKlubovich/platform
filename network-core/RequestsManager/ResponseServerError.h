﻿#pragma once

#include "Response.h"


namespace network
{

    class ResponseServerError : public Response
    {
    public:
        ResponseServerError();
        ResponseServerError(const QString & errorMessage, QSharedPointer<Request> request = QSharedPointer<Request>());

        QString getErrorMessage() { return _errorMessage; }

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        QString _errorMessage;
    };
}
