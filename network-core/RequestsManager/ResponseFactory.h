﻿#pragma once


namespace network
{

    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class ResponseFactory
    {
    protected:
        ResponseFactory(bool initialize = false);
    public:
        virtual ~ResponseFactory() = default;

    public:
        static ResponseShp createResponseObject(const QVariant &data);

        virtual ResponseShp createSpecializedResponseObject();

    private:
        typedef QMap<QString, QSharedPointer<ResponseFactory>> ResponseFactoryMap;
        ResponseFactoryMap _responseFactoryMap;

        static const QString DEFAULT_TYPE;
    };


    template <class T>
    class ResponseFactoryInstance : public ResponseFactory
    {
    public:
        virtual ~ResponseFactoryInstance() override = default;

    protected:
        ResponseShp createSpecializedResponseObject() override
        {
            return ResponseShp(new T);
        }
    };

}
