﻿#pragma once

#include "Request.h"
#include "Response.h"
#include "ResponseServerError.h"

#include "CallbackHolder.h"


namespace network
{

    class ICallbackHolder;
    typedef QSharedPointer<ICallbackHolder> CallbackHolderShp;

    class RequestsManager : public QObject
    {
        Q_OBJECT

    public:
        RequestsManager(QObject *parent = nullptr);
        ~RequestsManager() = default;

        bool init();

        void sendRequest(QVariant inData);
        void sendRequest(const Request &request);

        template <typename T, typename M>
        void execute(const RequestShp& request, T* obj, M method)
        {
            typedef CallbackHolder<T, M> Holder;
            typedef QSharedPointer<Holder> HolderShp;
            const auto requestData = qMakePair(request, HolderShp::create(obj, method));
//            _requestHolders.insert(request->uuid(), HolderShp::create(obj, method));
            _requestHolders.insert(request->uuid(), requestData);

            sendRequest(request);
        }

    private:
        void sendRequest(const QSharedPointer<Request>& commandRequest);

    signals:
        void responseReady(QVariant outData);
        void responseReady(QSharedPointer<network::Response>);

        void requestFailed();

    private slots:
        void onResponse();
        void onResponseH(const QByteArray& uuid);

    private:
        QNetworkAccessManager _netManager;
        QMap<QUuid, QPair<RequestShp, CallbackHolderShp>> _requestHolders;
    };

    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

}
