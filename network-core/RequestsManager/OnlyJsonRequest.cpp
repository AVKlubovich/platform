﻿#include "Common.h"
#include "OnlyJsonRequest.h"


using namespace network;

OnlyJsonRequest::OnlyJsonRequest(const QString& json)
    : Request("")
    , _json(json)
{
}

OnlyJsonRequest::~OnlyJsonRequest()
{
}

QVariant OnlyJsonRequest::toVariant() const
{
    QJsonDocument doc = QJsonDocument::fromJson(_json.toUtf8());
    auto obj = doc.object();

    return QVariant::fromValue(obj.toVariantMap());
}

QVariant OnlyJsonRequest::bodyToVariant() const
{
    return QVariant();
}

void OnlyJsonRequest::bodyFromVariant(const QVariant& msg)
{
}
