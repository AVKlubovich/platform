﻿#include "Common.h"
#include "ResponseGeneric.h"

#include "Request.h"


using namespace network;

ResponseGeneric::ResponseGeneric()
    : Response()
    , _status(StatusUnknown)
{
}

ResponseGeneric::ResponseGeneric(QSharedPointer<Request> request, ResponseGeneric::Status status)
    : Response("generic", request)
    , _status(status)
{
}

QVariant ResponseGeneric::bodyToVariant() const
{
    QVariantMap bodyMap;
    bodyMap["status"] = _status;
    bodyMap["message"] = _message;
    bodyMap["data"] = _data;

    return bodyMap;
}

void ResponseGeneric::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();
    QVariantMap::iterator statusIter = bodyMap.find("status");
    if (statusIter != bodyMap.end())
        _status = (Status)statusIter.value().toInt();
    else
        _status = StatusUnknown;

    _message = bodyMap["message"].toString();
    _data = bodyMap["data"];
}
