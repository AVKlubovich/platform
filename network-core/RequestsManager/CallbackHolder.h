﻿#pragma once


namespace network
{

    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class ICallbackHolder
    {
    public:
        virtual ~ICallbackHolder() {}
        virtual void Call(ResponseShp response) = 0;
    };

    template<typename T, typename M>
    class CallbackHolder : public ICallbackHolder
    {
    };

    template<typename T>
    class CallbackHolder<T, void (T::*)(ResponseShp)> : public ICallbackHolder
    {
        typedef void (T::*M)(ResponseShp);

    public:
        CallbackHolder(T* object, M method)
            : _object(object)
            , _method(method)
        {
        }

        void Call(QSharedPointer<Response> response)
        {
            if (_object != nullptr)
            {
                (_object->*_method)(response);
            }
        }

    private:
        T* _object;
        M _method;
    };

}
