﻿#pragma once


namespace network
{

    class Request
    {
    protected:
        Request(const QString & type);

    public:
        virtual ~Request() {}

        QUuid uuid() const;
        QString type() const;

        virtual QVariant toVariant() const;
        virtual void fromVariant(const QVariant & msg) final;

    protected:
        virtual QVariant bodyToVariant() const = 0;
        virtual void bodyFromVariant(const QVariant & body) = 0;

    private:
        QString _type;
        QUuid _uuid;
    };

    typedef QSharedPointer<Request> RequestShp;

}
