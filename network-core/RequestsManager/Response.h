﻿#pragma once


namespace network
{

    class Request;

    class Response
    {
    protected:
        Response() = default;
        Response(const QString & type, QSharedPointer<Request> request);

    public:
        virtual ~Response() {}

        QUuid uuid() const;

        QVariant toVariant();
        void fromVariant(const QVariant & msg);

        //This function should be implemented by derived class to fill in bory part of the response structure.
        virtual QVariant bodyToVariant() const = 0;
        virtual void bodyFromVariant(const QVariant & body) = 0;

    private:
        QString _type;
        QSharedPointer<Request> _request;
    };

    typedef QSharedPointer<Response> ResponseShp;

}
