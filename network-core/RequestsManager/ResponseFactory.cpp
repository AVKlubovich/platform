﻿#include "Common.h"
#include "ResponseFactory.h"

#include "Response.h"
#include "DefaultResponse.h"
#include "Users/ResponseLogin.h"
#include "ResponseGeneric.h"
#include "ResponseServerError.h"


using namespace network;

const QString ResponseFactory::DEFAULT_TYPE = "default_response";

ResponseFactory::ResponseFactory(bool initialize)
{
    if (!initialize)
        return;

    // NOTE: general responses
    _responseFactoryMap["ServerError"].reset(new ResponseFactoryInstance<ResponseServerError>);
    _responseFactoryMap["generic"].reset(new ResponseFactoryInstance<ResponseGeneric>);

    // NOTE: user account
    _responseFactoryMap["login"].reset(new ResponseFactoryInstance<ResponseLogin>);

    // NOTE: QATransport
    _responseFactoryMap["get_user_list"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["delete_user"].reset(new ResponseFactoryInstance<DefaultResponse>);

    _responseFactoryMap["get_new_data"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["fetch_new_data"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["taken_info_operation"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_select_complaints"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_select_reviews"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_select_clients"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["update_complaint"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["insert_complaint"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["change_in_status"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["save_permissions"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_new_db_data"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["worker_statistic"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_comments"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_history"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["add_comment"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_user_permissions"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["load_files"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["remove_file"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_version"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["add_data"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["update_data"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["delete_data"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["statistic_complaints"].reset(new ResponseFactoryInstance<DefaultResponse>);

    // NOTE: PhotoValidator
    _responseFactoryMap["select_new_photos"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["fetch_new_photos"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["edit_photos"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["accept_driver"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["select_history_photos"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["select_statistic"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["select_template"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["get_driver_photo"].reset(new ResponseFactoryInstance<DefaultResponse>);
    _responseFactoryMap["quick_search"].reset(new ResponseFactoryInstance<DefaultResponse>);

    _responseFactoryMap["update_dbase"].reset(new ResponseFactoryInstance<DefaultResponse>);

    _responseFactoryMap[DEFAULT_TYPE].reset(new ResponseFactoryInstance<DefaultResponse>);
}

ResponseShp ResponseFactory::createSpecializedResponseObject()
{
    Q_ASSERT_X(false, __FUNCTION__, "Creation of Response object prohibited, only derived allowed");
    return QSharedPointer<Response>();
}

ResponseShp ResponseFactory::createResponseObject(const QVariant &msg)
{
    static ResponseFactory responseFactory(true);

//    qDebug() << __FUNCTION__ << " msg:" << msg;
    //check header and body exist
    QVariantMap msgRootMap = msg.toMap();
    QVariantMap::iterator headIter = msgRootMap.find("head");
    QVariantMap::iterator bodyIter = msgRootMap.find("body");
    if (headIter == msgRootMap.end() || bodyIter == msgRootMap.end())
    {
        qCritical() << "Wrong response data format: 'head' or 'body' does not exist." << "msg = '" << msg << "'";
        //Q_ASSERT(false);
        return QSharedPointer<Response>();
    }

    //read header
    QVariantMap headMap = headIter->toMap();
    QString commandType = headMap["type"].toString();
    if (commandType.isEmpty())
    {
        qCritical() << "Wrong response data format: 'head/type' field is empty";
        //Q_ASSERT(false);
        return QSharedPointer<Response>();
    }

    ResponseFactoryMap::iterator factoryIter = responseFactory._responseFactoryMap.find(commandType);
    if (factoryIter == responseFactory._responseFactoryMap.end())
        factoryIter = responseFactory._responseFactoryMap.find(DEFAULT_TYPE);
    if (factoryIter != responseFactory._responseFactoryMap.end())
    {
        QSharedPointer<ResponseFactory> factoryPtr = factoryIter.value();
        QSharedPointer<Response> response = factoryPtr->createSpecializedResponseObject();
        response->fromVariant(msg);
        return response;
    }
    else
    {
        qCritical() << "Wrong response data format: command of type" << commandType << "not found";
        Q_ASSERT(false);
        return QSharedPointer<Response>();
    }
}
