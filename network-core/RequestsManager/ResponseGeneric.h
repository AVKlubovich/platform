﻿#pragma once

#include "Response.h"


namespace network
{

    class Request;

    class ResponseGeneric : public Response
    {
    public:
        enum Status
        {
            StatusUnknown,
            StatusSuccess,
            StatusFail,
        };

        ResponseGeneric();
        ResponseGeneric(QSharedPointer<network::Request> requestGeneric, Status status = Status::StatusUnknown);

        void setStatus(Status status) {_status = status;}
        Status status() { return _status; }
        void setMessage(const QString &msg) {_message=msg;}
        QString message() {return _message;}

        QVariant data() {return _data;}
        void setData(QVariant data) {_data = data;}

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        Status _status;
        QString _message;
        QVariant _data;
    };

}
