﻿#include "Common.h"
#include "Response.h"

#include "Request.h"


using namespace network;

Response::Response(const QString& type, QSharedPointer<Request> request)
    : _type(type)
    , _request(request)
{
}

QUuid Response::uuid() const
{
    return _request->uuid();
}

QVariant Response::toVariant()
{
    QVariantMap response;

    //generate header
    QVariantMap headMap;
    headMap["type"] = _type;
    response["head"] = headMap;
    
    //generate body
    QVariant body = bodyToVariant();
    response["body"] = body;

    return response;
}

void Response::fromVariant(const QVariant & msg)
{
    //check header and body exist
    QVariantMap msgRootMap = msg.toMap();
    QVariantMap::iterator headIter = msgRootMap.find("head");
    QVariantMap::iterator bodyIter = msgRootMap.find("body");
    if (headIter == msgRootMap.end() || bodyIter == msgRootMap.end())
    {
        qWarning() << __FUNCTION__ << " Wrong response data format: 'head' or 'body' does not exist";
        Q_ASSERT(false);
    }
        
    //read header
    QVariantMap headMap = headIter->toMap();
    _type = headMap["type"].toString();
    if (_type.isEmpty())
    {
        qWarning() << __FUNCTION__ << "Wrong response data format: 'head/type' field is empty";
        Q_ASSERT(false);
    }

    //read body
    bodyFromVariant(bodyIter.value());

    //QVariantMap resultMap;
    //resultMap["status"] = -1;
    //resultMap["error_string"] = errorStr;
    //resultMap["error_type"] = errorType;
}
