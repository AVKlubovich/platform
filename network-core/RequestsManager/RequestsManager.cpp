﻿#include "Common.h"
#include "RequestsManager.h"

#include "ResponseServerError.h"
#include "ResponseFactory.h"
#include "Response.h"

#include "CallbackHolder.h"

#include "Packet/BodyConverter.h"

#include "../utils/Settings/Settings.h"
#include "../utils/Settings/SettingsFactory.h"


using namespace network;

RequestsManager::RequestsManager(QObject* parent)
    : QObject(parent)
{
}

bool RequestsManager::init()
{
    return true;
}

void RequestsManager::sendRequest(QVariant inData)
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Connection");

    QUrl url;
    url.setScheme(settings["Scheme"].toString());
    url.setHost(settings["Host"].toString());
    url.setPort(settings["Port"].toInt());
    QNetworkRequest request(url);

    auto converter = BodyConverter::converters.value(QByteArray("application/json"), nullptr);
    if (converter == nullptr)
    {
        Q_ASSERT_X(converter != nullptr, __FUNCTION__, "converter != nullptr");
        return;
    }
    const auto data = converter->convert(inData);

    request.setRawHeader("Accept-Encoding", "gzip, deflate");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("CustomRawHeader", "CustomRawData");
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(data.size()));

    auto reply = _netManager.post(request, data);
    qDebug() << __FUNCTION__ << "Send: " << data;
    connect(reply, &QNetworkReply::finished, this, &RequestsManager::onResponse);
}

void RequestsManager::sendRequest(const Request &commandRequest)
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Connection");

    QUrl url;
    url.setScheme(settings["Scheme"].toString());
    url.setHost(settings["Host"].toString());
    url.setPort(settings["Port"].toInt());
    QNetworkRequest request(url);

    BodyConverter *converter = BodyConverter::converters.value(QByteArray("application/json"), nullptr);
    if (converter == nullptr)
    {
        Q_ASSERT_X(converter != nullptr, __FUNCTION__, "converter != nullptr");
        return;
    }
    auto inData = commandRequest.toVariant();
    const auto data = converter->convert(inData);

    request.setRawHeader("Accept-Encoding", "gzip, deflate");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("CustomRawHeader", "CustomRawData");
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(data.size()));

    auto reply = _netManager.post(request, data);
    qDebug() << __FUNCTION__ << "Send: " << data;
    connect(reply, &QNetworkReply::finished, this, &RequestsManager::onResponse);
}

void RequestsManager::sendRequest(const QSharedPointer<Request>& commandRequest)
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Connection");

    QUrl url;
    url.setScheme(settings["Scheme"].toString());
    url.setHost(settings["Host"].toString());
    url.setPort(settings["Port"].toInt());
    QNetworkRequest request(url);

    BodyConverter *converter = BodyConverter::converters.value(QByteArray("application/json"), nullptr);
    if (converter == nullptr)
    {
        Q_ASSERT_X(converter != nullptr, __FUNCTION__, "converter != nullptr");
        return;
    }
    auto inData = commandRequest->toVariant();
    const auto data = converter->convert(inData);

    const QByteArray uuid = commandRequest->uuid().toByteArray();
    request.setRawHeader("Accept-Encoding", "gzip, deflate");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("CustomRawHeader", "CustomRawData");
    request.setRawHeader("uuid", uuid);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(data.size()));

    auto reply = _netManager.post(request, data);
    qDebug() << __FUNCTION__ << "Send: " << data;
    connect(reply, &QNetworkReply::finished, this, [this, uuid](){
        onResponseH(uuid);
    });
}

void RequestsManager::onResponse()
{
    auto reply = qobject_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply != nullptr);

    const auto error = reply->error();
    if (error == QNetworkReply::NoError)
    {
        const QByteArray rawData = reply->readAll();
        qDebug() << __FUNCTION__ << "Reply incoming data: " << rawData;

        auto converter = BodyConverter::converters.value( "application/json", nullptr );
        if (converter == nullptr)
        {
            Q_ASSERT_X(converter != nullptr, __FUNCTION__, "converter != nullptr");
            return;
        }
        QVariant data = converter->convert(rawData);

        QSharedPointer<Response> responsePtr = network::ResponseFactory::createResponseObject(data);
        if (!responsePtr)
        {
            QString errorMsg = QString("Unknown response data format: ").append(rawData);
            qWarning() << errorMsg;
            responsePtr.reset(new ResponseServerError(errorMsg));
        }
        responseReady(responsePtr);
        emit responseReady(data);
    }
    else
    {
        QString errorMsg = QString("Network error: %1. Error number: %2").arg(reply->errorString()).arg(reply->error());
        qWarning() << __FUNCTION__ << errorMsg;
        QSharedPointer<ResponseServerError> responseServerError(new ResponseServerError(errorMsg));
        emit responseReady(responseServerError);
        emit requestFailed();//old stuff
    }

    reply->deleteLater();
}

void RequestsManager::onResponseH(const QByteArray& uuid)
{
    auto reply = qobject_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply != nullptr);
    if (reply == nullptr)
    {
        qDebug() << __FUNCTION__ << "Error: " << "reply is null";

        const auto requestData = _requestHolders.take(QUuid(uuid));
        CallbackHolderShp holder = requestData.second;

        reply->deleteLater();

        Q_ASSERT(holder);
        if (!holder)
        {
            qDebug() << __FUNCTION__ << "Error: " << "holden do not found";
            return;
        }

        holder->Call(QSharedPointer<Response>(new ResponseServerError("")));
    }

    QSharedPointer<Response> response;

    const auto& error = reply->error();
    if (error == QNetworkReply::NoError)
    {
        const auto& rawData = reply->readAll();
//        qDebug() << __FUNCTION__ << "Reply:" << rawData << rawData.count();

        const auto& converter = BodyConverter::converters.value("application/json", nullptr);
        if (converter == nullptr)
        {
            qDebug() << __FUNCTION__ << "Error: " << "converter not found";
            Q_ASSERT_X(converter != nullptr, __FUNCTION__, "converter != nullptr");
            return;
        }

        const auto& data = converter->convert(rawData);
//        qDebug() << "incoming: " << data;

        // NOTE: uncomment when do not use Logger
        const auto& list = QJsonDocument::fromVariant(data).toJson().split('\n');
        for (const auto& string : list)
            qDebug() << qPrintable(string);

        response = network::ResponseFactory::createResponseObject(data);
    }
    else
    {
        QString errorMsg = QString("Network error: %1. Error number: %2").arg(reply->errorString()).arg(reply->error());
        qWarning() << __FUNCTION__ << errorMsg;
        response = QSharedPointer<Response>(new ResponseServerError(errorMsg));
    }

    const auto requestData = _requestHolders.take(QUuid(uuid));
    CallbackHolderShp holder = requestData.second;

    reply->deleteLater();

    Q_ASSERT(holder);
    if (!holder)
    {
        qDebug() << __FUNCTION__ << "Error: " << "holden do not found";
        return;
    }
    holder->Call(response);
}


