﻿#include "Common.h"
#include "DefaultResponse.h"


using namespace network;

DefaultResponse::DefaultResponse()
    : Response()
{
}

DefaultResponse::DefaultResponse(const QString& type, const QVariant& response, const QSharedPointer<Request>& request)
    : Response(type, request)
    , _response(response)
{
}

QVariant DefaultResponse::bodyToVariant() const
{
    return _response;
}

void DefaultResponse::bodyFromVariant(const QVariant& body)
{
    _response = body;
}
