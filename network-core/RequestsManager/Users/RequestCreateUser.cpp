﻿#include "Common.h"
#include "RequestCreateUser.h"


using namespace network;

RequestCreateUser::RequestCreateUser(const QString &name, const QString &login
                                     , const QString &password, const QString &position
                                     , const QString &company, const QString &department)
    : Request("createUser")
    , _login(login)
    , _name(name)
    , _password(password)
    , _position(position)
    ,_company(company)
    ,_department(department)
{
}

RequestCreateUser::RequestCreateUser(const QVariant & msg)
    : Request("createUser")
{
    fromVariant(msg);
}

QVariant RequestCreateUser::bodyToVariant() const
{
    QVariantMap loginInfo;
    loginInfo["login"] = _login;
    loginInfo["name"] = _name;
    loginInfo["password"] = _password;
    loginInfo["offecer"] = _position;
    loginInfo["company"] = _company;
    loginInfo["department"] = _department;
    return loginInfo;
}

void RequestCreateUser::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();
    _name = bodyMap["name"].toString();
    _login = bodyMap["login"].toString();
    _password = bodyMap["password"].toString();
    _position = bodyMap["offecer"].toString();
    _company = bodyMap["company"].toString();
    _department = bodyMap["department"].toString();
//    QVariantMap::iterator userNameIter = bodyMap.find("login");
//    if (userNameIter == bodyMap.end())
//        throw std::runtime_error("'login' field does not exist");
//    _login = userNameIter.value().toString();
    if ((_login.isEmpty()) || (_name.isEmpty()) || (_password.isEmpty()))
        throw std::runtime_error("'_login' or '_name' or '_password' field exist, but not set");

//    QVariantMap bodyMap = body.toMap();
//    QVariantMap::iterator statusIter = bodyMap.find("status");
//    if (statusIter != bodyMap.end())
//        _status = (Status)statusIter.value().toInt();
//    else
//        _status = StatusUnknown;

}

QString RequestCreateUser::department() const
{
    return _department;
}

QString RequestCreateUser::company() const
{
    return _company;
}

QString RequestCreateUser::position() const
{
    return _position;
}

QString RequestCreateUser::password() const
{
    Q_ASSERT(!_password.isEmpty());
    return _password;
}

QString RequestCreateUser::login() const
{
    Q_ASSERT(!_login.isEmpty());
    return _login;
}

QString RequestCreateUser::name() const
{
    Q_ASSERT(!_name.isEmpty());
    return _name;
}
