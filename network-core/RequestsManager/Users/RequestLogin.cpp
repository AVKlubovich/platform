﻿#include "Common.h"
#include "RequestLogin.h"


using namespace network;

RequestLogin::RequestLogin(const QString & login, const QString & password)
    : Request("login")
    , _login(login)
    , _password(password)
{
}

RequestLogin::RequestLogin(const QVariant & msg)
    : Request("login")
{
    fromVariant(msg);
}

QString RequestLogin::login() const
{
    Q_ASSERT(!_login.isEmpty());
    return _login;
}

QString RequestLogin::password() const
{
    //Q_ASSERT(!_password.isEmpty());
    return _password;
}



QVariant RequestLogin::bodyToVariant() const
{
    QVariantMap loginInfo;
    loginInfo["login"] = _login;
    loginInfo["password"] = _password;
    return loginInfo;
}

void RequestLogin::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();
    QVariantMap::iterator loginIter = bodyMap.find("login");
    QVariantMap::iterator passwordIter = bodyMap.find("password");
    if ((loginIter == bodyMap.end()) && (passwordIter == bodyMap.end()))
        throw "'login' or 'password' field does not exist";
    _login = loginIter.value().toString();
    _password = passwordIter.value().toString();
    if (_login.isEmpty())
        throw "'login' field exist, but not set";
}
