﻿#pragma once

#include "../Request.h"


namespace network
{

    class RequestLogin : public Request
    {
    public:
        RequestLogin(const QString & login, const QString &password);
        RequestLogin(const QVariant & msg);

        QString login() const;
        QString password() const;

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        QString _login;
        QString _password;
    };

}
