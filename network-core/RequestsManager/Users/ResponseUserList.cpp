﻿#include "Common.h"
#include "ResponseUserList.h"


using namespace network;

ResponseUserList::ResponseUserList()
    : Response("get_user_list", QSharedPointer<network::RequestUserList>())
    , _status(StatusUnknown)
{

}

ResponseUserList::ResponseUserList(QSharedPointer<network::RequestUserList> requestUserList)
    : Response("get_user_list", requestUserList)
    , _status(StatusUnknown)
{
}

QVariant ResponseUserList::bodyToVariant() const
{
    QVariantMap bodyMap;
    bodyMap["status"] = _status;
    bodyMap["userList"] = _users.toVariant();

    return bodyMap;
}

void ResponseUserList::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();

    //response status
    QVariantMap::iterator statusIter = bodyMap.find("status");
    if (statusIter != bodyMap.end())
    {
        _status = (Status)statusIter.value().toInt();
        qWarning() << "'status' field does not exist in the response body";
    }
    else
        _status = StatusUnknown;

    //user list
    QVariantMap::iterator userListIter = bodyMap.find("userList");
    if (userListIter == bodyMap.end())
    {
        _status = StatusDataFormatError;
        _statusMsg = "'userList' data does not exist in response body";
        qWarning() << _statusMsg;
    }
    else
    {
        //QVariantList userList = userListIter.value();
        //qDebug() << "Users:" << userList;
        /*for (QVariantList::iterator userIter : userList)
        {
            QVariantMap userParamsList = userIt.val
        }*/
    }

}
