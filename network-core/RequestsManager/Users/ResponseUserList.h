﻿#pragma once

#include "../Response.h"

#include "RequestUserList.h"

#include "Users.h"


namespace network
{

    class ResponseUserList : public Response
    {
    public:
        enum Status
        {
            StatusUnknown,
            StatusSuccess,
            StatusDBError,
            StatusDataFormatError,
        };

        ResponseUserList();
        ResponseUserList(QSharedPointer<network::RequestUserList> requestUserList);

        void setStatus(Status status) { _status = status; }
        Status status() { return _status; }

        void setUsers(const Users & userList) { _users = userList; }
        Users users() { return _users; }

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        Status _status;
        QString _statusMsg;
        Users _users;
    };

}
