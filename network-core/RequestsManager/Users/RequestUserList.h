﻿#pragma once

#include "../Request.h"
#include "Users.h"


namespace network
{

    class RequestUserList : public Request
    {
    public:
        RequestUserList();
        RequestUserList(const QVariant & msg);

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;
    };

}
