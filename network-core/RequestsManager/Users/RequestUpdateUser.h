﻿#pragma once

#include "../Request.h"


namespace network
{

    class RequestUpdateUser : public Request
    {
    public:
        RequestUpdateUser(const quint64 id, const QString &name, const QString &login, const QString &password
                          , const QString &position, const QString &company
                          , const QString &department);
        RequestUpdateUser(const QVariant & msg);

        QString name() const;
        QString login() const;
        QString password() const;
        QString position() const;
        QString company() const;
        QString department() const;

        quint64 id() const;

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        quint64 _id;
        QString _login;
        QString _name;
        QString _password;
        QString _position;
        QString _company;
        QString _department;
    };

}
