﻿#include "Common.h"
#include "RequestUpdateUser.h"


using namespace network;

RequestUpdateUser::RequestUpdateUser(const quint64 id
                                     , const QString &name, const QString &login
                                     , const QString &password, const QString &position
                                     , const QString &company, const QString &department)
    : Request("updateUser")
    , _id(id)
    , _login(login)
    , _name(name)
    , _password(password)
    , _position(position)
    ,_company(company)
    ,_department(department)
{
}

RequestUpdateUser::RequestUpdateUser(const QVariant & msg)
    : Request("updateUser")
    , _id(0)
{
    fromVariant(msg);
}

QVariant RequestUpdateUser::bodyToVariant() const
{
    QVariantMap loginInfo;
    loginInfo["id"] = _id;
    loginInfo["login"] = _login;
    loginInfo["name"] = _name;
    loginInfo["password"] = _password;
    loginInfo["offecer"] = _position;
    loginInfo["company"] = _company;
    loginInfo["department"] = _department;
    return loginInfo;
}

void RequestUpdateUser::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();
    _id = bodyMap["id"].toLongLong();
    _name = bodyMap["name"].toString();
    _login = bodyMap["login"].toString();
    _password = bodyMap["password"].toString();
    _position = bodyMap["offecer"].toString();
    _company = bodyMap["company"].toString();
    _department = bodyMap["department"].toString();
//    QVariantMap::iterator userNameIter = bodyMap.find("login");
//    if (userNameIter == bodyMap.end())
//        throw std::runtime_error("'login' field does not exist");
//    _login = userNameIter.value().toString();
    if ((_login.isEmpty()) || (_name.isEmpty()) || (_password.isEmpty()))
        throw std::runtime_error("'_login' or '_name' or '_password' field exist, but not set");

//    QVariantMap bodyMap = body.toMap();
//    QVariantMap::iterator statusIter = bodyMap.find("status");
//    if (statusIter != bodyMap.end())
//        _status = (Status)statusIter.value().toInt();
//    else
//        _status = StatusUnknown;

}

QString RequestUpdateUser::department() const
{
    return _department;
}

quint64 RequestUpdateUser::id() const
{
    return _id;
}

QString RequestUpdateUser::company() const
{
    return _company;
}

QString RequestUpdateUser::position() const
{
    return _position;
}

QString RequestUpdateUser::password() const
{
    Q_ASSERT(!_password.isEmpty());
    return _password;
}

QString RequestUpdateUser::login() const
{
    Q_ASSERT(!_login.isEmpty());
    return _login;
}

QString RequestUpdateUser::name() const
{
    Q_ASSERT(!_name.isEmpty());
    return _name;
}
