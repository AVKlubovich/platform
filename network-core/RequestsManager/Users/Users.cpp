﻿#include "Common.h"
#include "Users.h"


using namespace network;

QVariant User::toVariant() const
{
    QVariantMap user;
    user["id"] = _id;
    user["login"] = _login;
    user["name"] = _name;
    user["officer"] = _officer;
    user["department"] = _department;
    user["company"] = _company;
    user["password"] = _password;
    return user;
}

void User::fromVariant(const QVariant & data)
{
    QVariantMap userMap = data.toMap();
    _id = userMap["id"].toLongLong();
    _login = userMap["login"].toString();
    _name = userMap["name"].toString();
    _officer = userMap["officer"].toString();
    _department = userMap["department"].toString();
    _company = userMap["company"].toString();
    _password = userMap["password"].toString();
}

long long User::id() const
{
    return _id;
}

void User::setId(long long id)
{
    _id = id;
}

QString User::login() const
{
    return _login;
}

void User::setLogin(const QString &login)
{
    _login = login;
}

QString User::name() const
{
    return _name;
}

void User::setName(const QString &name)
{
    _name = name;
}

QString User::officer() const
{
    return _officer;
}

void User::setOfficer(const QString &officer)
{
    _officer = officer;
}

QString User::password() const
{
    return _password;
}

void User::setPassword(const QString &password)
{
    _password = password;
}

QString User::company() const
{
    return _company;
}

void User::setCompany(const QString &company)
{
    _company = company;
}

QString User::department() const
{
    return _department;
}

void User::setDepartment(const QString &department)
{
    _department = department;
}

//QVariant Users::toVariant() const
//{
//    QVariantList users;
//    for (const User user: *this)
//    {
//        users.push_back(user.toVariant());
//    }
//    return users;
//}

void Users::fromVariant(QVariant data)
{
    QVariantList usersVariantList = data.toList();
    for (QVariant & userVariant: usersVariantList)
    {
        User user;
        user.fromVariant(userVariant);
        push_back(user);
    }
}
