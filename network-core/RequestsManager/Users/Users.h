﻿#pragma once


namespace network
{

    class User
    {
    public:
        explicit User() : _id(0) {}
//        const QString login() const {return _login;};
//        void setLogin(const QString & login) {_login=login;};
//        const QString userPassword() const;
//        void setName(const QString & name) {_name=name;};
//        const QString name() const {return _name;};
    public:
        QVariant toVariant() const;
        void fromVariant(const QVariant & data);

        long long id() const;
        void setId(long long id);

        QString login() const;
        void setLogin(const QString &login);

        QString name() const;
        void setName(const QString &name);

        QString officer() const;
        void setOfficer(const QString &officer);

        QString password() const;
        void setPassword(const QString &password);

        QString company() const;
        void setCompany(const QString &company);

        QString department() const;
        void setDepartment(const QString &department);

    private:
        long long _id;
        QString _login;
        QString _name;
        QString _officer;
        QString _company;
        QString _department;
        QString _password;

        //const QUuid _uuid;
    };

    class Users : public QList<User>
    {
    public:
        QVariant toVariant() const{
            QVariantList users;
            for (const User user: *this)
            {
                users.push_back(user.toVariant());
            }
            return users;
        }

        void fromVariant(QVariant users);
    };

}
