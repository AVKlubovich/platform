﻿#pragma once

#include "../Response.h"


namespace network
{

    class RequestLogin;

    class ResponseLogin : public Response
    {
    public:
        enum Status
        {
            StatusError = -1,
            StatusUnknown,
            StatusSuccess,
            StatusRejected,
            statusNoPermissions
        };

        ResponseLogin();
        ResponseLogin(QSharedPointer<network::RequestLogin> requestLogin, Status status = Status::StatusUnknown);

        const Status status() const;
        void setStatus(const Status status);

        const quint64 userId() const;
        void setUserId(const quint64 userId);

        const QList<QVariant> userRights() const;
        void setUserRights(const QList<QVariant> &userRights);

        QString error() const;
        void setError(const QString &error);

        const QString userName() const;
        void setUserName(const QString &userName);

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        Status _status;
        quint64 _userId;
        QString _userName;
        QList<QVariant> _userRights;
        QString _error;

        bool _errors;
    };

    typedef QSharedPointer<ResponseLogin> ResponseLoginShp;
}
