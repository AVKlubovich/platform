﻿#include "Common.h"
#include "ResponseLogin.h"

#include "RequestLogin.h"


using namespace network;

ResponseLogin::ResponseLogin()
    : Response()
    , _status(StatusUnknown)
    , _userId(0)
    , _errors(false)
{
}

ResponseLogin::ResponseLogin(QSharedPointer<RequestLogin> requestLogin, ResponseLogin::Status status)
    : Response("login", requestLogin)
    , _status(StatusUnknown)
    , _userId(0)
    , _errors(false)
{
}

const ResponseLogin::Status ResponseLogin::status() const
{
    return _status;
}

void ResponseLogin::setStatus(const Status status)
{
    _status = status;
}

const quint64 ResponseLogin::userId() const
{
    return _userId;
}

void ResponseLogin::setUserId(const quint64 userId)
{
    _userId = userId;
}

QVariant ResponseLogin::bodyToVariant() const
{
    QVariantMap bodyMap;
    if(_errors)
    {
        bodyMap["status"] = _status;
        bodyMap["error"] = _error;

       return bodyMap;
    }

    // NOTE: Вася - сука!!! несколько часов из-за него убил. 2016.12.23 02.38

    bodyMap["user_name"] = _userName;
    bodyMap["status"] = _status;
    bodyMap["id_user"] = _userId;
    bodyMap["user_rights"] = _userRights;

    return bodyMap;
}

void ResponseLogin::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();
    QVariantMap::iterator statusIter = bodyMap.find("status");
    if (statusIter != bodyMap.end())
        _status = (Status)statusIter.value().toInt();
    else
        _status = StatusUnknown;

    if (_status == StatusError)
    {
        _error = bodyMap["error"].toString();
        return;
    }

    auto userIdIter = bodyMap.find("id_user");
    if (userIdIter != bodyMap.end())
        _userId = userIdIter.value().toULongLong();
    else
        _userId = 0;

    _userRights = bodyMap["user_rights"].toList();
}

const QString ResponseLogin::userName() const
{
    return _userName;
}

void ResponseLogin::setUserName(const QString &userName)
{
    _userName = userName;
}

QString ResponseLogin::error() const
{
    return _error;
}

void ResponseLogin::setError(const QString &error)
{
    _error = error;
    _errors = true;
}

const QList<QVariant> ResponseLogin::userRights() const
{
    return _userRights;
}

void ResponseLogin::setUserRights(const QList<QVariant> &permissions)
{
    _userRights = permissions;
}
