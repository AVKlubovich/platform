﻿#pragma once

#include "../Request.h"


namespace network
{

    class RequestCreateUser : public Request
    {
    public:
        RequestCreateUser(const QString &name, const QString &login, const QString &password
                          , const QString &position, const QString &company
                          , const QString &department);
        RequestCreateUser(const QVariant & msg);

        QString name() const;
        QString login() const;
        QString password() const;
        QString position() const;
        QString company() const;
        QString department() const;

    protected:
        virtual QVariant bodyToVariant() const override;
        virtual void bodyFromVariant(const QVariant & body) override;

    private:
        QString _login;
        QString _name;
        QString _password;
        QString _position;
        QString _company;
        QString _department;
    };

}
