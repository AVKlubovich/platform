﻿#pragma once

#include "Request.h"


namespace network
{

    class DefaultRequest : public Request
    {
    public:
        DefaultRequest(const QString& type, const QVariant& data);

    public:
        //QVariant toVariant() const override;

    protected:
        QVariant bodyToVariant() const override;
        void bodyFromVariant(const QVariant & body) override;

    private:
        QVariant _body;
    };

}
