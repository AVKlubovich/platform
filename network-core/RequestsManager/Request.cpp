﻿#include "Common.h"
#include "Request.h"


using namespace network;

Request::Request(const QString& type)
    : _type(type)
    , _uuid(QUuid::createUuid())
{
}

QUuid Request::uuid() const
{
    return _uuid;
}

QString Request::type() const
{
    return _type;
}

QVariant Request::toVariant() const
{
    QVariantMap msg;

    //generate header
    QVariantMap headMap;
    headMap["type"] = _type;
    msg["head"] = headMap;

    //QVariantMap resultMap;
    //resultMap["status"] = -1;
    //resultMap["error_string"] = errorStr;
    //resultMap["error_type"] = errorType;

    //auto& responce = _context._responce;
    //responce->setBody(QVariant::fromValue(resultMap));

    //generate body
    QVariant body = bodyToVariant();
    msg["body"] = body;

    return msg;
}

void Request::fromVariant(const QVariant & msg)
{
    //check header and body exist
    QVariantMap msgRootMap = msg.toMap();
    QVariantMap::iterator headIter = msgRootMap.find("head");
    QVariantMap::iterator bodyIter = msgRootMap.find("body");
    if (headIter == msgRootMap.end() || bodyIter == msgRootMap.end())
        throw std::runtime_error("Wrong request data format: 'head' or 'body' does not exist");

    //read header
    QVariantMap headMap = headIter->toMap();
    _type = headMap["type"].toString();
    if (_type.isEmpty())
        throw std::runtime_error("Wrong request data format: 'head/type' field is empty");

    //read body
    bodyFromVariant(bodyIter.value());

    //QVariantMap resultMap;
    //resultMap["status"] = -1;
    //resultMap["error_string"] = errorStr;
    //resultMap["error_type"] = errorType;
}
