﻿#include "Common.h"
#include "ResponseServerError.h"


using namespace network;

ResponseServerError::ResponseServerError()
    : Response()
{
}

ResponseServerError::ResponseServerError(const QString& errorMessage, QSharedPointer<Request> request)
    : Response("ServerError", request)
    , _errorMessage(errorMessage)
{
}

QVariant ResponseServerError::bodyToVariant() const
{
    QVariantMap bodyMap;
    bodyMap["errorMessage"] = _errorMessage;

    return bodyMap;
}

void ResponseServerError::bodyFromVariant(const QVariant & body)
{
    QVariantMap bodyMap = body.toMap();
    QVariantMap::iterator errorMessageIter = bodyMap.find("errorMessage");

    Q_ASSERT(errorMessageIter != bodyMap.end());

    if (errorMessageIter != bodyMap.end())
    {
        _errorMessage = errorMessageIter.value().toString();
        Q_ASSERT(!_errorMessage.isEmpty() && !_errorMessage.isNull());
    }
}
