﻿#include "Common.h"
#include "DefaultRequest.h"


using namespace network;

DefaultRequest::DefaultRequest(const QString& type, const QVariant& request)
    : Request(type)
    , _body(request)
{
}

//QVariant DefaultRequest::toVariant() const
//{
//    return _request;
//}

QVariant DefaultRequest::bodyToVariant() const
{
    return _body;
}

void DefaultRequest::bodyFromVariant(const QVariant& body)
{
    _body = body;
}
