﻿#pragma once

#include "Response.h"
#include "Request.h"


namespace network
{

    class DefaultResponse : public Response
    {
    public:
        DefaultResponse();
        DefaultResponse(const QString& type, const QVariant& data, const QSharedPointer<Request>& request);

        QVariant bodyToVariant() const override;
        void bodyFromVariant(const QVariant & body) override;

    private:
        QVariant _response;
    };

}
