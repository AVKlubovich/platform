﻿#include "Common.h"
#include "Headers.h"


using namespace network;

void Headers::setHeader(const QByteArray& key, const QByteArray& value)
{
    _headers[key] = value;
}

void Headers::clear()
{
    _headers.clear();
}

QByteArray Headers::header(const QByteArray& key) const
{
    const auto value = _headers.value(key);
    return value;
}

const QMap<QByteArray, QByteArray>& Headers::allHeaders() const
{
    return _headers;
}
