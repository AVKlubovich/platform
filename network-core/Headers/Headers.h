﻿#pragma once


namespace network
{

    class Headers
    {
    public:
        Headers() = default;
        ~Headers() = default;

        void setHeader(const QByteArray& key, const QByteArray& value);
        void clear();

        QByteArray header(const QByteArray& key) const;
        const QMap<QByteArray, QByteArray>& allHeaders() const;

    private:
        QMap<QByteArray, QByteArray> _headers;
    };

}
