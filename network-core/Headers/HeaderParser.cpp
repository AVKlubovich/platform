﻿#include "Common.h"
#include "HeaderParser.h"


namespace network
{

    bool HeaderParser::parse( const QByteArray& data )
    {
        const auto sepIndex = data.indexOf( '=' );
        if ( sepIndex < 0 )
            return false;

        const auto key = data.mid( 0, sepIndex );
        const auto value = data.mid( sepIndex + 1 );

        if ( key.isEmpty() || value.isEmpty() )
            return false;

        _headers.setHeader( key, value );
        return true;
    }

    const Headers& HeaderParser::headers() const
    {
        return _headers;
    }

}
