﻿#pragma once


#include "Headers.h"


namespace network
{

    class HeaderParser
    {
    public:
        HeaderParser() = default;
        ~HeaderParser() = default;

        bool parse( const QByteArray& data );

        const Headers& headers() const;

    private:
        Headers _headers;
    };

}
