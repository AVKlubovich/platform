# platform

##Описание проекта
Ядро клиент-серверной системы

##Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14)

##Системные требования
Windows; Linux  
nginx  
PostgreSql v9.4 и выше

##Документация
[Соглашения по оформлению кода](https://drive.google.com/open?id=0B48GpktEZIksWjNjTmdWcW53Rnc)

##Список контрибьюторов
[Alexandr Klubovich](../../../../avklubovich)  
[Alexandr Dedckov](../../../../jimak)