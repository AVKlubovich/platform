#include "Common.h"
#include "FileDownloader.h"

#include "File.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"


using namespace file_attach;

FileDownloader::FileDownloader(QObject *parent)
    : QObject(parent)
    , _manager(new QNetworkAccessManager(this))
    , _outputFile(new QFile)
{
}

void FileDownloader::downloadFile(FileShp file)
{
    _file = file;

    prepareOutputFile();

    QNetworkRequest request(_file->url());
    const auto& reply = _manager->get(request);
    connect(reply, &QNetworkReply::readyRead,
            this, &FileDownloader::downloadReadyRead);

    connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
            this, &FileDownloader::slotError);
    connect(reply, &QNetworkReply::sslErrors,
            this, &FileDownloader::slotSslErrors);

    connect(reply, &QNetworkReply::finished,
            this, &FileDownloader::downloadFinished);
}

void FileDownloader::downloadReadyRead()
{
    const auto& reply = static_cast<QNetworkReply*>(sender());
    _outputFile->write(reply->readAll());
}

void FileDownloader::downloadFinished()
{
    _outputFile->close();

    const auto& reply = static_cast<QNetworkReply*>(sender());
    if (reply->error())
    {
        emit error(reply->errorString());
    }
    else
    {
        _file->setDownloadedUrl(QUrl::fromLocalFile(_outputFile->fileName()));
        emit finish();
    }

    reply->deleteLater();
}

void FileDownloader::slotError(QNetworkReply::NetworkError error)
{
}

void FileDownloader::slotSslErrors(QList<QSslError> errors)
{
}

void FileDownloader::prepareOutputFile()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    const auto& filesFolder = settings["FilesFolder"].toString();

    auto filesDir = QDir::home();
    if (!filesDir.exists(filesFolder))
    {
        const auto& mkpathResult = filesDir.mkpath(filesFolder);
        if (!mkpathResult)
        {
            Q_ASSERT_X(mkpathResult, __FUNCTION__, "filesDir.mkpath == false");
            emit error(tr("Do not create users directory"));
            return;
        }
    }
    filesDir.cd(filesFolder);

    const auto& fullFileName = QString("%1%2%3")
                               .arg(filesDir.absolutePath())
                               .arg(QDir::separator())
                               .arg(_file->originalFileName());
    _outputFile->setFileName(fullFileName);
    if (!_outputFile->exists())
    {
        if (!_outputFile->open(QIODevice::WriteOnly))
        {
            Q_ASSERT_X(false, __FUNCTION__, "_outputFile->open == false");
            emit error(tr("Do not create file"));
            return;
        }
    }
}
