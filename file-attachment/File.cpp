#include "Common.h"
#include "File.h"


using namespace file_attach;

File::File(const quint64 id, const QUrl &url, const QString &origName)
    : _id(id)
    , _url(url)
    , _origName(origName)
{
}

const quint64 File::fileId() const
{
    return _id;
}

const QUrl File::url() const
{
    return _url;
}

QString File::originalFileName()
{
    return _origName;
}

QUrl File::downloadedUrl()
{
    return _downloadedUrl;
}

void File::setDownloadedUrl(const QUrl& url)
{
    _downloadedUrl = url;
}
