#include "Common.h"
#include "FileAttachmentWidget.h"
#include "ui_FileAttachmentWidget.h"

#include "File.h"
#include "FileWidget.h"

#include "../network-core/RequestsManager/DefaultRequest.h"
#include "../network-core/RequestsManager/ResponseGeneric.h"

#include "../network-core/RequestsManager/Request.h"
#include "../network-core/RequestsManager/Response.h"
#include "../network-core/RequestsManager/RequestsManager.h"

#include "web-exchange/WebRequest.h"
#include "web-exchange/WebRequestManager.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "utils/BaseClasses/MetaTypeRegistration.h"
#include "permissions/UserPermission.h"

RegisterMetaType(file_attach::FileShp)

#define MB100 104857600


using namespace file_attach;

FileAttachmentWidget::FileAttachmentWidget(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::FileAttachmentWidget)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    connect(_ui->btnAddFile, &QPushButton::clicked, this, &FileAttachmentWidget::addFile);
    connect(_ui->btnCloseWidget, &QPushButton::clicked, this, &FileAttachmentWidget::hide);
}

FileAttachmentWidget::~FileAttachmentWidget()
{
    delete _ui;
}

void FileAttachmentWidget::loadFilesList(const quint64 idComplaint)
{
    _idComplaint = idComplaint;
    _widgets.clear();
    checkFilesCount();
    emit removeAllWidgets();

    QVariantMap requestMap;
    requestMap["type_command"] = "load_files";
    requestMap["id_complaint"] = _idComplaint;

    network::RequestShp request(new network::DefaultRequest("load_files", QVariant::fromValue(requestMap)));
    _requestsManager->execute(request, this, &FileAttachmentWidget::onLoadFilesList);
}

void FileAttachmentWidget::onLoadFilesList(network::ResponseShp response)
{
    auto responseObj = response->toVariant().toMap();
    const auto& commandType = responseObj["head"].toMap()["type"].toString();

    if (commandType.trimmed() != "load_files")
        return;

    const auto& responseMap = responseObj["body"].toMap();

    const auto& responseList = responseMap["files"].toList();
    for (const auto& file : responseList)
    {
        createFileWidget(file.toMap());
    }

    emit filesCountChanged(_widgets.count());
}

void FileAttachmentWidget::uploadAllFiles(const quint64 idComplaint)
{
    for (const auto widget : _widgets)
    {
        const auto& fInfo = QFileInfo(widget->getFile()->url().toString());

        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

        QHttpPart typeCommandPart;
        typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
        typeCommandPart.setBody("add_file");
        multiPart->append(typeCommandPart);

        QHttpPart jsonPart;
        const auto& json = QString("{\"id_complaint\":\"%1\"}").arg(idComplaint);
        jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
        jsonPart.setBody(json.toUtf8());
        multiPart->append(jsonPart);

        QHttpPart imagePart;
        imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/plain"));
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"file\"; filename=\"%1\"").arg(fInfo.fileName())));
        imagePart.setRawHeader("Content-Transfer-Encoding","binary");
        QFile *file = new QFile(fInfo.absoluteFilePath());
        file->open(QIODevice::ReadOnly);
        imagePart.setBodyDevice(file);
        file->setParent(multiPart);
        multiPart->append(imagePart);

        sendToPhpAddFile(multiPart);
    }
}

void FileAttachmentWidget::addFile()
{
    const QFileInfo fInfo(QFileDialog::getOpenFileName());
    if (fInfo.size() > MB100)
    {
        QMessageBox::warning(nullptr, tr("Warning!"), tr("File size exceeds the limit of 104857600 bytes"));
        return;
    }

    if (_idComplaint == 0)
    {
        QVariantMap fileMap;
//        const auto fileId = fileMap["id"].toULongLong();
//        const auto& fileUrl = fileMap["file_url"].toString();
//        const auto& origName = fileMap["orig_file_name"].toString();
        fileMap["id"] = 0;
        fileMap["file_url"] = fInfo.absoluteFilePath();
        fileMap["orig_file_name"] = fInfo.fileName();
        fileMap["downloaded_url"] = fInfo.absoluteFilePath();
        fileMap["idUser"] = permissions::UserPermission::instance().idUser();
        createFileWidget(fileMap);

        return;
    }

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("add_file");
    multiPart->append(typeCommandPart);

    QHttpPart jsonPart;
    const auto& json = QString("{\"id_complaint\":\"%1\"}").arg(_idComplaint);
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/plain"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"file\"; filename=\"%1\"").arg(fInfo.fileName())));
    imagePart.setRawHeader("Content-Transfer-Encoding","binary");
    QFile *file = new QFile(fInfo.absoluteFilePath());
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart);
    multiPart->append(imagePart);

    sendToPhpAddFile(multiPart);
}

void FileAttachmentWidget::onAddFile(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        qDebug() << reply->errorString();
        QMessageBox::critical(nullptr, tr("Error"), tr("File do not upload"));
        return;
    }

    auto bArray = reply->readAll();
    bArray = bArray.right(bArray.size() - bArray.indexOf("{"));
    const auto doc = QJsonDocument::fromJson(bArray);
    auto jobj = doc.object();
    const auto& outData = jobj.toVariantMap();

    const auto status = outData["head"].toMap()["status"].toInt();
    if (status != 1)
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("File do not upload"));
        return;
    }

    const auto& resultMap = outData["body"].toMap();
    createFileWidget(resultMap);

    --_uploadCounter;
    if (_uploadCounter == 0)
        emit finishUpload();

    emit filesCountChanged(_widgets.count());
}

void FileAttachmentWidget::removeFile(const quint64 fileId, const QString& fileUrl)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "remove_file";
    requestMap["id_file"] = fileId;
    requestMap["file_url"] = fileUrl;
    requestMap["idUser"] = permissions::UserPermission::instance().idUser();

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart typeCommandPart;
    typeCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"type_command\""));
    typeCommandPart.setBody("remove_file");
    multiPart->append(typeCommandPart);

    QHttpPart jsonPart;
    const auto& json = QString("{\"type_command\":\"%1\", \"id_file\":\"%2\", \"file_url\":\"%3\"}")
                       .arg("remove_file")
                       .arg(fileId)
                       .arg(fileUrl);
    jsonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"json\""));
    jsonPart.setBody(json.toUtf8());
    multiPart->append(jsonPart);

    sendToPhpRemoveFile(multiPart);
}

void FileAttachmentWidget::onRemoveFile(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("File do not remove"));
        return;
    }

    auto bArray = reply->readAll();
    bArray = bArray.right(bArray.size() - bArray.indexOf("{"));
    const auto doc = QJsonDocument::fromJson(bArray);
    auto jobj = doc.object();
    const auto& outData = jobj.toVariantMap();

    const auto status = outData["head"].toMap()["status"].toInt();
    if (status != 1)
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("File do not remove"));
        return;
    }

    const auto fileId = outData["body"].toMap()["id_file"].toInt();
    emit removeFileWidget(fileId);

    sender()->deleteLater();
}

void FileAttachmentWidget::onRemoveWidget()
{
    _widgets.removeAll(qobject_cast<FileWidget*>(sender()));

    emit filesCountChanged(_widgets.count());
    checkFilesCount();
}

void FileAttachmentWidget::sendToPhpAddFile(QHttpMultiPart *multiPart)
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("PHP");
    QUrl url(settings["Url"].toString());
    QNetworkRequest request(url);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkReply* reply = networkManager->post(request, multiPart);
    multiPart->setParent(reply);
    connect(networkManager, &QNetworkAccessManager::finished, this, &FileAttachmentWidget::onAddFile);

    ++_uploadCounter;
}

void FileAttachmentWidget::sendToPhpRemoveFile(QHttpMultiPart *multiPart)
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("PHP");
    QUrl url(settings["Url"].toString());
    QNetworkRequest request(url);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkReply* reply = networkManager->post(request, multiPart);
    multiPart->setParent(reply);
    connect(networkManager, &QNetworkAccessManager::finished, this, &FileAttachmentWidget::onRemoveFile);
}

void FileAttachmentWidget::createFileWidget(const QVariantMap &fileMap)
{
    const auto fileId = fileMap["id"].toULongLong();
    const auto& fileUrl = fileMap["file_url"].toString();
    const auto& origName = fileMap["orig_file_name"].toString();
    const FileShp file = FileShp::create(fileId, fileUrl, origName);

    if (fileMap.contains("downloaded_url"))
        file->setDownloadedUrl(fileMap["downloaded_url"].toUrl());

    const auto newFileWidget = new FileWidget(file, this);
    connect(newFileWidget, &FileWidget::removeFile, this, &FileAttachmentWidget::removeFile);
    connect(newFileWidget, &FileWidget::willDestroyed, this, &FileAttachmentWidget::onRemoveWidget);
    connect(this, &FileAttachmentWidget::removeFileWidget, newFileWidget, &FileWidget::deleteLaterPerId);
    connect(this, &FileAttachmentWidget::removeAllWidgets, newFileWidget, &FileWidget::deleteLater);

    _widgets << newFileWidget;
    _ui->filesLayout->addWidget(newFileWidget);

    checkFilesCount();
}

void FileAttachmentWidget::checkFilesCount()
{
    const auto msg = QString("Файлов: %1").arg(_widgets.count());
    _ui->labelFileCount->setText(msg);
}
