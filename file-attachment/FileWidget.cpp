#include "Common.h"
#include "FileWidget.h"
#include "ui_FileWidget.h"

#include "File.h"
#include "FileDownloader.h"

#include "../progress-indicator/ProgressIndicator.h"


using namespace file_attach;

FileWidget::FileWidget(const FileShp &file, QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::FileWidget)
    , _file(file)
{
    _ui->setupUi(this);

    _ui->labelFileName->setText(file->originalFileName());

    connect(_ui->btnDownload, &QPushButton::clicked, this, &FileWidget::downloadFile);
    connect(_ui->btnOpen, &QPushButton::clicked, this, &FileWidget::openFile);
    connect(_ui->btnSave, &QPushButton::clicked, this, &FileWidget::saveFile);
    connect(_ui->btnDelete, &QPushButton::clicked, this, &FileWidget::deleteFile);

    _ui->btnDownload->setIcon(QIcon(":/buttons/load.png"));
    _ui->btnOpen->setIcon(QIcon(":/buttons/play.png"));

    setBtnState(BtnState::NotLoad);
}

FileWidget::~FileWidget()
{
    delete _ui;
}

const FileShp &FileWidget::getFile()
{
    return _file;
}

void FileWidget::deleteLaterPerId(const quint64 fileId)
{
    if (_file->fileId() == fileId)
    {
        emit willDestroyed();
        deleteLater();
    }
}

void FileWidget::setBtnState(const FileWidget::BtnState state)
{
    _ui->btnSave->setVisible(false);

    switch(state)
    {
        case BtnState::NotLoad:
        {
            _ui->indicator->setVisible(false);
            _ui->btnOpen->setVisible(false);
            break;
        }
        case BtnState::Downloaded:
        {
            _ui->btnDownload->setVisible(false);
            _ui->indicator->setVisible(true);
            _ui->indicator->startAnimation();
            break;
        }
        case BtnState::DownloadFinish:
        {
            _ui->indicator->setVisible(false);
            _ui->btnDownload->setVisible(false);
            _ui->btnOpen->setVisible(true);
            break;
        }
        default:
            break;
    }
}

void FileWidget::downloadFile()
{
    auto downloadThread = new QThread(this);
    auto downloader = new FileDownloader();
    downloader->moveToThread(downloadThread);

    connect(downloadThread, &QThread::finished, downloader, &FileDownloader::deleteLater);
    connect(downloadThread, &QThread::finished, downloadThread, &QThread::deleteLater);
    connect(this, &FileWidget::startDownload, downloader, &FileDownloader::downloadFile);
    connect(downloader, &FileDownloader::finish, this, &FileWidget::downloadFinish);
    connect(downloader, &FileDownloader::error, this, &FileWidget::downloadError);

    downloadThread->start();

    emit startDownload(_file);
    setBtnState(BtnState::Downloaded);
}

void FileWidget::openFile()
{
    const auto result = QDesktopServices::openUrl(_file->downloadedUrl());
    if (!result)
        qDebug() << "Do not open file" << _file->downloadedUrl();
}

void FileWidget::saveFile()
{

}

void FileWidget::deleteFile()
{
    emit removeFile(_file->fileId(), _file->url().toString());
    emit willDestroyed();

    deleteLater();
}

void FileWidget::downloadFinish()
{
    setBtnState(BtnState::DownloadFinish);
}

void FileWidget::downloadError(const QString &error)
{
    qDebug() << error;
}
