#pragma once


namespace file_attach
{

    class File
    {
        friend class QSharedPointer<File>;

    private:
        File(const quint64 id, const QUrl& url, const QString& origName);

    public:
        const quint64 fileId() const;
        const QUrl url() const;
        QString originalFileName();

        QUrl downloadedUrl();
        void setDownloadedUrl(const QUrl &url);

    private:
        quint64 _id;
        QUrl _url;
        QString _origName;
        QUrl _downloadedUrl;
    };

    typedef QSharedPointer<File> FileShp;

}

Q_DECLARE_METATYPE(file_attach::FileShp);
