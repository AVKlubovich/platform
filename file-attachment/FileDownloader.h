#pragma once


namespace file_attach
{

    class File;
    typedef QSharedPointer<File> FileShp;


    class FileDownloader : public QObject
    {
        Q_OBJECT

    public:
        FileDownloader(QObject *parent = nullptr);
        ~FileDownloader() = default;

    public slots:
        void downloadFile(FileShp file);

    private slots:
        void downloadReadyRead();
        void downloadFinished();
        void slotError(QNetworkReply::NetworkError error);
        void slotSslErrors(QList<QSslError> errors);

    private:
        void prepareOutputFile();

    signals:
        void finish();
        void error(const QString& err);

    private:
        QNetworkAccessManager *_manager;
        FileShp _file;

        QFile *_outputFile;
    };

    typedef QSharedPointer<FileDownloader> FileDownloaderShp;

}
