#pragma once


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace Ui
{
    class FileAttachmentWidget;
}

namespace file_attach
{

    class FileWidget;

    class FileAttachmentWidget : public QWidget
    {
        Q_OBJECT

    public:
        FileAttachmentWidget(QWidget* parent = nullptr);
        ~FileAttachmentWidget();

        void loadFilesList(const quint64 idComplaint);
        void onLoadFilesList(network::ResponseShp response);

        void uploadAllFiles(const quint64 idComplaint);

    signals:
        void filesCountChanged(const quint64);

        void removeFileWidget(const quint64);
        void removeAllWidgets();

        void finishUpload();
        void signalClose();

    private slots:
        void addFile();
        void onAddFile(QNetworkReply *reply);

        void removeFile(const quint64 fileId, const QString &fileUrl);
        void onRemoveFile(QNetworkReply *reply);
        void onRemoveWidget();

        void sendToPhpAddFile(QHttpMultiPart *multiPart);
        void sendToPhpRemoveFile(QHttpMultiPart *multiPart);

    private:
        void createFileWidget(const QVariantMap& fileMap);
        void checkFilesCount();

    private:
        Ui::FileAttachmentWidget *_ui;
        quint64 _idComplaint = 0;
        network::RequestsManagerShp _requestsManager;

        QVector<FileWidget *> _widgets;

        quint64 _uploadCounter = 0;
    };

}
