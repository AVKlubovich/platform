#pragma once


namespace Ui
{
    class FileWidget;
}

namespace file_attach
{

    class File;
    typedef QSharedPointer<File> FileShp;

    class FileDownloader;
    typedef QSharedPointer<FileDownloader> FileDownloaderShp;


    class FileWidget : public QWidget
    {
        Q_OBJECT

    public:
        FileWidget(const FileShp& file, QWidget* parent = nullptr);
        ~FileWidget();

        const FileShp& getFile();

    public slots:
        void deleteLaterPerId(const quint64 fileId);

    signals:
        void startDownload(file_attach::FileShp);
        void removeFile(const quint64, const QString&);

        void willDestroyed();

    private:
        enum BtnState{ NotLoad, Downloaded, DownloadFinish };
        void setBtnState(const BtnState state);
        void downloadFile();
        void openFile();
        void saveFile();
        void deleteFile();

    private slots:
        void downloadFinish();
        void downloadError(const QString& error);

    private:
        Ui::FileWidget*_ui;

        FileShp _file;
    };

}
