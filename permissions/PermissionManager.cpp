﻿#include "Common.h"
#include "PermissionManager.h"

#include "Permission.h"
#include "PermissionNode.h"


using namespace permissions;

PermissionManager::PermissionManager()
    : _map(QSharedPointer<Map>::create())
{
}

const QSharedPointer<const PermissionManager::Map> PermissionManager::map() const
{
    return _map;
}

bool PermissionManager::testPermission(qint64 userId, const QByteArray& commandName, qint64 depId) const
{
    auto map = _map;    // Copy for thread-safety

    auto node = map->value(userId);
    if (node.isNull())
        return false;

    auto permission = node->permissions().value(commandName);
    if (permission.isNull())
        return false;

    const auto enabled = permission->departments().contains(depId);
    return enabled;
}

QSet<qint64> PermissionManager::getUserDepartments(qint64 userId, const QByteArray& commandName) const
{
    auto departmentsMap = _departmentsMap;
    auto departments = departmentsMap->value(userId).value(commandName);

    return departments;
}

void PermissionManager::beginUpdate()
{
    Q_ASSERT(_temp.isNull());

    _temp = QSharedPointer<TempMap>::create();
    _tempDepartmentsMap = QSharedPointer<UserDepartments>::create();
}

void PermissionManager::endUpdate()
{
    Q_ASSERT(!_temp.isNull());

    // TODO: copy content
    auto copy = QSharedPointer< Map >::create();
    for (auto& item : *_temp)
    {
        const auto key = item->departmentId();
        const auto value = item.constCast<const PermissionNode>();
        copy->insert(key, value);
    }

    _map = copy;
    _departmentsMap = _tempDepartmentsMap;
    _temp.reset();
    _tempDepartmentsMap.reset();
}

void PermissionManager::setPermission(qint64 userId, const QByteArray& commandName, qint64 depId)
{
    Q_ASSERT(isUpdating());

    auto node = _temp->value(userId);
    if (node.isNull())
        node = QSharedPointer< PermissionNode >::create(userId);

    auto permission = node->permissions().value(commandName).constCast<Permission>();
    if (permission.isNull())
        permission = QSharedPointer< Permission >::create(commandName);

    permission->departments() << depId;

    node->permissions()[commandName] = permission;
    _temp->insert(userId, node);

    (*_tempDepartmentsMap)[userId][commandName] << depId;
}

bool PermissionManager::isUpdating() const
{
    return !_temp.isNull();
}
