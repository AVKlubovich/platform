﻿#include "Common.h"
#include "PermissionNode.h"


namespace permissions
{

    PermissionNode::PermissionNode( qint64 id )
        : _departmentId( id )
    {
    }

    qint64 PermissionNode::departmentId() const
    {
        return _departmentId;
    }

    const PermissionNode::PermissionMap& PermissionNode::permissions() const
    {
        return _permissions;
    }

    PermissionNode::PermissionMap& PermissionNode::permissions()
    {
        return _permissions;
    }

}
