﻿#pragma once


namespace permissions
{

    class PermissionManager;
    typedef QSharedPointer<PermissionManager> PermissionManagerShp;

     class PermissionDownloader
     {
     public:
         explicit PermissionDownloader() = default;
         ~PermissionDownloader() = default;

         bool downloadePermissions(PermissionManagerShp& permissionManager);
     };

}
