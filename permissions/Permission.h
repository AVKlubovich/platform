﻿#pragma once


namespace permissions
{

     class Permission
     {
     public:
         explicit Permission(const QByteArray& signature);
         explicit Permission(const QByteArray &signature, const QString& description);
         ~Permission() = default;

         const QByteArray& signature() const;
         const QSet<qint64>& departments() const;
         const QString& description() const;

         QSet<qint64>& departments();

     private:
         Q_DISABLE_COPY( Permission );

     private:
         const QByteArray _signature;
         const QString _description;
         QSet<qint64> _departments;
     };

}
