﻿#pragma once


namespace permissions
{

    class PermissionNode;


    class PermissionManager
    {
    public:
        using Map = QMap<qint64, QSharedPointer<const PermissionNode>>; // key - id_department
        using DepartmentsMap = QMap<QByteArray, QSet<qint64>>; // key - command name; value set of id_departmends
        using UserDepartments = QMap<qint64, DepartmentsMap>; // key - id_user;

    private:
        using TempMap = QMap< qint64, QSharedPointer< PermissionNode > >; // key - id_department

    public:
        PermissionManager();
        ~PermissionManager() = default;

        const QSharedPointer<const Map> map() const; // Thread-safe
        bool testPermission(qint64 userId, const QByteArray& commandName, qint64 depId) const;

        QSet<qint64> getUserDepartments(qint64 userId, const QByteArray& commandName) const;

        void beginUpdate();
        void endUpdate();
        void setPermission(qint64 userId, const QByteArray& commandName, qint64 depId);
        
    private:
        bool isUpdating() const;

    private:
        QSharedPointer<Map> _map;
        QSharedPointer<TempMap> _temp;

        QSharedPointer<const UserDepartments> _departmentsMap;
        QSharedPointer<UserDepartments> _tempDepartmentsMap;
    };

    typedef QSharedPointer<PermissionManager> PermissionManagerShp;

}
