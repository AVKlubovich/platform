﻿#pragma once


namespace permissions
{

    class Permission;


    class PermissionNode : public QEnableSharedFromThis<PermissionNode>
    {
    public:
        using PermissionMap = QMap<QByteArray, QSharedPointer<const Permission>>;

    public:
        explicit PermissionNode(qint64 departmentId);

        qint64 departmentId() const;
        const PermissionMap& permissions() const;

        PermissionMap& permissions();

    private:
        const qint64 _departmentId;
        PermissionMap _permissions;
    };

}
