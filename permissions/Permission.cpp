﻿#include "Common.h"
#include "Permission.h"


namespace permissions
{

    Permission::Permission(const QByteArray& signature)
        : _signature(signature)
    {
    }

    Permission::Permission(const QByteArray &signature, const QString &description)
        : _signature(signature)
        , _description(description)
    {

    }

    const QByteArray& Permission::signature() const
    {
        return _signature;
    }

    const QSet<qint64>& Permission::departments() const
    {
        return _departments;
    }

    const QString &Permission::description() const
    {
        return _description;
    }

    QSet<qint64>& Permission::departments()
    {
        return _departments;
    }

}
