﻿#include "Common.h"
#include "PermissionDownloader.h"

#include "PermissionManager.h"

#include "../database/DBManager.h"
#include "../database/DBWraper.h"
#include "../database/DBHelpers.h"


using namespace permissions;

bool PermissionDownloader::downloadePermissions(PermissionManagerShp& permissionManager)
{
    auto& dbWraper = database::DBManager::instance().getDBWraper();

    const auto permissionsStr = QString(
        "SELECT * "
        "FROM users_schema.permissions "
        "INNER JOIN users_schema.commands "
        "ON (users_schema.commands.id = users_schema.permissions.id_command)"
        );
    auto permissionsQuery = dbWraper->query();
    permissionsQuery.prepare(permissionsStr);

    const auto permissionsResult = permissionsQuery.exec();
    if (!permissionsResult)
    {
        qDebug() << "db error" << "permissions load is failed" << permissionsQuery.lastError().text();
        qDebug() << permissionsQuery.lastQuery();
        return false;
    }

    auto resultList = database::DBHelpers::queryToVariant(permissionsQuery);

    permissionManager->beginUpdate();
    for (auto& value : resultList)
    {
        permissionManager->setPermission(value.toMap()["id_user"].value<qint64>(),
                                         value.toMap()["name"].value<QByteArray>(),
                                         value.toMap()["id_department"].value<qint64>());
    }
    permissionManager->endUpdate();

    return true;
}
