﻿#include "Common.h"
#include "UserPermission.h"

#include "database/DBManager.h"
#include "database/DBWraper.h"
#include "database/DBHelpers.h"


using namespace permissions;

// client
quint64 UserPermission::idUser() const
{
    return _idUser;
}

// client
void UserPermission::setIdUser(const quint64 &idUser)
{
    _idUser = idUser;
}

// client
QMap<int, QSet<int> > UserPermission::mapPermissions() const
{
    return _mapPermissions;
}

// client
void UserPermission::setPermissions(QList<QVariant> listValues)
{
    _mapPermissions.clear();
    for (const auto& value : listValues)
    {
        const auto& map = value.toMap();

        _idUser = map["id_user"].toLongLong();
        auto idDepartment = map["id_department"].toLongLong();
        auto idCommand = map["id_command"].toLongLong();

        QSet <int> set;
        if (_mapPermissions.contains(idDepartment))
            set = _mapPermissions.value(idDepartment);

        set.insert(idCommand);
        _mapPermissions[idDepartment] = set;
    }
}

// server
bool UserPermission::setUsersPermissions()
{
    _mapUsersPermissions.clear();

    auto& dbWraper = database::DBManager::instance().getDBWraper();

    const auto permissionsStr = QString(
        "SELECT * "
        "FROM users_schema.permissions "
        );
    auto permissionsQuery = dbWraper->query();
    permissionsQuery.prepare(permissionsStr);

    const auto permissionsResult = permissionsQuery.exec();
    if (!permissionsResult)
    {
        qDebug() << "db error" << "permissions load is failed" << permissionsQuery.lastError().text();
        qDebug() << permissionsQuery.lastQuery();
        return false;
    }

    auto resultList = database::DBHelpers::queryToVariant(permissionsQuery);

    for (auto userPermission : resultList)
    {
        auto mapUserPermission = userPermission.toMap();

        auto idUser       = mapUserPermission["id_user"].toLongLong();
        auto idDepartment = mapUserPermission["id_department"].toLongLong();
        auto idCommand    = mapUserPermission["id_command"].toLongLong();

        QMap < int, QSet<int> > mapPermission;
        QSet < int > set;

        if (_mapUsersPermissions.contains(idUser))
            mapPermission = _mapUsersPermissions.value(idUser);

        if (mapPermission.contains(idDepartment))
            set = mapPermission.value(idDepartment);

        set.insert(idCommand);
        mapPermission[idDepartment] = set;
        _mapUsersPermissions[idUser] = mapPermission;
    }

    return true;
}

QList<int> UserPermission::getListDepartments()
{
    return _mapPermissions.keys();
}

bool UserPermission::getUserAdminComplaints()
{
    for (auto department = _mapPermissions.begin(); department != _mapPermissions.end(); ++department)
    {
        auto list = department.value().toList();
        if (list.contains(admin_complaints))
            return true;
    }
    return false;
}

// server
QMap<int, QSet<int>> UserPermission::findPermissionsUser(const quint64 idUser)
{
    QMap < int, QSet<int> > userPermission;
    if (_mapUsersPermissions.contains(idUser))
        userPermission = _mapUsersPermissions[idUser];

    return userPermission;
}

// server
QList<int> UserPermission::listDepartmentUser(const quint64 idUser)
{
    QList < int > listUserDepartments;
    if (_mapUsersPermissions.contains(idUser))
        listUserDepartments = _mapUsersPermissions[idUser].keys();

    return listUserDepartments;
}

// server
QStringList UserPermission::stringlistDepartmentUser(const quint64 idUser)
{
    QStringList listUserDepartments;
    for (auto department : listDepartmentUser(idUser))
        listUserDepartments << QString::number(department);
    return listUserDepartments;
}

// server
QVariantList UserPermission::existCommand(const quint64 idUser, const QString &nameCommand)
{
    const auto permissionsStr = QString(
        "SELECT p.* FROM users_schema.permissions AS p "
        " INNER JOIN users_schema.commands AS c ON (p.id_command = c.id)"
        " WHERE p.id_user = %1 AND c.permission_description = '%2'"
        ).arg(idUser).arg(nameCommand);

    auto& dbWraper = database::DBManager::instance().getDBWraper();
    auto permissionsQuery = dbWraper->query();
    permissionsQuery.prepare(permissionsStr);

    const auto permissionsResult = permissionsQuery.exec();
    if (!permissionsResult)
    {
        qDebug() << "db error" << "permissions load is failed" << permissionsQuery.lastError().text();
        qDebug() << permissionsQuery.lastQuery();
        return QVariantList();
    }

    const auto listResult = database::DBHelpers::queryToVariant(permissionsQuery);
    QVariantList listDepartment;
    for (auto element : listResult)
        listDepartment.append(element.toMap()["id_department"]);

    return listDepartment;
}

// client
QStringList UserPermission::getDepartmentsUser()
{
    QStringList departments;
    for (auto department = _mapPermissions.begin(); department != _mapPermissions.end(); ++department)
        departments << QString::number(department.key());
    return departments;
}

// client
QStringList UserPermission::getDepartmentsDriversUser()
{
    QStringList departments;
    for (auto department = _mapPermissions.begin(); department != _mapPermissions.end(); ++department)
    {
        auto list = department.value().toList();
        if (list.contains(get_select_drivers))
            departments << QString::number(department.key());
    }
    return departments;
}

// client
QStringList UserPermission::getDepartmentsOperatorsUser()
{
    QStringList departments;
    for (auto department = _mapPermissions.begin(); department != _mapPermissions.end(); ++department)
    {
        auto list = department.value().toList();
        if (list.contains(get_select_operators))
            departments << QString::number(department.key());
    }
    return departments;
}

// client
QStringList UserPermission::getDepartmentsLogistsUser()
{
    QStringList departments;
    for (auto department = _mapPermissions.begin(); department != _mapPermissions.end(); ++department)
    {
        auto list = department.value().toList();
        if (list.contains(get_select_logists))
            departments << QString::number(department.key());
    }
    return departments;
}

// server
bool UserPermission::isQATUser(const quint64 idUser)
{
    const auto& departments = listDepartmentUser(idUser);
    for (const auto& department : departments)
    {
        if (department == 1)
            return true;
    }

    return false;
}

// server
bool UserPermission::isAdmin(const quint64 idUser)
{
    return isQATUser(idUser);
}
