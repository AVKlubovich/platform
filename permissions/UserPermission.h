﻿#pragma once

#include "utils/BaseClasses/Singleton.h"


namespace permissions
{

    class UserPermission
        : public utils::Singleton<UserPermission>
    {
        friend class utils::Singleton<UserPermission>;

    private:
        UserPermission() = default;
    public:
        ~UserPermission() = default;

        enum commands
        {
            save_permissions = 1,
            get_select_clients,
            insert_complaint,
            get_select_complaints,
            taken_info_operation,
            update_complaint,
            get_select_drivers,
            get_select_logists,
            get_select_operators,
            change_in_status,
            get_select_reviews,
            createUser,
            delete_user,
            get_user_list,
            updateUser,
            download_file,
            fetch_new_data,
            get_complaint_attachments,
            get_new_db_data,
            login,
            upload_file,
            admin_complaints = 23,
            statistic = 25,
            complaint_challenge = 26
        };

        quint64 idUser() const;
        void setIdUser(const quint64 &idUser);

        QMap<int, QSet<int>> mapPermissions() const;

        void setPermissions(QList <QVariant> listValues);
        bool setUsersPermissions();
        QList <int> getListDepartments();
        bool getUserAdminComplaints();

        QMap <int, QSet<int>> findPermissionsUser(const quint64 idUser);
        QList <int> listDepartmentUser(const quint64 idUser);
        QStringList stringlistDepartmentUser(const quint64 idUser);

        QVariantList existCommand(const quint64 idUser, const QString & nameCommand);

        QStringList getDepartmentsUser();
        QStringList getDepartmentsDriversUser();
        QStringList getDepartmentsOperatorsUser();
        QStringList getDepartmentsLogistsUser();

        bool isAdmin(const quint64 idUser);
        bool isQATUser(const quint64 idUser);

    private:
        quint64 _idUser;
        QSet <int> _idDepartment;
        QSet <int> _idCommand;
        QMap <int, QSet<int>> _mapPermissions; // Qmap < department, QSet <commands> >
        QMap <quint64, QMap<int, QSet<int>>> _mapUsersPermissions; // QMap < idUser, QMap< department, QSet <commands> > >
    };

}
