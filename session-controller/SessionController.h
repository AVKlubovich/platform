﻿#pragma once


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;

    class ResponseServerError;
    typedef QSharedPointer<ResponseServerError> ResponseServerErrorShp;
}

namespace session_controller
{

    class Session;
    typedef QSharedPointer<Session> SessionShp;

    class LoginDialog;
    typedef QSharedPointer<LoginDialog> LoginDialogShp;

    class SessionController : public QObject
    {
        Q_OBJECT

    public:
        explicit SessionController(SessionShp& session);

#ifdef QT_DEBUG
        void setDefaultCredentials(const QString& login, QString& password);
#endif

        //Run user login dialog window and get its credentials (e.g. login/password).
        //Return true if credentias successfully entered and processed, otherwise return false.
        bool requestUserCredentials();

    signals:
        void ready();

    private slots:
        void loginAccepted();
        void responseLoginReady(network::ResponseShp response);
        void responseLoginFailed(network::ResponseServerErrorShp response);

    private:
        LoginDialogShp _loginDialog;
        network::RequestsManagerShp _requestsManager;
        SessionShp _session;
    };

    typedef QSharedPointer<SessionController> SessionControllerShp;

}
