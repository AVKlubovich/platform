﻿#include "Common.h"
#include "SessionController.h"

#include "LoginDialog/LoginDialog.h"

#include "Session.h"

#include "network-core/RequestsManager/RequestsManager.h"

#include "network-core/RequestsManager/Users/RequestLogin.h"
#include "network-core/RequestsManager/Users/ResponseLogin.h"

#include "permissions/UserPermission.h"


using namespace session_controller;

SessionController::SessionController(SessionShp& session)
    : QObject(nullptr)
    , _requestsManager(new network::RequestsManager)
    , _session(session)
{
    Q_ASSERT(session);

    _loginDialog = LoginDialogShp::create();

    connect(_loginDialog.data(), &LoginDialog::loginAccepted,
            this, &SessionController::loginAccepted);

    connect(_requestsManager.data(), static_cast<void (network::RequestsManager::*)(QSharedPointer<network::Response>)>(&network::RequestsManager::responseReady),
            this, &SessionController::responseLoginReady);
}

#ifdef QT_DEBUG
void SessionController::setDefaultCredentials(const QString& login, QString& password)
{
    _loginDialog->setDefaultCredentials(login, password);
}
#endif

bool SessionController::requestUserCredentials()
{
    const auto res = _loginDialog->requestCredentials();
    return res;
}

void SessionController::loginAccepted()
{
    const auto& userLogin = _loginDialog->getLogin();
    const auto& userPassword = _loginDialog->getPassword();

    // NOTE: Send login request to server
    network::RequestLogin loginRequest(userLogin, userPassword);
    _requestsManager->sendRequest(loginRequest);
}

void SessionController::responseLoginReady(network::ResponseShp response)
{
    QString msg;
    // NOTE: check success
    network::ResponseLoginShp responseLogin = qSharedPointerDynamicCast<network::ResponseLogin>(response);
    const network::ResponseLogin::Status status = static_cast<network::ResponseLogin::Status>(responseLogin->status());

    if (responseLogin)
    {
        if (status == network::ResponseLogin::StatusSuccess)
        {
            if (!responseLogin->userName().isEmpty())
                _session->setUserName(responseLogin->userName());
            else
                _session->setUserName(_loginDialog->getLogin());
            _session->setLogin(_loginDialog->getLogin());
            _session->setPassword(_loginDialog->getPassword());
            _session->setUserId(responseLogin->userId());
            _session->setUserRights(responseLogin->userRights());

            permissions::UserPermission::instance().setPermissions(responseLogin->userRights());

            _loginDialog->accept();
            return;
        }
        else if (status == network::ResponseLogin::StatusRejected)
        {
            msg = tr("Login error. Check login and password.");
        }
        else if (status == network::ResponseLogin::statusNoPermissions)
        {
            msg = tr("Do not have permissions");
        }
        else
        {
            msg = tr("Unknown error.");
        }
    }

    // NOTE: check errors
    network::ResponseServerErrorShp responseServerError = qSharedPointerDynamicCast<network::ResponseServerError>(response);
    if (responseServerError)
        msg = responseServerError->getErrorMessage();

    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.exec();

    // NOTE: retry to login
    _loginDialog->requestCredentialsRetry();
}

void SessionController::responseLoginFailed(network::ResponseServerErrorShp response)
{
    QMessageBox msgBox;
    msgBox.setText(response->getErrorMessage());
    msgBox.exec();
    // NOTE: retry to login
    _loginDialog->requestCredentialsRetry();
}
