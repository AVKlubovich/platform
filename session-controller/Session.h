﻿#pragma once


namespace session_controller
{

    class Session
    {
    public:
        explicit Session();

        void setLogin(const QString& login);
        const QString& login() const;

        void setUserId(const quint64 userId);
        const quint64 userId() const;

        void setUserName(const QString& userName);
        const QString userName() const;

        void setPassword(const QString& password);
        const QString password() const;

        void setUserRights(const QList<QVariant> &userRights);
        const QList<QVariant> userRights() const;

    private:
        const QUuid _uuid;
        quint64 _id;
        QString _login;
        QString _userName;
        QString _password;
        QList<QVariant> _userRights;
    };

}
