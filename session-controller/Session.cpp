﻿#include "Common.h"
#include "Session.h"


using namespace session_controller;

Session::Session()
    : _uuid(QUuid::createUuid())
    , _id(0)
//    , _userRights(0)
{
}

void Session::setLogin(const QString& login)
{
    _login = login;
}

const QString&Session::login() const
{
    return _login;
}

void Session::setUserId(const quint64 userId)
{
    _id = userId;
}

const quint64 Session::userId() const
{
    return _id;
}

void Session::setUserName(const QString & userName)
{
    _userName = userName;
}
const QString Session::userName() const
{
    return _userName;
}

void Session::setPassword(const QString& password)
{
    _password = password;
}

const QString Session::password() const
{
    return _password;
}

const QList<QVariant> Session::userRights() const
{
    return _userRights;
}

void Session::setUserRights(const QList<QVariant> &permissions)
{
    _userRights = permissions;
}
