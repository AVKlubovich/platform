﻿#pragma once


namespace Ui
{
    class LoginDialog;
}

namespace session_controller
{
    class LoginDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit LoginDialog(QWidget *parent = nullptr);
        ~LoginDialog();

        //Run dialog and request user credentials (login, password).
        bool requestCredentials();
        void requestCredentialsRetry();
        QString getLogin();
        QString getPassword();

#ifdef QT_DEBUG
        void setDefaultCredentials(const QString& login, const QString& password);
#endif

    signals:
        void loginAccepted();

    private slots:
        void loginEditChanged(const QString& str);
        void acceptedButtonClicked();

    private:
        Ui::LoginDialog *_ui;
    };

    typedef QSharedPointer<LoginDialog> LoginDialogShp;
}
