﻿#pragma once


namespace database
{

    class DatabaseCreator
    {
    public:
        DatabaseCreator() = default;
        ~DatabaseCreator() = default;

        bool createDatabase(const QSharedPointer<QSqlDatabase> dbConnection,
                            const QString& dbScript = QString());
    };

}
