﻿#pragma once


namespace database
{

    class DBManager;

    class DBWraper
    {
        friend class QSharedPointer<DBWraper>;

    private:
        DBWraper(QSharedPointer<QSqlDatabase> dbConnection);
    public:
        ~DBWraper();

        const bool startTransaction();
        const bool commit();
        const bool rollback();

        const bool execQuery(const QString& queryStr);
        const bool execQueries(const QStringList queries);

        QSqlQuery query();
        const bool execQuery(QSqlQuery& query);

    private:
        QSharedPointer<QSqlDatabase> _dbConnection;
        bool _transactionStarted = false;
    };

    typedef QSharedPointer<DBWraper> DBWraperShp;

}
