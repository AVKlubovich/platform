﻿#include "Common.h"
#include "DBWraper.h"

#include "DBManager.h"


using namespace database;

DBWraper::DBWraper(QSharedPointer<QSqlDatabase> dbConnection)
    : _dbConnection(dbConnection)
{
    Q_ASSERT(!_dbConnection.isNull());
}

DBWraper::~DBWraper()
{
    Q_ASSERT(!_dbConnection.isNull());

    if (_transactionStarted)
    {
        if (!_dbConnection->commit())
        {
            _dbConnection->rollback();
        }
    }
}

const bool DBWraper::startTransaction()
{
    Q_ASSERT(!_dbConnection.isNull());
    _transactionStarted = _dbConnection->transaction();
    return _transactionStarted;
}

const bool DBWraper::commit()
{
    Q_ASSERT(!_dbConnection.isNull());
    _transactionStarted = !_dbConnection->commit();
    return !_transactionStarted;
}

const bool DBWraper::rollback()
{
    Q_ASSERT(!_dbConnection.isNull());
    _transactionStarted = false;
    return _dbConnection->rollback();
}

const bool DBWraper::execQuery(const QString& queryStr)
{
    QSqlQuery query(*(_dbConnection.data()));
    return query.exec(queryStr);
}

const bool DBWraper::execQueries(const QStringList queries)
{
    if (_dbConnection.isNull())
    {
        Q_ASSERT(!_dbConnection.isNull());
        return false;
    }

    if (!_dbConnection->isOpen())
        return false;

    if (!startTransaction())
        return false;

    _transactionStarted = true;

    for (const auto& queryStr : queries)
    {
        QSqlQuery query(*(_dbConnection.data()));
        const auto result = query.exec(queryStr);
        if (!result)
        {
            // TODO: need to add log
            qDebug() << query.lastError();
            qDebug() << query.executedQuery();
            rollback();
            return false;
        }
    }

    if (!commit())
    {
        rollback();
        return false;
    }

    _transactionStarted = false;

    return true;
}

QSqlQuery DBWraper::query()
{
    Q_ASSERT(!_dbConnection.isNull());

    QSqlQuery newQuery(*(_dbConnection.data()));
    return newQuery;
}

const bool DBWraper::execQuery(QSqlQuery& query)
{
    Q_ASSERT(!_dbConnection.isNull());

    if (!startTransaction())
        return false;

    const auto result = query.exec();
    if (!result)
    {
        rollback();
        return false;
    }

    if (!commit())
    {
        rollback();
        return false;
    }

    return true;
}
