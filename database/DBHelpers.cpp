﻿#include "Common.h"
#include "DBHelpers.h"

#include "DBWraper.h"


#define SQLITE_MAX_FUNC_ARGS 100
#define SQLITE_MAX_VARIABLE_NUMBER 999

using namespace database;


namespace
{
    template<typename T>
    QList<T> queryToVariantT(QSqlQuery& query)
    {
        static_assert(std::is_convertible<T, QVariant>::value , "T must be convertable to QVariant");

        QList<T> resultList;

        const auto& record = query.record();
        QStringList fields;
        fields.reserve(record.count());
        for (auto i = 0; i < record.count(); i++)
            fields << record.fieldName(i);

        while (query.next())
        {
            QVariantMap item;
            for (auto i = 0; i < record.count(); i++)
            {
                const auto& key = fields[i];
                const auto& value = query.value(i);

                item[key] = value;
            }
            resultList << item;
        }

        return resultList;
    }
}

QList<QVariantMap> DBHelpers::queryToVariantMap(QSqlQuery& query)
{
    return queryToVariantT<QVariantMap>(query);
}

QList<QVariant> DBHelpers::queryToVariant(QSqlQuery& query)
{
    return queryToVariantT<QVariant>(query);
}

void DBHelpers::variantMapToJson(QJsonObject& json, const QVariantMap& data)
{
    for (auto it = data.begin(); it != data.end(); ++it)
        json[it.key()] = it.value().toString();
}

QList<QSqlQuery> DBHelpers::createInsertQueries(const QVariantMap& inData, const DBWraperShp& wraper)
{
    QList<QSqlQuery> queriesList;
    for (auto it = inData.begin(); it != inData.end(); ++it)
    {
        const QString& tableName = it.key();

        const QVariantList& insertData = it.value().toList();

        if (insertData.isEmpty())
            continue;

        const QVariantMap& insertMap = insertData.first().toMap();
        const QString& insertKeys = insertMap.keys().join(",");
        const quint8 keyCount = insertMap.keys().count();

        QVariantMap listValuesForBind;
        QStringList keysListTemplate;
        quint64 counter = 0;
        for (const auto& insertValues : insertData)
        {
            const QVariantMap insertValuesMap = insertValues.toMap();

            const QStringList listKeys = insertValuesMap.keys();

            QStringList listKeysForBind;
            for (auto key : listKeys)
            {
                const auto keyValue = QString(":%1_%2").arg(key).arg(counter);
                listKeysForBind << keyValue;
                listValuesForBind.insert(keyValue,
                                         insertValuesMap[key]);
            }

            QString templateStr = QString("(%1)").arg(listKeysForBind.join(","));
            keysListTemplate << templateStr;

            ++counter;
        }

        while (!keysListTemplate.isEmpty())
        {
            QStringList keysListTemplatePart;
            int i = 0;
            while (i < SQLITE_MAX_FUNC_ARGS
                   && ((i + 1) * keyCount) < SQLITE_MAX_VARIABLE_NUMBER)
            {
                keysListTemplatePart << keysListTemplate.takeFirst();
                if (keysListTemplate.isEmpty())
                    break;

                ++i;
            }

            const QString& queryStr = QString(
                "INSERT OR REPLACE INTO %1 "
                "(%2) "
                "VALUES "
                "%3"
                )
                .arg(tableName)
                .arg(insertKeys)
                .arg(keysListTemplatePart.join(","));

            auto query = wraper->query();
            query.prepare(queryStr);

            for (auto it = listValuesForBind.begin(); it != listValuesForBind.end(); ++it)
                query.bindValue(it.key(), it.value());

            queriesList << query;
        }
    }

    return queriesList;
}

QList<QSqlQuery> DBHelpers::createInsertQueriesPerTable(const QVariantList& inData, const DBWraperShp& wraper)
{
//    QMap<QString, QString> queriesMap;
    QMap<QString, QList<QVariantMap>> dataMap;
    for (const auto item : inData)
    {
        const auto jobj = item.toMap();
        if (!jobj.contains("table") ||
            !jobj.contains("data"))
            continue;

        const auto table = jobj["table"].toString();
        const auto data = jobj["data"].toMap();
        if (data.count() == 0
            || table.isEmpty())
            continue;

        QList<QVariantMap>& dataForTable = dataMap[table];
        dataForTable << data;

//        auto listKeys = data.keys();
//        const auto stringKeys = QStringList(listKeys).join(", ");

//        QStringList listValues;
//        for (auto key : listKeys)
//            listValues.append("'" + data.value(key).toString() + "'");

//        const auto stringValues = listValues.join(", ");
//        if (!queriesMap.contains(table))
//        {
//            QString sqlQuery = QString("INSERT INTO %1 (%2) VALUES (%3)")
//                               .arg(table)
//                               .arg(stringKeys)
//                               .arg(stringValues);
//            queriesMap[table] = sqlQuery;
//        }
//        else
//        {
//            QString sqlQuery = queriesMap[table];
//            QString newSqlQuery = QString("%1, (%2)").arg(sqlQuery).arg(stringValues);
//            queriesMap[table] = newSqlQuery;
//        }
    }

//    const auto sqlList = createInsertQueries(dataMap, wraper);
//    return sqlList;
    return QList<QSqlQuery>();
}
