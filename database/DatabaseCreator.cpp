﻿#include "Common.h"
#include "DatabaseCreator.h"


using namespace database;

bool DatabaseCreator::createDatabase(const QSharedPointer<QSqlDatabase> dbConnection, const QString& dbScript)
{
    if (!dbConnection->isOpen())
        return false;

    QStringList queriesList = dbScript.split(";", QString::SkipEmptyParts);
    if (queriesList.isEmpty())
    {
        QFile script(":/dbscripts/db_create.sql");
        if (!script.open(QFile::ReadOnly))
        {
            qDebug() << script.exists();
            qDebug() << script.error();
            qDebug() << script.errorString();
            Q_ASSERT(false);
        }

        const QString newDbScript = script.readAll();
        queriesList = newDbScript.simplified().split(";", QString::SkipEmptyParts);
        if (queriesList.isEmpty())
        {
            qWarning() << "Database script is empty";
            return false;
        }
    }

    const auto resTr = dbConnection->transaction();
    if (!resTr)
    {
        qDebug() << dbConnection->lastError();
        Q_ASSERT(false);
        return false;
    }

    for (const auto& queryStr : queriesList)
    {
        QSqlQuery query(*(dbConnection.data()));
        const auto result = query.exec(queryStr);
        if (!result)
        {
            qDebug() << query.lastError();
            qDebug() << query.executedQuery();
            const auto resRollback = dbConnection->rollback();
            if (!resRollback)
            {
                qDebug() << dbConnection->lastError().text();
                Q_ASSERT(false);
                return false;
            }
            return false;
        }
    }

    if (!dbConnection->commit())
    {
        qDebug() << dbConnection->lastError().text();
        const auto resRollback = dbConnection->rollback();
        if (!resRollback)
        {
            qDebug() << dbConnection->lastError().text();
            Q_ASSERT(false);
            return false;
        }
        return false;
    }

    return true;
}
