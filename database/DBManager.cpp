﻿#include "Common.h"
#include "DBManager.h"

#include "DBWraper.h"


using namespace database;

DBManager::DBManager()
    : QObject(nullptr)
{
}

DBManager::~DBManager()
{
    QList<QObject *> keys;
    {QReadLocker rLocker(&_rwLock);
        keys = _threadConnections.keys();
    }

    for(int i = 0; i < keys.count(); i++)
        removeConnection(keys[i]);

    {QWriteLocker wLocker(&_rwLock);
        _threadConnections.clear();
    }
}

void DBManager::initSettings(const DBSettings & settings)
{
    _settings = settings;
}

const QSharedPointer<QSqlDatabase> DBManager::getConnection()
{
    if (this == nullptr)
        return QSharedPointer<QSqlDatabase>(nullptr);

    auto currentThread = QThread::currentThread();

    {
        QReadLocker rLocker(&_rwLock);
        auto it = _threadConnections.find(currentThread);
        if (it != _threadConnections.end())
        {
            auto dbConnection = it.value();
            Q_ASSERT(!dbConnection.isNull());
            if(!dbConnection->isOpen())
                dbConnection->open();
            return dbConnection;
        }
    }

    connect(currentThread, &QThread::destroyed,
            this, &DBManager::removeConnectionThread,
            Qt::DirectConnection);

    auto dbConnection = QSharedPointer<QSqlDatabase>::create(
                            QSqlDatabase::addDatabase(_settings.dbType,
                                                      QString("connection_%1").arg(reinterpret_cast<quintptr>(currentThread)))
                            );

    dbConnection->setHostName(_settings.hostName);
    dbConnection->setDatabaseName(_settings.database);
    dbConnection->setPort(_settings.port);
    dbConnection->setUserName(_settings.user);
    dbConnection->setPassword(_settings.password);

    if (!dbConnection->open())
    {
        qDebug() << "-------------";
        qDebug() << dbConnection->drivers();
        qDebug() << dbConnection->lastError().text();
        qDebug() << "-------------";

        Q_ASSERT(false);
    }

    QWriteLocker wLocker(&_rwLock);
    _threadConnections.insert(currentThread, dbConnection);

    return dbConnection;
}

const QSharedPointer<DBWraper> DBManager::getDBWraper()
{
    if (this == nullptr)
        return QSharedPointer<DBWraper>(nullptr);

    const auto currentThread = QThread::currentThread();

    {
        QReadLocker rLocker(&_rwLock);
        auto it = _wrapers.find(currentThread);
        if (it != _wrapers.end())
        {
            auto dbWraper = it.value();
            Q_ASSERT(!dbWraper.isNull());
            return dbWraper;
        }
    }

    const auto dbWrapper = QSharedPointer<DBWraper>::create(getConnection());
    _wrapers.insert(currentThread, dbWrapper);
    return dbWrapper;
}

void DBManager::removeConnectionThread(QObject * obj)
{
    Q_ASSERT(obj != nullptr);
    {
        QReadLocker rLocker(&_rwLock);
        if(!_threadConnections.contains(obj))
            return;
    }

    removeConnection(obj);
}

void DBManager::removeConnection(QObject * obj)
{
    QSharedPointer<QSqlDatabase> dbConnection;
    {
        QReadLocker rLocker(&_rwLock);
        auto it = _threadConnections.find(obj);
        if ( it != _threadConnections.end())
            dbConnection = it.value();
        else
            return;
    }

    Q_ASSERT(!dbConnection.isNull());
    // NOTE: на лине при закрытии здесь segmentation fault
    if(dbConnection->isOpen())
        dbConnection->close();
    dbConnection.clear();

    {
        QWriteLocker wLocker(&_rwLock);
        _threadConnections.remove(obj);
    }

    QSharedPointer<DBWraper> dbWraper;
    {
        QReadLocker rLocker(&_rwLock);
        const auto it = _wrapers.find(obj);
        if ( it != _wrapers.end())
            dbWraper = it.value();
        else
            return;
    }
    {
        QWriteLocker wLocker(&_rwLock);
        _wrapers.remove(obj);
    }
}
