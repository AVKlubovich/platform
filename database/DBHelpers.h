﻿#pragma once


namespace database
{

    class DBWraper;
    typedef QSharedPointer<DBWraper> DBWraperShp;

    class DBHelpers
    {
    private:
        DBHelpers() = delete;
        ~DBHelpers() = delete;

    public:
        static QList<QVariantMap> queryToVariantMap(QSqlQuery& query);
        static QList<QVariant> queryToVariant(QSqlQuery& query);
        static void variantMapToJson(QJsonObject& json, const QVariantMap& data);

        static QList<QSqlQuery> createInsertQueries(const QVariantMap& inData, const DBWraperShp& wraper);
        static QList<QSqlQuery> createInsertQueriesPerTable(const QVariantList& inData, const DBWraperShp& wraper);
    };

}
