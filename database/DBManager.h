﻿#pragma once

#include "../utils/BaseClasses/Singleton.h"


namespace database
{

    class DBWraper;

    class DBManager : public QObject, public utils::Singleton<DBManager>
    {
        Q_OBJECT
        friend class utils::Singleton<DBManager>;
        friend class QSharedPointer<DBManager>;

    public:
        struct DBSettings
        {
            QString dbType;
            QString hostName;
            QString database;
            int port = 0;
            QString user;
            QString password;
        };

    private:
        DBManager();
    public:
        ~DBManager();

        void initSettings(const DBSettings & settings);
        const QSharedPointer<QSqlDatabase> getConnection();
        const QSharedPointer<DBWraper> getDBWraper();

    private slots:
        void removeConnectionThread(QObject * obj);

    private:
        void removeConnection(QObject * obj);

    private:
        QMap<QObject*, QSharedPointer<QSqlDatabase>> _threadConnections;
        QMap<QObject*, QSharedPointer<DBWraper>> _wrapers;
        DBSettings _settings;
        QReadWriteLock _rwLock;
    };

}
