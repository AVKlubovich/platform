#include "Common.h"
#include "WebRequestList.h"

#include "WebRequest.h"


#include "../utils/BaseClasses//MetaTypeRegistration.h"


RegisterMetaType( QSharedPointer<network::WebRequestList> );


namespace network
{

    WebRequestList::WebRequestList()
        : _locker( QReadWriteLock::Recursive )
        , _isInProgress( false )
    {
    }

    WebRequestList::~WebRequestList()
    {
        release();
    }

    void WebRequestList::setProgressState( bool isInProgress )
    {
        QWriteLocker lock( &_locker );
        Q_ASSERT( _isInProgress != isInProgress );
        _isInProgress = isInProgress;
    }

    bool WebRequestList::addRequest( const QSharedPointer<WebRequest>& request )
    {
        QWriteLocker lock( &_locker );
        if ( _isInProgress )
            return false;

        _requests << request;
        _isDone << false;

        const auto index = _requests.size() - 1;

        auto callback = [this, index]()
        {
            // ReSharper disable once CppDeclarationHidesLocal
            QWriteLocker lock( &_locker );

            _isDone[index] = true;
            for ( auto isDone : _isDone )
                if ( !isDone )
                    return;
            if ( _callback )
                _callback();
        };
        request->setCallback( callback );

        return true;
    }

    bool WebRequestList::setCallback( const std::function<void()>& callback )
    {
        QReadLocker lock( &_locker );
        if ( _isInProgress )
            return false;

        _callback = callback;
        return true;
    }

    void WebRequestList::release()
    {
        _callback = nullptr;
    }

    QList<QSharedPointer<WebRequest>> WebRequestList::requests() const
    {
        QReadLocker lock( &_locker );
        return _requests;
    }

}
