#pragma once



namespace network
{

    class WebRequest;
    class WebRequestManager;

 
    class WebRequestList
    {
        friend class QSharedPointer<WebRequestList>;
        friend class WebRequestManager;

    public:
        ~WebRequestList();

        bool addRequest( const QSharedPointer< WebRequest >& request );
        bool setCallback( const std::function< void() >& callback );
        void release();

        QList< QSharedPointer< WebRequest > > requests() const;

    private:
        WebRequestList();
        Q_DISABLE_COPY( WebRequestList );

        void setProgressState( bool isInProgress );

        QList< QSharedPointer< WebRequest > > _requests;
        QList< bool > _isDone;
        std::function< void() > _callback;
        mutable QReadWriteLock _locker;
        bool _isInProgress;
    };

}

Q_DECLARE_METATYPE(QSharedPointer<network::WebRequestList>);
