#pragma once



namespace network
{

    class WebRequest
    {
        friend class QSharedPointer<WebRequest>;

        explicit WebRequest(const QString& commandName);
        Q_DISABLE_COPY(WebRequest);

    public:
        ~WebRequest();

        void setUrl(const QUrl& url);
        QUrl url() const;

        void setArguments( const QVariantMap& arguments );
        void setCallback( const std::function< void() >& callback );
        void release();

        QByteArray compile( const QString& login = QString(), const QString& password = QString() ) const;
        QString lastRequest() const;
        QString lastRequesEncrypted() const;

        void setReply( const QByteArray& data );
        void unpackGzip();
        const QByteArray& reply() const;
        void runCallback();

        void setEncrypt(bool encrypt);


    private:
        QUrl _url;
        const QString _commandName;
        QVariantMap _args;
        QByteArray _reply;
        std::function<void()> _callback;
        mutable QString _request;
        mutable QString _requestEncrypted;
        bool _encrypt;
    };

    typedef QSharedPointer<WebRequest> WebRequestShp;

}

Q_DECLARE_METATYPE(QSharedPointer<network::WebRequest>);
