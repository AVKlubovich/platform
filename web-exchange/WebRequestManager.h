#pragma once

#include "utils/BaseClasses/Singleton.h"


namespace network
{

    class WebRequest;
    class WebRequestList;


    class WebRequestManager
            : public QObject
            , public utils::LazySingleton<WebRequestManager>
    {
        friend class QSharedPointer<WebRequestManager>;
        friend class utils::LazySingleton<WebRequestManager>;

        Q_OBJECT

    signals:
        void initialized();

    public:
        ~WebRequestManager() = default;

    public slots:
        void init();
    public:
        bool isInitialized() const;
        void setCredentials(const QString& login, const QString& password);
        void setUrl(const QString& url);
        void setSingleUrl(const QString& url);

        void sendRequest(const QSharedPointer<WebRequest>& webRequest);
        void sendRequestCurrentThread(const QSharedPointer<WebRequest>& webRequest);
        bool sendRequestList(const QSharedPointer<WebRequestList>& webRequestList);

    private slots:
        void sendRequestInternal(const QVariant& wrapper);
        void sendRequestInternal(const QSharedPointer<WebRequest>& webRequest);
        void onReplyFinished();
        void onReplyError(QNetworkReply::NetworkError code);

    private:
        WebRequestManager();
        void funcOnReplyFinished(QNetworkReply * reply, QByteArray dataReply = "");

        QPointer<QNetworkAccessManager> _manager;
        bool _isInitialized = false;
        QString _url;
        QString _singleUrl;
        QString _login;
        QString _password;

        QMap<QNetworkReply *, QSharedPointer<WebRequest>> _requests;
    };

    typedef QSharedPointer<WebRequestManager> WebRequestManagerShp;

}
