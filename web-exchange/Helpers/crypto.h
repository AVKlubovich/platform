﻿#pragma once


namespace network
{

    class CryptoWeb
    {
    public:
        static std::string decrypt(std::string input);
        static std::string encrypt(const std::string& input);

    private:
        CryptoWeb() = delete;
        ~CryptoWeb() = delete;

        static char decode_char(const std::string & cc);
        static std::string encode_char(char c);
    };

}
