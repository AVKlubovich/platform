#include "crypto.h"

using namespace std;
using namespace network;

// NOTE: Кодирует символ
std::string CryptoWeb::encode_char(char c)
{
    int i = 0;
    int len = 64; // 3 + 58-49 + 90-64 + 122-96;
    if (c > len - 1)
    {
        c = abs((double)(len - c));
        i++;
    }

    char let;
    string ar = "()0";
    if (c <= 2) let = ar[(int)c];
    else if (c <= 11) let = c + 47;
    else if (c <= 37) let = c - 11 + 64;
    else if (c <= 63) let = c - 37 + 96;

    string ret = to_string(i);
    ret += c;
    ret += let;

    return ret;
}

// NOTE: Декодирование символа
char CryptoWeb::decode_char(const std::string & cc)
{
    int len = 64; // 3 + 58-49 + 90-64 + 122-96;
    map<int, int> ar;
    ar.insert(pair<int, int> (40, 0));
    ar.insert(pair<int, int> (41, 1));
    ar.insert(pair<int, int> (48, 2));

    string d = "";
    d += cc[0];

    int i = atoi(d.c_str());
    char c = cc[1];

    if ((int) c >= 97) c = c + 37 - 96;
    else if (c >= 65) c = c + 11 - 64;
    else if (c >= 50) c = c - 47;
    else if (ar.find((int) c) != ar.end()) c = ar[(int)c];

    return i > 0 ? c + 64 : c;
}

// NOTE: Кодирование строки
std::string CryptoWeb::encrypt( const std::string& input )
{
    string output = "";
    int length = input.length();
    for (int i = 0; i < length; i++)
    {
        string c = encode_char(input[i]); // символ в нашем списке

        string d = "";
        d += c[0];

        output += c[2];
        if (atoi(d.c_str()) > 0) output += c[0];
    }
    return output;
}

// NOTE: Декодирование строки
std::string CryptoWeb::decrypt( std::string input )
{
    input += "q";
    string output = "";
    vector<string> chars;

    int length = input.size();
    int offset = 0;

    // парсим строку на символы
    while (offset < length - 1)
    {
        string c = input.substr(offset, 2);

        if (c[1] == '1') offset += 1;
        else c[1] = '0';

        string pb = "";
        pb += c[1];
        pb += c[0];

        chars.push_back(pb);
        offset += 1;
    }

    // расшифровываем символы
    for (int i = 0; i < chars.size(); i++)
    {
        string c = chars[i];
        char char_dec = decode_char(c); // символ в нашем списке

        output += char_dec;
    }
    return output;
}
