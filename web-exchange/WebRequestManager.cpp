#include "Common.h"
#include "WebRequestManager.h"

#include "WebRequest.h"
#include "WebRequestList.h"


using namespace network;

WebRequestManager::WebRequestManager()
    : QObject(nullptr)
{
}

bool WebRequestManager::isInitialized() const
{
    return _isInitialized;
}

void WebRequestManager::setCredentials(const QString& login, const QString& password)
{
    _login = login;
    _password = password;
}

void WebRequestManager::setUrl(const QString& url)
{
    _url = url;
}

void WebRequestManager::setSingleUrl(const QString &url)
{
    _singleUrl = url;
}

void WebRequestManager::sendRequest(const QSharedPointer<WebRequest>& webRequest)
{
    if (!_singleUrl.isEmpty())
    {
        webRequest->setUrl(_singleUrl);
        _singleUrl.clear();
    }
    else
    {
        webRequest->setUrl(QUrl(_url));
    }

    if (QThread::currentThread() != thread())
    {
        QVariant wrapper;
        wrapper.setValue(webRequest);

        const auto result = QMetaObject::invokeMethod(this, "sendRequestInternal", Q_ARG(QVariant, wrapper));
        Q_ASSERT(result);
        return;
    }

    sendRequestInternal(webRequest);
}

void WebRequestManager::sendRequestCurrentThread(const QSharedPointer<WebRequest>& webRequest)
{
    Q_ASSERT(!webRequest.isNull());
    auto data = webRequest->compile(_login, _password);

    QUrl url;
    if (!_singleUrl.isEmpty())
    {
        url = _singleUrl;
        _singleUrl.clear();
    }
    else
    {
        if (_url.isEmpty())
            url = webRequest->url();
        else
            url = _url;
    }

    QNetworkRequest request(url);

    qDebug() << "WebRequest, url = " << url << "\n";

    request.setRawHeader("Accept-Encoding", "gzip, deflate");
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(data.size()));

    QNetworkAccessManager manager;

//    connect(&manager, &QNetworkAccessManager::finished, [this](QNetworkReply* repl)
//    {
//        QByteArray data = repl->readAll();
//        qDebug() << "[FINISH]" << data;
//        funcOnReplyFinished(repl, data);
//        repl->deleteLater();
//        return;
//    });

    auto reply = manager.post(request, data);

    QJsonObject log;
    log["request"] = webRequest->lastRequest();
    log["body"] = webRequest->lastRequesEncrypted();
    log["url"] = url.toString();

    qDebug() << "WebRequest, log = " << log << "\n";

    _requests[reply] = webRequest;

    if (!reply->isFinished() && reply->error() == QNetworkReply::NoError)
    {
        QEventLoop loop;
        connect(&manager, &QNetworkAccessManager::finished, this, [&loop](QNetworkReply*) {
            loop.quit();
        });
        QTimer::singleShot(5000, &loop, SLOT(quit()));
        loop.exec();
    }

    if (reply->error() == QNetworkReply::NoError)
    {
        qDebug() << __FUNCTION__ << "no error";
        QByteArray data = reply->readAll();
        funcOnReplyFinished(reply, data);
    }
    else
    {
        qDebug() << __FUNCTION__ << "error" << reply->error();
        onReplyError(reply->error());
    }

    reply->deleteLater();
}

bool WebRequestManager::sendRequestList(const QSharedPointer<WebRequestList>& webRequestList)
{
    if (webRequestList->requests().isEmpty())
    {
        return false;
    }
    for (const auto& request : webRequestList->requests())
    {
        sendRequest(request);
    }
    return true;
}

void WebRequestManager::sendRequestInternal(const QVariant& wrapper)
{
    auto webRequest = wrapper.value<QSharedPointer<WebRequest>>();
    sendRequestInternal(webRequest);
}

void WebRequestManager::sendRequestInternal(const QSharedPointer<WebRequest>& webRequest)
{
    Q_ASSERT(!webRequest.isNull());
    auto data = webRequest->compile(_login, _password);
    const auto url = webRequest->url();

    QNetworkRequest request(url);

    request.setRawHeader("Accept-Encoding", "gzip, deflate");
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(data.size()));

    auto reply = _manager->post(request, data);

    QJsonObject log;
    log["request"] = webRequest->lastRequest();
    log["body"] = webRequest->lastRequesEncrypted();
    log["url"] = url.toString();

    _requests[reply] = webRequest;

    connect(reply, &QNetworkReply::finished, this, &WebRequestManager::onReplyFinished);
    connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &WebRequestManager::onReplyError);
}

void WebRequestManager::onReplyFinished()
{
    auto reply = qobject_cast<QNetworkReply *>(sender());
    funcOnReplyFinished(reply);

    reply->deleteLater();
}

void WebRequestManager::funcOnReplyFinished(QNetworkReply * reply, QByteArray dataReply)
{
    Q_ASSERT(reply != nullptr);
    if (reply == nullptr)
    {
        qDebug() << __FUNCTION__ << "Reply is null";
    }

    auto it = _requests.find(reply);
    Q_ASSERT(it != _requests.end());
    if (it == _requests.end())
    {
        qDebug() << __FUNCTION__ << "Request do not found";
    }
    auto request = it.value();

    QByteArray data = reply->readAll();
    if (data.isEmpty())
     data = dataReply;

    qDebug() << "reply data" << data;

    if (!data.isEmpty())
    {
        request->setReply(data);

        const auto headers = reply->rawHeaderPairs();
        for (const auto& header : headers)
        {
            if (QString::fromLatin1(header.first) == "Content-Encoding" &&
                QString::fromLatin1(header.second).toLower() == "gzip")
            {
                request->unpackGzip();
            }
        }
    }

    if (!data.isEmpty())
    {
        const auto jsonReply = QJsonDocument::fromJson(request->reply()).object();

        QJsonObject log;
        log["request"] = request->lastRequest();
        log["body"] = request->lastRequesEncrypted();
        log["reply"] = jsonReply;
        log["url"] = request->url().toString();

        qDebug() << "WebRequest, log = " << log << "\n";
    }

    request->runCallback();

    _requests.erase(it);
}

void WebRequestManager::onReplyError(QNetworkReply::NetworkError code)
{
    auto reply = qobject_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply != nullptr);

    auto it = _requests.find(reply);
    Q_ASSERT(it != _requests.end());
    auto request = it.value();

    static const auto enumIndex = QNetworkReply::staticMetaObject.indexOfEnumerator("NetworkError");
    static const auto enumerator = QNetworkReply::staticMetaObject.enumerator(enumIndex);

    const auto message = QString(enumerator.valueToKey(code));

    QJsonObject log;
    log["request"] = request->lastRequest();
    log["body"] = request->lastRequesEncrypted();
    log["error"] = message;

    qDebug() << "WebRequest, log = " << log << "\n";
}

void WebRequestManager::init()
{
    _manager = new QNetworkAccessManager(this);

    _isInitialized = true;
    emit initialized();
}
