#include "Common.h"
#include "WebRequest.h"

#include "Helpers/crypto.h"
#include "Helpers/qcompressor.h"

#include "../utils/BaseClasses/MetaTypeRegistration.h"

RegisterMetaType(QSharedPointer<network::WebRequest>)


using namespace network;

WebRequest::WebRequest( const QString& commandName )
    : _commandName( commandName )
    , _encrypt(false)
{
}

WebRequest::~WebRequest()
{
    release();
}

void WebRequest::setUrl(const QUrl& url)
{
    Q_ASSERT( url.isValid() );
    _url = url;
}

QUrl WebRequest::url() const
{
    return _url;
}

void WebRequest::setArguments( const QVariantMap& arguments )
{
    _args = arguments;
}

void WebRequest::setCallback( const std::function<void()>& callback )
{
    _callback = callback;
}

void WebRequest::release()
{
    _callback = nullptr;
}

QByteArray WebRequest::compile( const QString& login, const QString& password ) const
{
    Q_ASSERT( !_commandName.isEmpty() );
    QMap< QString, QString > params;

    params["name"] = login;
    params["pass"] = password;
    params["type_query"] = _commandName;

    for ( auto it = _args.begin(); it != _args.end(); ++it )
    {
        const auto& key = it.key();
        const auto& value = it.value().toStringList().join( ',' );

        params[key] = value;
    }

    QStringList request;

    for (auto it = params.begin(); it != params.end(); ++it)
    {
        request << QString("%1=%2")
                   .arg(QString(QUrl::toPercentEncoding(it.key())))
                   .arg(QString(QUrl::toPercentEncoding(it.value())));
    }

    _request = request.join( '&' );
    qDebug() << "_request: " << _request;

    if (_encrypt)
    {
        std::string encrypted = CryptoWeb::encrypt(_request.toStdString());
        Q_ASSERT(!encrypted.empty());
        encrypted = "*=" + encrypted;

        _requestEncrypted = encrypted.c_str();
        qDebug() << _requestEncrypted;

        return QByteArray(encrypted.c_str());
    }
    else
    {
        return _request.toUtf8();
    }
}

QString WebRequest::lastRequest() const
{
    return _request;
}

QString WebRequest::lastRequesEncrypted() const
{
    return _requestEncrypted;
}

void WebRequest::setReply( const QByteArray& data )
{
    _reply = data;
}

void WebRequest::unpackGzip()
{
    QByteArray uncompressed;
    QCompressor::gzipDecompress(_reply, uncompressed);
    //uncompressed = qCompress(_reply);
    _reply = uncompressed;
}

const QByteArray& WebRequest::reply() const
{
    return _reply;
}

void WebRequest::runCallback()
{
    if ( _callback )
        _callback();
}

void WebRequest::setEncrypt(bool encrypt)
{
    _encrypt = encrypt;
}
