#pragma once


namespace progress_indicator
{

    /*!
        \class ProgressIndicator
        \brief The ProgressIndicator class lets an application display a progress indicator to show that a lengthy task is under way.

        Progress indicators are indeterminate and do nothing more than spin to show that the application is busy.
        \sa QProgressBar
    */
    class ProgressIndicator : public QWidget
    {
        Q_OBJECT
        Q_PROPERTY(int delay READ animationDelay WRITE setAnimationDelay)
        Q_PROPERTY(bool displayedWhenStopped READ isDisplayedWhenStopped WRITE setDisplayedWhenStopped)
        Q_PROPERTY(QColor color READ color WRITE setColor)

    public:
        ProgressIndicator(QWidget* parent = nullptr);

        /*! Returns the delay between animation steps.
            \return The number of milliseconds between animation steps. By default, the animation delay is set to 40 milliseconds.
            \sa setAnimationDelay
         */
        qint32 animationDelay() const { return _delay; }

        /*! Returns a Boolean value indicating whether the component is currently animated.
            \return Animation state.
            \sa startAnimation stopAnimation
         */
        bool isAnimated() const;

        /*! Returns a Boolean value indicating whether the receiver shows itself even when it is not animating.
            \return Return true if the progress indicator shows itself even when it is not animating. By default, it returns false.
            \sa setDisplayedWhenStopped
         */
        bool isDisplayedWhenStopped() const;

        /*! Returns the color of the component.
            \sa setColor
          */
        const QColor& color() const { return _color; }

        virtual QSize sizeHint() const;
        qint32 heightForWidth(qint32 w) const;

        void setSizeWidgetDefault(quint32 w = 100, quint32 h = 100);

    public slots:
        /*! Starts the spin animation.
            \sa stopAnimation isAnimated
         */
        void startAnimation();

        /*! Stops the spin animation.
            \sa startAnimation isAnimated
         */
        void stopAnimation();

        /*! Sets the delay between animation steps.
            Setting the \a delay to a value larger than 40 slows the animation, while setting the \a delay to a smaller value speeds it up.
            \param delay The delay, in milliseconds.
            \sa animationDelay
         */
        void setAnimationDelay(qint32 delay);

        /*! Sets whether the component hides itself when it is not animating.
           \param state The animation state. Set false to hide the progress indicator when it is not animating; otherwise true.
           \sa isDisplayedWhenStopped
         */
        void setDisplayedWhenStopped(bool state);

        /*! Sets the color of the components to the given color.
            \sa color
         */
        void setColor(const QColor& color);

    protected:
        virtual void timerEvent(QTimerEvent *event);
        virtual void paintEvent(QPaintEvent *event);

    private:
        qint16 _angle;
        qint32 _timerId;
        qint32 _delay;
        bool _displayedWhenStopped;
        QColor _color;
    };

}
