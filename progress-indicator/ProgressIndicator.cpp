#include "Common.h"
#include "ProgressIndicator.h"


using namespace progress_indicator;

ProgressIndicator::ProgressIndicator(QWidget* parent)
    : QWidget(parent)
    , _angle(0)
    , _timerId(-1)
    , _delay(40)
    , _displayedWhenStopped(false)
    , _color(Qt::black)
{
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setFocusPolicy(Qt::NoFocus);
}

bool ProgressIndicator::isAnimated () const
{
    return (_timerId != -1);
}

void ProgressIndicator::setDisplayedWhenStopped(bool state)
{
    _displayedWhenStopped = state;

    update();
}

bool ProgressIndicator::isDisplayedWhenStopped() const
{
    return _displayedWhenStopped;
}

void ProgressIndicator::startAnimation()
{
    _angle = 0;

    if (_timerId == -1)
        _timerId = startTimer(_delay);
}

void ProgressIndicator::stopAnimation()
{
    if (_timerId != -1)
        killTimer(_timerId);

    _timerId = -1;

    update();
}

void ProgressIndicator::setAnimationDelay(qint32 delay)
{
    if (_timerId != -1)
        killTimer(_timerId);

    _delay = delay;

    if (_timerId != -1)
        _timerId = startTimer(_delay);
}

void ProgressIndicator::setColor(const QColor & color)
{
    _color = color;

    update();
}

QSize ProgressIndicator::sizeHint() const
{
    return QSize(20, 20);
}

qint32 ProgressIndicator::heightForWidth(qint32 w) const
{
    return w;
}

void ProgressIndicator::setSizeWidgetDefault(quint32 w, quint32 h)
{
    setFixedSize(w, h);
}

void ProgressIndicator::timerEvent(QTimerEvent * /*event*/)
{
    _angle = (_angle + 30) % 360;

    update();
}

void ProgressIndicator::paintEvent(QPaintEvent * /*event*/)
{
    if (!_displayedWhenStopped && !isAnimated())
        return;

    const auto width = qMin(this->width(), this->height());

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    const auto outerRadius = static_cast<qint32>((width - 1) * 0.5);
    const auto innerRadius = static_cast<qint32>((width - 1) * 0.5 * 0.38);

    const auto capsuleHeight = outerRadius - innerRadius;
    const auto capsuleWidth  = static_cast<qint32>((width > 32 ) ? capsuleHeight * .23 : capsuleHeight * .35);
    const auto capsuleRadius = static_cast<qint32>(capsuleWidth / 2);

    for (int i = 0; i < 12; i++)
    {
        auto& color = _color;
        color.setAlphaF(1.0f - (i / 12.0f));
        p.setPen(Qt::NoPen);
        p.setBrush(color);
        p.save();
        p.translate(rect().center());
        p.rotate(_angle - i * 30.0f);
        p.drawRoundedRect(static_cast<int>(-capsuleWidth * 0.5), -(innerRadius + capsuleHeight), capsuleWidth, capsuleHeight, capsuleRadius, capsuleRadius);
        p.restore();
    }
}
