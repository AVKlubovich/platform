#pragma once


namespace utils
{
    class ThreadPool;
    class Logger;
}

namespace database
{
    class DBManager;
}

namespace permissions
{
    class PermissionManager;
    typedef QSharedPointer<PermissionManager> PermissionManagerShp;
}

namespace core
{

    class Server;
    class DBManager;
    class RequestHandlerFactory;
    
    class Core
    {
    public:
        Core();
        ~Core();

        bool init();
        bool setHandlerFactory(const QSharedPointer<RequestHandlerFactory>& factory);

        QSharedPointer<Server> server() const;

    private:
        Q_DISABLE_COPY(Core);
        bool initLogger();
        bool initServer();
        bool initDBManager();
        bool initPermissions();
        bool initCommandFactory();
        bool initWebManager();

    private:
        QSharedPointer<utils::Logger> _logger;
        QSharedPointer<Server> _server;

        permissions::PermissionManagerShp _permissionManager;
    };

}
