#include "Common.h"
#include "Core.h"

#include "../../utils/Settings/Settings.h"
#include "../../utils/Settings/SettingsFactory.h"
#include "../../utils/Logging/Logger.h"
#include "../../utils/Logging/Devices/DebuggerDevice.h"
#include "../../utils/Logging/Devices/ConsoleDevice.h"
#include "../../utils/Logging/Devices/FileDevice.h"

#include "Server/Server.h"

#include "../../database/DBManager.h"

#include "../../permissions//PermissionManager.h"
#include "../../permissions/PermissionDownloader.h"
#include "../../permissions/UserPermission.h"

#include "Commands/CommandFactory.h"

#include "../../web-exchange/WebRequestManager.h"


using namespace core;

Core::Core()
{
    QDir::setCurrent(QCoreApplication::applicationDirPath());
}

Core::~Core()
{
    network::WebRequestManager::releaseSingleton();
}

bool Core::init()
{
    if (!initLogger())
    {
        qWarning() << "Could not initialize logger";
        return false;
    }

    if (!initServer())
    {
        qWarning() << "Could not initialize server";
        return false;
    }

    if (!initDBManager())
    {
        qWarning() << "Could not initialize dbmanager";
        return false;
    }

    if (!initPermissions())
    {
        qWarning() << "Could not initialize users permissions";
        return false;
    }

    if (!initCommandFactory())
    {
        qWarning() << "Could not initialize command factory";
        return false;
    }

    if (!initWebManager())
    {
        qWarning() << "Could not initialize web manager";
        return false;
    }

    qDebug() << "Core is initialize";
    return true;
}

bool Core::setHandlerFactory(const QSharedPointer<RequestHandlerFactory>& factory)
{
    if (!_server.isNull())
    {
        _server->setHandlerFactory(factory);
        qDebug() << "handler factory is created";
        return true;
    }
    return false;
}

QSharedPointer<Server> Core::server() const
{
    Q_ASSERT( !_server.isNull() );
    return _server;
}

bool Core::initLogger()
{
    // TODO: http://134.17.26.128:8080/browse/OKK-125
    return true;

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Log");

    _logger = QSharedPointer<utils::Logger>::create();

    const auto loggerOptions = QSharedPointer<utils::LoggerMessages::Options>::create();
    loggerOptions->timerInterval = settings["FlushInterval"].toLongLong();
    if (!_logger->init(loggerOptions))
        return false;

    // FileDevice
    const auto fileOptions = QSharedPointer<utils::FileDevice::FileOptions>::create();
    fileOptions->maxSize = settings["MaxSize"].toLongLong();
    fileOptions->prefixName = settings["PrefixName"].toString(); //"okk_server.log";
    fileOptions->directory = settings["Dir"].toString();

    if (!_logger->addDevice(fileOptions))
        return false;

    // DebuggerDevice
    const auto debuggerDevice = QSharedPointer<utils::DebuggerDevice::DebuggerOptions>::create();

    if (!_logger->addDevice(debuggerDevice))
        return false;


    // ConsoleDevice
    const auto consoleDevice = QSharedPointer<utils::ConsoleDevice::ConsoleOptions>::create();

    if (!_logger->addDevice(consoleDevice))
        return false;


    settings.endGroup();
    qDebug() << "initLogger";
    return true;
}

bool Core::initServer()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();

    auto server = QSharedPointer<Server>::create();

    Server::Options options;
    options.balancerOptions.fastThreadCount = settings.value("MultiThreading/FastThreadCound").toInt();
    options.balancerOptions.longThreadCount = settings.value("MultiThreading/LongThreadCound").toInt();
    options.fastCgiOptions.path = settings.value("FastCGI/ConnectionString").toString().toLatin1();
    options.fastCgiOptions.backlog = settings.value("FastCGI/Backlog").toInt();
    options.fastCgiOptions.maxConnections = settings.value("FastCGI/MaxJobs").toInt();

    server->start(options);
    if (!server->isStarted())
        return false;

    _server = server;
    qDebug() << "initServer";
    return true;
}

bool Core::initDBManager()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("DBManager");

    database::DBManager::DBSettings dbSettings;
    dbSettings.hostName = settings["Host"].toString();
    dbSettings.database = settings["Name"].toString();
    dbSettings.dbType = settings["Type"].toString();
    dbSettings.port = settings["Port"].toInt();
    dbSettings.user = settings["User"].toString();
    dbSettings.password = settings["Password"].toString();
    auto& dbManager = database::DBManager::instance();
    dbManager.initSettings(dbSettings);

    qDebug() << "initDBManager";
    return true;
}

bool Core::initPermissions()
{
//    NOTE: temporary solution for PhotoValidator
//    return true;

    qDebug() << "initPermissions";
//    _permissionManager = engine::PermissionManagerShp::create();
//    engine::PermissionDownloader pDownloader;
//    const auto result = pDownloader.downloadePermissions(_permissionManager);

    const auto result = permissions::UserPermission::instance().setUsersPermissions();
    return result;
}

bool Core::initCommandFactory()
{
    qDebug() << "initCommandFactory";
    CommandFactory::instance().registerSignatures();
    return true;
}

bool Core::initWebManager()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("ApiGeneral");

    const auto login = settings["Login"].toString();
    const auto password = settings["Password"].toString();
    const auto url = settings["Url"].toString();

    network::WebRequestManager::createSingleton();
    const auto& webManager = network::WebRequestManager::instance();
    webManager->setCredentials(login, password);
    webManager->setUrl(url);

    webManager->init();

    qDebug() << "initWebManager";

    return webManager->isInitialized();
}
