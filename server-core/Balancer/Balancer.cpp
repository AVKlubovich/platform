#include "Common.h"
#include "Balancer.h"


namespace core
{

    Balancer::Balancer( const Options& options, QObject* parent )
        : QObject( parent )
        , _options( options )
    {
        initThreadPool();
    }

    Balancer::~Balancer()
    {
    }

    void Balancer::startJob( QRunnable* runnable, bool isFastJob )
    {
        Q_ASSERT( runnable != nullptr );

        if ( !isFastJob )
        {
            _longPool.start( runnable );
            return;
        }

        auto started = false;
        
        started = _fastPool.tryStart( runnable );
        if ( !started )
            started = _longPool.tryStart( runnable );

        if ( !started )
            _fastPool.start( runnable );
    }

    void Balancer::initThreadPool()
    {
        const auto idealThreadCount = QThread::idealThreadCount();

        if ( _options.fastThreadCount <= 0 )
        {
            _options.fastThreadCount = idealThreadCount * 3 / 2;
            return;
        }

        if ( _options.longThreadCount <= 0 )
        {
            _options.longThreadCount = idealThreadCount / 2;
            return;
        }

        _fastPool.setMaxThreadCount( _options.fastThreadCount );
        _longPool.setMaxThreadCount( _options.longThreadCount );
    }

}
