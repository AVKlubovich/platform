#pragma once


namespace core
{
    
    class Balancer
        : public QObject
    {
        Q_OBJECT

    public:
        struct Options
        {
            int fastThreadCount;
            int longThreadCount;
        };

    public:
        explicit Balancer( const Options& options, QObject *parent = nullptr );
        ~Balancer();

        void startJob( QRunnable *runnable, bool isFastJob = true );

    private:
        void initThreadPool();

    private:
        Options _options;
        QThreadPool _fastPool;
        QThreadPool _longPool;
    };

}
