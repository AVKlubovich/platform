#pragma once

#include "../../network-core/Headers/Headers.h"
#include "../../network-core/RequestsManager/Response.h"
#include "Request/RequestHandler.h"


namespace network
{
    class Packet;
    class Headers;
}

namespace core
{

    class FcgxRequest;
    class RequestHandlerFactory;


    class FcgxJob
        : public QRunnable
    {
    public:
        explicit FcgxJob( const QSharedPointer< FcgxRequest > fcgxRequest,
            const QSharedPointer< RequestHandlerFactory >& handlerFactory,
            bool logTraffic);
        ~FcgxJob();

        void run() override;

    private:
        bool parseRequest( network::Packet& packet ) const;
        bool sendResponce( const network::Headers& headers, const QByteArray& body ) const;
        void sendResponse(const network::Response& response) const;

        void sendResponceSlow(const RequestHandlerShp& handler);

    private:
        QSharedPointer< FcgxRequest > _fcgxRequest;
        QSharedPointer< RequestHandlerFactory > _handlerFactory;
        QPointer< QThread > _thread;
        QReadWriteLock _locker;
        const bool _logTraffic;
    };

}
