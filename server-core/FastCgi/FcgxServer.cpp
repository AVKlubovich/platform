#include "Common.h"
#include "FcgxServer.h"

#include "../3rdParty/fastcgi/fcgi_config.h"
#include "../3rdParty/fastcgi/include/fcgiapp.h"

#include "FcgxRequest.h"
#include "FcgxJob.h"

#include "ComplexityResolver/ComplexityResolver.h"
#include "Balancer/Balancer.h"


namespace core
{

    FcgxServer::FcgxServer( const Options& options )
        : _options( options )
        , _semaphore( options.maxConnections )
    {
        FCGX_Init();
    }

    FcgxServer::~FcgxServer()
    {
    }

    bool FcgxServer::init()
    {
        Q_ASSERT( !_isInitialized );

        _socket = FCGX_OpenSocket( _options.path.data(), _options.backlog );
        if ( _socket < 0 )
            return false;

        if ( _options.complexityResolver.isNull() )
            _options.complexityResolver = QSharedPointer<ComplexityResolver>::create( "REQUEST_URI" );

        start();
        _isInitialized = true;
        return true;
    }

    bool FcgxServer::isInitialized() const
    {
        return _isInitialized;
    }

    // ReSharper disable once CppMemberFunctionMayBeConst
    void FcgxServer::close()
    {
        Q_ASSERT( isInitialized() );
        FCGX_ShutdownPending();

        // TODO: close socket
        _isInitialized = false;
        _socket = -1;
    }

    void FcgxServer::setBalancer( const QSharedPointer<Balancer>& balancer )
    {
        _balancer = balancer;
    }

    void FcgxServer::setHandlerFactory( const QSharedPointer<RequestHandlerFactory>& handlerFactory )
    {
        _handlerFactory = handlerFactory;
    }

    void FcgxServer::run()
    {
        Q_ASSERT( isInitialized() );
        Q_ASSERT( !_balancer.isNull() );
        Q_ASSERT_X( _balancer->thread() != QThread::currentThread(), "",
            "Balancer must not be in FcgxServer thread, becase FcgxServer thread does not run event loop" );

        for ( ;; )
        {
            auto request = QSharedPointer< FcgxRequest >::create( _socket );
            Q_ASSERT( request->isInitialized() );

            const auto acceptResult = FCGX_Accept_r( &request->request() );
            if ( acceptResult < 0 )
                break;

            auto complexityResolver = _options.complexityResolver;
            const QByteArray complexity = FCGX_GetParam( complexityResolver->key().constData(), request->request().envp );
            const auto isComplex = _options.complexityResolver->isComplex( complexity );

            request->setComplexity( isComplex ? FcgxRequest::Slow : FcgxRequest::Fast );

            auto job = new FcgxJob( request, _handlerFactory, _options.logTraffic );
            _balancer->startJob( job, !isComplex );
        }
    }

}
