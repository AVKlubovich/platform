#include "Common.h"
#include "FcgxRequest.h"

#include "../../utils/BaseClasses/MetaTypeRegistration.h"



namespace core
{

    FcgxRequest::FcgxRequest( int socket )
    {
        const auto initResult = FCGX_InitRequest( &_request, socket, 0 );
        _isInitialized = initResult == 0;
    }

    FcgxRequest::~FcgxRequest()
    {
        finish();
    }

    FCGX_Request& FcgxRequest::request()
    {
        return _request;
    }

    const FCGX_Request& FcgxRequest::request() const
    {
        return _request;
    }

    bool FcgxRequest::isInitialized() const
    {
        return _isInitialized;
    }

    void FcgxRequest::setComplexity( Complexity complexity )
    {
        _complexity = complexity;
    }

    void FcgxRequest::finish()
    {
        if ( !_isInitialized )
            return;

        FCGX_Finish_r( &_request );
        _isInitialized = false;
    }

}
