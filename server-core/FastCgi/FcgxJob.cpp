#include "Common.h"
#include "FcgxJob.h"

#include "../3rdParty/fastcgi/fcgi_config.h"
#include "../3rdParty/fastcgi/include/fcgiapp.h"

#include "../../network-core/Headers/Headers.h"
#include "../../network-core/Headers/HeaderParser.h"
#include "../../network-core/Packet/Packet.h"
#include "../../network-core/Packet/PacketParser.h"
#include "../../network-core/Packet/PacketBuilder.h"

#include "Request/RequestHandler.h"
#include "Responce/Responce.h"
#include "../../network-core/RequestsManager/ResponseServerError.h"
#include "Commands/Command.h"

#include "FcgxRequest.h"


namespace
{

    const int maxBodyLength = 100 * 1024 * 1024;    // 100 mb
    const auto maxHeaderCount = 1000;

}


using namespace core;

FcgxJob::FcgxJob( const QSharedPointer<FcgxRequest> fcgxRequest,
    const QSharedPointer< RequestHandlerFactory >& handlerFactory,
    bool logTraffic)
    : _fcgxRequest( fcgxRequest )
    , _handlerFactory( handlerFactory )
    , _logTraffic(logTraffic)
{
    Q_ASSERT( !_fcgxRequest.isNull() );
    Q_ASSERT( !_handlerFactory.isNull() );
    setAutoDelete( true );
}

FcgxJob::~FcgxJob()
{
}

void FcgxJob::run()
{
    QElapsedTimer timer;
    timer.start();

    auto onExit = [&]
    {
        auto &request = _fcgxRequest->request();
        FCGX_Finish_r( &request );

        _locker.lockForWrite();
        _thread = nullptr;
        _locker.unlock();
    };

    _locker.lockForWrite();
    _thread = QThread::currentThread();
    _locker.unlock();

    network::Packet packet;
    const auto parseResult = parseRequest( packet );
    if ( !parseResult )
    {
        onExit();
        return;
    }

    //TODO: replace this with 'response' below
    auto responce = QSharedPointer< Responce >::create();
    //QSharedPointer<Responce> response;

    core::Command::Context context;
    context._packet = packet;
    context._responce = responce;

    Q_ASSERT( !_handlerFactory.isNull() );
    auto handler = _handlerFactory->create(context);

    if (handler->commandType() == Command::TypeCommand::ErrorCommand)
    {
        onExit();
    }

    if (handler->commandType() == Command::TypeCommand::SlowCommand)
    {
        auto callback = [this, handler]()
        {
            this->sendResponceSlow(handler);
        };

        handler->setCallback(callback);
        handler->processSlow();

        return;
    }

    try
    {
        QSharedPointer<network::Response> response = handler->process();
        //if (!processResult)
        //{
        //    onExit();
        //    return;
        //}

        network::PacketBuilder builder;
        if (response)
        {
            builder.setHeaders(packet.headers());
            builder.setBody(response->toVariant());
        }
        else
        {
            //old style
            builder.setHeaders(responce->headers());
            builder.setBody(responce->body());
        }

        sendResponce(builder.headers(), builder.body());
    }
    catch (const std::runtime_error & ex)
    {
        QString errMsg = QString("Server error: %1").arg(ex.what());
        network::ResponseServerError errResponse(errMsg, QSharedPointer<network::Request>());
        sendResponse(errResponse);
    }

    onExit();
}

bool FcgxJob::parseRequest( network::Packet& packet ) const
{
    network::HeaderParser headerParser;
    auto &request = _fcgxRequest->request();

    QString logMessage;
    if (_logTraffic)
    {
        logMessage.reserve(4096);
        logMessage = "Request:\n";
    }

    auto p = request.envp;
    for (auto i = 0; *p != nullptr && i < maxHeaderCount; i++, p++)
    {
        if (_logTraffic)
            logMessage += QString(*p) + "\n";
        headerParser.parse(*p);
    }

    const auto headers = headerParser.headers();
    auto bodyPresent = false;
    const auto bodySize = headers.header( "CONTENT_LENGTH" ).toInt( &bodyPresent );
    if ( !bodyPresent || bodySize > maxBodyLength )
        return false;

    QByteArray body;
    body.resize( bodySize );
    char *bodyChar = body.data();
    const auto readBodyResult = FCGX_GetStr( body.data(), bodySize, request.in );
    if ( readBodyResult != bodySize )
        return false;

    network::PacketParser packetParser;
    packetParser.setHeaders( headerParser.headers() );
    packetParser.setBody( body );

    if (_logTraffic)
    {
        logMessage += QString("\n") + QJsonDocument::fromJson(body).toJson();
        qDebug().noquote() << logMessage;
    }

    packet = packetParser.packet();

    return true;
}

bool FcgxJob::sendResponce(const network::Headers& headers, const QByteArray& body) const
{
    // MAP: remake logger
    // qDebug() << "Send response: " << body;

    QString logMessage;
    if (_logTraffic)
    {
        logMessage.reserve(4096);
        logMessage = "Reply:\n";
        logMessage += body;
        logMessage += "\n";
    }

    auto &request = _fcgxRequest->request();

    // Headers
    const auto& headersMap = headers.allHeaders();
    for ( auto it = headersMap.constBegin(); it != headersMap.constEnd(); ++it )
    {
        const auto& key = it.key();
        const auto& value = it.value();

        if (request.out == nullptr)
            continue;

        // TODO: check errors
        FCGX_PutStr( key.data(), key.size(), request.out );
        FCGX_PutS( ": ", request.out );
        FCGX_PutStr( value.data(), value.size(), request.out );
        FCGX_PutS( "\r\n", request.out );

        if (_logTraffic)
            logMessage += key + ": " + value + "\n";
    }
    FCGX_PutS( "\r\n", request.out );

    if (_logTraffic)
        logMessage += "\n";

    // Body
    FCGX_PutStr( body.data(), body.size(), request.out );

    if (_logTraffic)
    {
        logMessage += QString("\n") + QJsonDocument::fromJson(body).toJson();
        qDebug().noquote() << logMessage;
    }

    return true;
}

void FcgxJob::sendResponse(const network::Response& response) const
{
    auto &request = _fcgxRequest->request();

    QByteArray outData("sendResponse body");

    // Body
    FCGX_PutStr(outData.data(), outData.size(), request.out);
}

void FcgxJob::sendResponceSlow(const RequestHandlerShp& handler)
{
    auto onExit = [&]
    {
        auto &request = _fcgxRequest->request();
        FCGX_Finish_r(&request);

        _locker.lockForWrite();
        _thread = nullptr;
        _locker.unlock();
    };

    _locker.lockForWrite();
    _thread = QThread::currentThread();
    _locker.unlock();

    network::Packet packet;
    const auto parseResult = parseRequest( packet );
    if ( !parseResult )
    {
        onExit();
        return;
    }

    //TODO: replace this with 'response' below
    auto responce = QSharedPointer< Responce >::create();
    //QSharedPointer<Responce> response;

    core::Command::Context context;
    context._packet = packet;
    context._responce = responce;

    network::ResponseShp response = handler->getSlowResponse();

    try
    {
        network::PacketBuilder builder;
        if (response)
        {
            builder.setHeaders(packet.headers());
            builder.setBody(response->toVariant());
        }
        else
        {
            //old style
            builder.setHeaders(responce->headers());
            builder.setBody(responce->body());
        }

        sendResponce(builder.headers(), builder.body());
    }
    catch (const std::runtime_error& ex)
    {
        QString errMsg = QString("Server error: %1").arg(ex.what());
        network::ResponseServerError errResponse(errMsg, QSharedPointer<network::Request>());
        sendResponse(errResponse);
    }

    onExit();
}



/*
CONTENT_TYPE=application/x-www-form-urlencoded
CONTENT_LENGTH=12
SCRIPT_NAME=/
REQUEST_URI=/
DOCUMENT_URI=/
DOCUMENT_ROOT=c:\Nginx\nginx-1.8.0/html
SERVER_PROTOCOL=HTTP/1.1
GATEWAY_INTERFACE=CGI/1.1
SERVER_SOFTWARE=nginx/1.8.0
REMOTE_ADDR=127.0.0.1
REMOTE_PORT=11293
SERVER_ADDR=127.0.0.1
SERVER_PORT=81
SERVER_NAME=localhost
REDIRECT_STATUS=200
HTTP_ACCEPT_ENCODING=gzip, deflate
HTTP_ACCEPT=text/plain; q=0.5, text/html
HTTP_CUSTOMRAWHEADER=CustomRawData
HTTP_CONTENT_TYPE=application/x-www-form-urlencoded
HTTP_CONTENT_LENGTH=12
HTTP_CONNECTION=Keep-Alive
HTTP_ACCEPT_LANGUAGE=ru-RU,en,*
HTTP_USER_AGENT=Mozilla/5.0
HTTP_HOST=localhost:81
*/
