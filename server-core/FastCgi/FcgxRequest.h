#pragma once


#include "../3rdParty/fastcgi/fcgi_config.h"
#include "../3rdParty/fastcgi/include/fcgiapp.h"


namespace core
{
    
    class FcgxRequest
    {
        friend class QSharedPointer< FcgxRequest >;

        Q_GADGET

    public:
        enum Complexity
        {
            Fast,
            Slow,
        };
        //Q_ENUM( Complexity );

    public:
        ~FcgxRequest();

        FCGX_Request& request();
        const FCGX_Request& request() const;
        bool isInitialized() const;
        void setComplexity( Complexity complexity );
        void finish();

    private:
        explicit FcgxRequest( int socket );
        Q_DISABLE_COPY( FcgxRequest );

    private:
        FCGX_Request _request;
        bool _isInitialized = false;
        Complexity _complexity = Slow;
    };

}


Q_DECLARE_METATYPE( QSharedPointer< core::FcgxRequest > );
