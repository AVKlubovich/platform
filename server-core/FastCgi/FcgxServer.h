#pragma once


namespace core
{

    class ComplexityResolver;
    class Balancer;
    class RequestHandlerFactory;


    class FcgxServer
        : public QThread
    {
        Q_OBJECT

    public:
        struct Options
        {
            QByteArray path;
            int backlog;
            int maxConnections;
            bool logTraffic;
            QSharedPointer< ComplexityResolver > complexityResolver;
        };

    public:
        explicit FcgxServer( const Options& options );
        ~FcgxServer();

        bool init();
        bool isInitialized() const;
        void close();
        void setBalancer( const QSharedPointer< Balancer >& balancer );
        void setHandlerFactory( const QSharedPointer< RequestHandlerFactory >& handlerFactory );

        void run() override;

    private:
        Options _options;
        bool _isInitialized = false;
        int _socket = 0;
        QSemaphore _semaphore;
        QSharedPointer< Balancer > _balancer;
        QSharedPointer< RequestHandlerFactory > _handlerFactory;
    };

}
