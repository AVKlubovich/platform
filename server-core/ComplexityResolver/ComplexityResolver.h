#pragma once


namespace core
{
    
    class ComplexityResolver
    {
    public:
        explicit ComplexityResolver( const QByteArray& key );
        virtual ~ComplexityResolver() = default;

        const QByteArray& key() const;
        virtual bool isComplex( const QByteArray& param ) const;

    private:
        const QByteArray _key;
    };

}