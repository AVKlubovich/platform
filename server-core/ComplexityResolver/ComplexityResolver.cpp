#include "Common.h"
#include "ComplexityResolver.h"


namespace core
{

    ComplexityResolver::ComplexityResolver( const QByteArray& key )
        : _key( key )
    {
    }

    const QByteArray& ComplexityResolver::key() const
    {
        return _key;
    }

    bool ComplexityResolver::isComplex( const QByteArray& param ) const
    {
        const auto result = false;
        return result;
    }

}
