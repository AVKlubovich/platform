#pragma once


#include "../FastCgi/FcgxServer.h"
#include "../Balancer/Balancer.h"


namespace core
{

    class FcgxServer;
    class FcgxJob;
    class RequestHandler;
    class RequestHandlerFactory;
    class Balancer;

    
    class Server : public QObject
    {
        Q_OBJECT

    public:
        struct Options
        {
            Balancer::Options balancerOptions;
            FcgxServer::Options fastCgiOptions;
        };

    signals:
        void started();

    public:
        Server();
        ~Server();

        bool isStarted() const;
        void setHandlerFactory( const QSharedPointer< RequestHandlerFactory >& handlerFactory );

    public slots:
        void start( const Options& options );

    private:
        bool _isStarted = false;
        Options _options;
        QSharedPointer< Balancer > _balancer;
        QScopedPointer< FcgxServer > _fcgxServer;
        QSharedPointer< RequestHandlerFactory > _handlerFactory;
        QVector< QSharedPointer< RequestHandler > > _handlers;
        QVector< QSharedPointer< FcgxJob > > _jobs;
    };

}
