#include "Common.h"
#include "Server.h"

#include "FastCgi/FcgxServer.h"
#include "Request/RequestHandler.h"
#include "Request/DefaultHandler.h"
#include "Balancer/Balancer.h"


using namespace core;

Server::Server()
    : QObject( nullptr )
    , _handlerFactory( QSharedPointer< DefaultHandlerFactory >::create() )
{
}

Server::~Server()
{
}

bool Server::isStarted() const
{
    return _isStarted;
}

void Server::setHandlerFactory( const QSharedPointer<RequestHandlerFactory>& handlerFactory )
{
    Q_ASSERT(!handlerFactory.isNull());
    _handlerFactory = handlerFactory;

    if (!_fcgxServer.isNull())
        _fcgxServer->setHandlerFactory(handlerFactory);
}

void Server::start( const Options& options )
{
    Q_ASSERT(!_isStarted);

    _options = options;
    _balancer = QSharedPointer< Balancer >::create( _options.balancerOptions );

    QScopedPointer<FcgxServer> fcgxServer( new FcgxServer( _options.fastCgiOptions ) );

    fcgxServer->setBalancer( _balancer );
    fcgxServer->setHandlerFactory( _handlerFactory );

    const auto fcgxInitResult = fcgxServer->init();
    if (!fcgxInitResult)
        return;

    _fcgxServer.reset( fcgxServer.take() );

    _isStarted = true;
    emit started();
}
