#pragma once

#include "../../network-core/Headers/Headers.h"


namespace core
{
    
    class Responce
    {
    public:
        Responce() = default;
        virtual ~Responce() = default;

        const network::Headers& headers() const;
        void setHeaders( const network::Headers& headers );
        const QVariant& body() const;
        void setBody( const QVariant& body );

    private:
        network::Headers _headers;
        QVariant _body;
    };

}
