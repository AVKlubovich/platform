#include "Common.h"
#include "Responce.h"


namespace core
{

    const network::Headers& Responce::headers() const
    {
        return _headers;
    }

    void Responce::setHeaders( const network::Headers& headers )
    {
        _headers = headers;
    }

    const QVariant& Responce::body() const
    {
        return _body;
    }

    void Responce::setBody( const QVariant& body )
    {
        _body = body;
    }

}
