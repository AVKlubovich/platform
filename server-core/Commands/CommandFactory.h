#pragma once


#include "../../utils/BaseClasses/Singleton.h"

#include "Command.h"
#include "CommandCreator.h"


namespace database
{
    class DBManager;
}

namespace core
{

    class CommandFactory
        : public utils::Singleton<CommandFactory>
    {
        friend class utils::Singleton<CommandFactory>;

    private:
        CommandFactory() = default;

        using CreatorFunction = QSharedPointer<Command>(*)(const Command::Context&);
        using SetSignatureFunction = void(*)(const QString&, bool);

        struct CommandInfo
        {
            QString signature;
            bool useCache;
        };

    public:
        ~CommandFactory() = default;

        template<typename T>
        void registerCommand(const QString& name, bool useCache = true);

        void registerSignatures();

        QStringList commandList() const;
        QString className(const QString& name) const;

        QSharedPointer<Command> create(const QString& name, const Command::Context& newContext) const;

    private:
        QHash<QString, CreatorFunction> _stringCreators;
        QMap<SetSignatureFunction, CommandInfo> _commandsInfo;
        QHash<QString, QString> _classNames;
    };


    template <typename T>
    void CommandFactory::registerCommand( const QString& name, bool useCache )
    {
        static_assert(std::is_base_of<Command, T>::value, "T must be a child of core::Command");
        static_assert(std::is_base_of<CommandCreator<T>, T>::value, "T must be a child of core::CommandCreator<T>");

        Q_ASSERT(name != nullptr);
        Q_ASSERT(!_stringCreators.contains(name));

        _classNames.insert(name, QString(typeid(T).name()));

        CreatorFunction creator = &T::creator;
        _stringCreators.insert(name, creator);

        SetSignatureFunction signatureSetter = &T::setConfiguration;
        const CommandInfo info = { name, useCache };
        _commandsInfo.insert(signatureSetter, info);
    }

}

// Command registration macro
namespace
{

    template<typename T>
    struct CommandAutoRegistrator
    {
        explicit CommandAutoRegistrator(const QString& name, bool useCache = true)
        {
            static_assert(std::is_base_of< ::core::CommandCreator<T>, T>::value, "T must be a child of core::CommandCreator<T>");
            ::core::CommandFactory::instance().registerCommand<T>(name, useCache);
        }

        static CommandAutoRegistrator _inst;
    };

}

#define RegisterCommand(COMMAND, ID)         \
namespace {                                  \
    template<>CommandAutoRegistrator<COMMAND> CommandAutoRegistrator<COMMAND>::_inst(ID); } \


#define RegisterCommandEx(COMMAND, ID, USE_CACHE)       \
namespace {                                             \
    template<>CommandAutoRegistrator<COMMAND> CommandAutoRegistrator<COMMAND>::_inst(ID, USE_CACHE); } \

