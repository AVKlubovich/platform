#pragma once

#include "network-core/Packet/Packet.h"
#include "network-core/RequestsManager/Response.h"


namespace database
{
    class DBManager;
}

namespace core
{

    class Responce;

    class Command
    {
        friend class QSharedPointer<Command>;

    public:
        enum TypeCommand
        {
            FastCommand,
            SlowCommand,
            ErrorCommand = -1
        };

        struct Context
        {
            network::Packet _packet;
            //TODO: replace it with new strong typed network::Response
            QSharedPointer<Responce> _responce;
            //QSharedPointer<network::Response> _response;
        };

    public:
        Command(const Context& newContext);
        virtual ~Command() = default;

        virtual QSharedPointer<network::Response> exec();
        virtual void execSlow(const std::function<void()>& callback);
        virtual network::ResponseShp getSlowResponse();
        virtual TypeCommand type() const;

        virtual bool checkInData(const QVariantMap& inBody);

    public:
        template<typename T, typename ...Args>
        static QSharedPointer<Command> create(Args &&...args);

    protected:
        void sendError(const QString& errorStr, const QString& errorType = QString());
        void sendError(const QString& errorStr, const QString& errorType, const QString& commandType);
        void sendPermissionsUserErorr(const QString& errorStr, const QString& errorType, const QString& commandType);

        template <typename Enum>
        const Enum stringToEnum(const QString & value)
        {
            QMetaEnum metaEnum = QMetaEnum::fromType<Enum>();
            return static_cast<Enum>(metaEnum.keyToValue(value.toUtf8().data()));
        }
        template <typename Enum>
        const QString enumToString(const Enum value)
        {
            QMetaEnum metaEnum = QMetaEnum::fromType<Enum>();
            return QString(metaEnum.valueToKey(static_cast<int>(value)));
        }
        const QDateTime getDBDateTime();

    protected:
        Context _context;
        std::function<void()> _callback;
    };

    template <typename T, typename ... Args>
    QSharedPointer<Command> Command::create(Args&&... args)
    {
        static_assert(std::is_base_of<Command, T>::value, "T must be a child of core::Command");
        auto command = QSharedPointer<T>::create(std::forward<Args>(args)...).template staticCast<Command>();

        return command;
    }

    typedef QSharedPointer<Command> CommandShp;

}
