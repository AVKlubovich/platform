#pragma once

#include "Command.h"


namespace core
{

    class CommandData;
    class CommandFactory;

    enum class Status
    {
        Ok = 1,
        CriticalError = -1,
        ParamsError = -2,
        DBError = -3,
        CloseComplaintError = -4
    };


    template <typename T>
    class CommandCreator
    {
        friend class CommandFactory;

    public:
        CommandCreator() = default;
        virtual ~CommandCreator() = default;

        static QString signature();
        static bool useCache();
        static QJsonObject jsonError(const QString& message = QString(),
                                     const Status status = Status::CriticalError,
                                     const QString& comment = QString());
        static QJsonObject jsonError(const QString& message, const QString& comment);

    private:
        static void setConfiguration(const QString& signatureName, bool useCache = true);

        static QSharedPointer<Command> creator(const Command::Context& newContext);
        static QString _signature;
        static bool _useCache;
    };


    template <typename T>
    QString CommandCreator<T>::_signature;
    template <typename T>
    bool CommandCreator<T>::_useCache;

    template <typename T>
    inline QString CommandCreator<T>::signature()
    {
        return _signature;
    }

    template <typename T>
    inline bool CommandCreator<T>::useCache()
    {
        return _useCache;
    }

    template <typename T>
    QJsonObject CommandCreator<T>::jsonError(const QString& message, const Status status, const QString& comment)
    {
        static_assert(std::is_base_of<CommandData, T>::value, "T must be a child of core::CommandData");

        QJsonObject json;
        json["status"] = static_cast<qint64>(status);
        json["type_command"] = T::signature();
        if (!message.isEmpty())
            json["error"] = message;
        if (!comment.isEmpty())
            json["comment"] = comment;

        return json;
    }

    template <typename T>
    QJsonObject CommandCreator<T>::jsonError(const QString& message, const QString& comment)
    {
        static_assert(std::is_base_of<CommandData, T>::value, "T must be a child of core::CommandData");

        return jsonError(message, Status::CriticalError, comment);
    }

    template <typename T>
    void CommandCreator<T>::setConfiguration(const QString& signatureName, bool useCache)
    {
        _signature = signatureName;
        _useCache = useCache;
    }

    template <typename T>
    QSharedPointer<Command> CommandCreator<T>::creator(const Command::Context& newContext)
    {
        static_assert(std::is_base_of<Command, T>::value, "T must be a child of core::Command");
        auto command = QSharedPointer<T>::create(newContext).template staticCast<Command>();

        return command;
    }

}
