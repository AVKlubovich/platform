#include "Common.h"
#include "CommandFactory.h"

#include "network-core/Packet/Packet.h"


using namespace core;

void CommandFactory::registerSignatures()
{
    for ( auto it = _commandsInfo.begin(); it != _commandsInfo.end(); ++it )
    {
        const auto& info = it.value();
        auto setter = it.key();

        setter( info.signature, info.useCache );
    }
}

QStringList CommandFactory::commandList() const
{
    QStringList list;
    for ( auto it = _stringCreators.begin(); it != _stringCreators.end(); ++it )
        list << it.key();

    return list;
}

QString CommandFactory::className(const QString& name) const
{
    return _classNames[name];
}

QSharedPointer<Command> CommandFactory::create(const QString& name, const Command::Context& newContext) const
{
    auto it = _stringCreators.find( name );
    if ( it == _stringCreators.end() )
        return QSharedPointer<Command>();

    auto creator = it.value();
    auto command = creator(newContext);

    return command;
}
