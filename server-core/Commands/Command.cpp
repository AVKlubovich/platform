#include "Common.h"
#include "Command.h"

#include "utils/BaseClasses/MetaTypeRegistration.h"
#include "network-core/Packet/Packet.h"

#include "server-core/Responce/Responce.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"


using namespace core;

Command::Command(const Context& newContext)
    : _context(newContext)
{
}

QSharedPointer<network::Response> Command::exec()
{
    return QSharedPointer<network::Response>();
}

void Command::execSlow(const std::function<void ()>& callback)
{
    _callback = callback;
}

network::ResponseShp Command::getSlowResponse()
{
    return QSharedPointer<network::Response>();
}

Command::TypeCommand Command::type() const
{
    return FastCommand;
}

bool Command::checkInData(const QVariantMap& inBody)
{
    return true;
}

void Command::sendError(const QString& errorStr, const QString& errorType)
{
    QVariantMap resultMap;
    resultMap["status"] = -1;
    resultMap["error_string"] = errorStr;
    resultMap["error_type"] = errorType;

    QVariantMap head;
    head["status"] = -1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    auto& responce = _context._responce;
    responce->setBody(QVariant::fromValue(result));
}

void Command::sendError(const QString& errorStr, const QString& errorType, const QString& commandType)
{
    QVariantMap resultMap;
    resultMap["status"] = -1;
    resultMap["error_string"] = errorStr;
    resultMap["error_type"] = errorType;

    QVariantMap head;
    head["type"] = commandType;
    head["status"] = -1;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    auto& responce = _context._responce;
    responce->setBody(QVariant::fromValue(result));
}

void Command::sendPermissionsUserErorr(const QString& errorStr, const QString& errorType, const QString& commandType)
{
    QVariantMap resultMap;
    resultMap["status"] = -2;
    resultMap["error_string"] = errorStr;
    resultMap["error_type"] = errorType;

    QVariantMap head;
    head["type"] = commandType;
    head["status"] = -2;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(resultMap);

    auto& responce = _context._responce;
    responce->setBody(QVariant::fromValue(result));
}

const QDateTime Command::getDBDateTime()
{
    QString selectDateTimeQueryStr = QString(
        "SELECT concat(localtimestamp::date, ' ', localtimestamp::time) AS date"
        );

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectDateTimeQuery = wraper->query();
    selectDateTimeQuery.prepare(selectDateTimeQueryStr);

    auto selectDateTimeQueryResult = wraper->execQuery(selectDateTimeQuery);
    if (!selectDateTimeQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << selectDateTimeQuery.lastError();
        //Q_ASSERT(false);
        return QDateTime::currentDateTime();
    }

    auto resultList = database::DBHelpers::queryToVariant(selectDateTimeQuery);

    if(resultList.count() > 0 &&
        resultList.value(0).toMap().contains("date"))
        return resultList.value(0).toMap().value("date").toDateTime();
    else
        return QDateTime::currentDateTime();
}
