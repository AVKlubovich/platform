cmake_minimum_required( VERSION 3.0.2 )

#project name
get_filename_component( CURRENT_DIR_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME )
project( ${CURRENT_DIR_NAME} )

#find packages
find_package( Qt5Core          REQUIRED )
find_package( Qt5Sql           REQUIRED )
find_package( Qt5Network       REQUIRED )

#settings
set( CURRENT_SRC_PATH "${CMAKE_CURRENT_LIST_DIR}" )

#includes
#include( custom-tools )
#include( cotire )

include_directories( "${CURRENT_SRC_PATH}" )
include_directories( "${CURRENT_SRC_PATH}/../common" )

#build file list
file( GLOB_RECURSE H_FILES "${CURRENT_SRC_PATH}/*.h" )
file( GLOB_RECURSE CPP_FILES "${CURRENT_SRC_PATH}/*.cpp" )

set( USER_FILES ${H_FILES} ${CPP_FILES} )

#wrapping
unset( MOC_LIST )
qt5_wrap_cpp( MOC_LIST ${H_FILES} OPTIONS "--no-notes" )

set( GENERATED_FILES ${MOC_LIST} )
set( ALL_FILES ${USER_FILES} ${GENERATED_FILES} )

apply_source_groups( USER_FILES ${CURRENT_SRC_PATH} "Sources" )
apply_source_groups( GENERATED_FILES ${CURRENT_SRC_PATH} "Generated" )

#compiling
add_library( ${PROJECT_NAME} ${ALL_FILES} )

#linking
target_link_libraries( ${PROJECT_NAME} Qt5::Core )
target_link_libraries( ${PROJECT_NAME} Qt5::Sql )
target_link_libraries( ${PROJECT_NAME} Qt5::Network )

#cotire
set_target_properties( ${PROJECT_NAME} PROPERTIES COTIRE_CXX_PREFIX_HEADER_INIT "Common.h" )
set_target_properties( ${PROJECT_NAME} PROPERTIES COTIRE_ADD_UNITY_BUILD FALSE )
cotire( ${PROJECT_NAME} )
