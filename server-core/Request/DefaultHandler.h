#pragma once


#include "RequestHandler.h"


namespace core
{
    
    class DefaultHandler
        : public RequestHandler
    {
    public:
        DefaultHandler(const core::Command::Context& context);
        ~DefaultHandler();

        quint64 commandType() const override;
        QSharedPointer<network::Response> process() override;

        void processSlow() override;
        network::ResponseShp getSlowResponse() override;
    };


    class DefaultHandlerFactory
        : public RequestHandlerFactory
    {
    public:
        DefaultHandlerFactory() = default;
        ~DefaultHandlerFactory() = default;

        QSharedPointer<RequestHandler> create(const core::Command::Context& context) override;
    };

}
