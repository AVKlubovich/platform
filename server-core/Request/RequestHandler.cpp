#include "Common.h"
#include "RequestHandler.h"


using namespace core;

RequestHandler::RequestHandler(const Command::Context& context)
    : _context(context)
{
}

quint64 RequestHandler::commandType() const
{
    if (_command.isNull())
        return Command::TypeCommand::ErrorCommand;
    return _command->type();
}

void RequestHandler::setCallback(const std::function<void ()>& callback)
{
    _callback = callback;
}
