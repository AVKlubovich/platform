#pragma once


#include "network-core/Headers/Headers.h"
#include "network-core/Packet/Packet.h"

#include "server-core/Responce/Responce.h"

#include "server-core/Commands/Command.h"


namespace network
{
    class Packet;
}


namespace core
{

    class RequestHandler
    {
    public:
        RequestHandler(const core::Command::Context& context);
        virtual ~RequestHandler() = default;

        virtual quint64 commandType() const;
        virtual QSharedPointer<network::Response> process() = 0;

        void setCallback(const std::function<void()>& callback);
        virtual void processSlow() = 0;
        virtual network::ResponseShp getSlowResponse() = 0;

    protected:
        core::CommandShp _command;
        core::Command::Context _context;
        std::function<void()> _callback;
    };

    typedef QSharedPointer<RequestHandler> RequestHandlerShp;


    class RequestHandlerFactory
    {
    public:
        RequestHandlerFactory() = default;
        virtual ~RequestHandlerFactory() = default;

        virtual QSharedPointer<RequestHandler> create(const core::Command::Context& context) = 0;
    };


}
