#include "Common.h"
#include "DefaultHandler.h"

#include "Responce/Responce.h"


using namespace core;

DefaultHandler::DefaultHandler(const Command::Context& context)
    : RequestHandler(context)
{
}

DefaultHandler::~DefaultHandler()
{
}

quint64 DefaultHandler::commandType() const
{
    return Command::TypeCommand::FastCommand;
}

QSharedPointer<network::Response> DefaultHandler::process()
{
    _context._responce->setHeaders(_context._packet.headers());
    QVariantMap data =
    {
        { "TestKey", "TestValue" },
        { "Request", _context._packet.body() },
    };
    _context._responce->setBody(data);

    return QSharedPointer<network::Response>();
}

void DefaultHandler::processSlow()
{
    _callback();
}

network::ResponseShp DefaultHandler::getSlowResponse()
{
    return QSharedPointer<network::Response>();
}


QSharedPointer<RequestHandler> DefaultHandlerFactory::create(const Command::Context& context)
{
    return QSharedPointer<DefaultHandler>::create(context);
}
