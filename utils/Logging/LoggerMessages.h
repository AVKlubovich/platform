﻿#pragma once

#include "Devices/BaseDevice.h"


namespace utils
{

    class LoggerMessages : public QObject
    {
        Q_OBJECT

    public:
        class LogMessage
        {
        public:
            LogMessage() = default;

            QtMsgType type;
            MessageLogContext context;
            QString msg;
        };

        class Options : public BaseOptions
        {
            friend class QSharedPointer<Options>;

        private:
            Options() = default;
        public:
            ~Options() override = default;

        public:
            int timerInterval;
        };

    public:
        LoggerMessages();
        ~LoggerMessages();

        bool init(const QSharedPointer<BaseOptions> & options);
        void addMessage(QtMsgType type, const MessageLogContext &context, const QString &msg);
        void close();

    signals:
        void sendMessagesOutput(QList<QSharedPointer<LogMessage>> messages);

    private slots:
        void timout();

    private:
        QSharedPointer<Options> _options;
        QList<QSharedPointer<LogMessage>> _messages;
        QMutex _mutexLock;
        QMutex _mutexIsClose;
        bool _isClose;
        QTimer _timer;
    };

}

