﻿#include "Common.h"
#include "Logger.h"
#include "Devices/ConsoleDevice.h"
#include "Devices/FileDevice.h"
#include "Devices/DebuggerDevice.h"
#include "Devices/BaseDevice.h"


using namespace utils;

Logger * Logger::_current = nullptr;

Logger::Logger() :
    QObject(nullptr)
{
    _current = this;
}

bool Logger::init(const QSharedPointer<BaseOptions> & options)
{
    if (!_loggerMessages.init(options))
        return false;

    Q_ASSERT(QCoreApplication::instance()->thread() == QThread::currentThread());

    connect(&_loggerMessages, &LoggerMessages::sendMessagesOutput, this, &Logger::receiveMessageOutput);
    qInstallMessageHandler(messageOutput);

    return true;
}

bool Logger::addDevice(const QSharedPointer<BaseOptions> & options)
{
    Q_ASSERT(QCoreApplication::instance()->thread() == QThread::currentThread());

    QSharedPointer<BaseDevice> device = deviceFactory(options);
    if (device.isNull())
        return false;

    QSharedPointer<BaseDevice> lastDev;
    for (auto dev : _devices)
    {
        if (typeid(*device.data()) == typeid(*dev.data()))
            lastDev = device;
    }

    if (!lastDev.isNull())
        _devices.removeOne(lastDev);

    _devices.append(device);

    return true;
}

QSharedPointer<BaseDevice> Logger::deviceFactory(const QSharedPointer<BaseOptions> & options)
{
    QSharedPointer<BaseDevice> device;
    if (!options.dynamicCast<FileDevice::FileOptions>().isNull())
        device = QSharedPointer<FileDevice>::create();
    else if (!options.dynamicCast<DebuggerDevice::DebuggerOptions>().isNull())
        device = QSharedPointer<DebuggerDevice>::create();
    else if (!options.dynamicCast<ConsoleDevice::ConsoleOptions>().isNull())
        device = QSharedPointer<ConsoleDevice>::create();
    else
    {
        Q_ASSERT(false);
        return device;
    }

    if (!device->init(options))
    {
        device.clear();
        return device;
    }

    return device;
}

void Logger::messageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    if (!_current)
        return;

    for (QSharedPointer<BaseDevice> dev : _current->_devices)
        dev->write(type, MessageLogContext(context), msg);

    //TODO: does multithreading needed here???
    //_current->_loggerMessages.addMessage(type, MessageLogContext(context), msg);
}

void Logger::receiveMessageOutput(QList<QSharedPointer<LoggerMessages::LogMessage>> messages)
{
    Q_ASSERT(QCoreApplication::instance()->thread() == QThread::currentThread());

    for (const auto msg : messages)
    {
        for (auto dev : _devices)
            dev->write(msg->type, msg->context, msg->msg);
    }

    for (auto dev : _devices)
        dev->flush();
}

Logger::~Logger()
{
    for (auto dev : _devices)
    {
        if (dev.isNull())
            continue;
        dev->flush();
    }
    _current = nullptr;
}
