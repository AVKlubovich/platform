﻿#pragma once


namespace utils
{

    class BaseOptions
    {
    public:
        BaseOptions() = default;
        virtual ~BaseOptions() = default;
    };

}

