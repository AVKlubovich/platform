﻿#include "Common.h"
#include "ConsoleDevice.h"


namespace utils
{

    ConsoleDevice::ConsoleDevice()
        : BaseDevice()
    {
    }

    bool ConsoleDevice::init(const QSharedPointer<BaseOptions> & options)
    {
        _isInitialized = false;

        auto consoleOptions = options.dynamicCast<ConsoleOptions>();
        if (consoleOptions.isNull())
        {
            Q_ASSERT(false);
            return false;
        }
        _options = options;

        _isInitialized = true;
        return _isInitialized;
    }

    void ConsoleDevice::write(QtMsgType type, const MessageLogContext &context, const QString &msg)
    {
        QMutexLocker locker(&_mutexWrite);
        auto dateTime = QDateTime::currentDateTime();
#ifdef _WIN32
        QTextCodec* codec = QTextCodec::codecForName("IBM 866");
        if ( codec )
            fprintf(stdout, "%s\n", codec->fromUnicode(msg).constData());
        else
            fprintf(stdout, "%s\n", msg.toLocal8Bit().constData());
#else
        fprintf(stdout, "%s\n", msg.toUtf8().constData());
#endif
    }

    void ConsoleDevice::flush()
    {
    }

}
