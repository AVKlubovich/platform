﻿#pragma once

#include "BaseDevice.h"


namespace utils
{

    class DebuggerDevice : public BaseDevice
    {
        Q_OBJECT
        friend class QSharedPointer<DebuggerDevice>;

    public:
        class DebuggerOptions : public BaseOptions
        {
            friend class QSharedPointer<DebuggerOptions>;

        private:
            DebuggerOptions() = default;
        public:
            ~DebuggerOptions() override = default;
        };

    private:
        DebuggerDevice();
    public:
        ~DebuggerDevice() override = default;

        bool init(const QSharedPointer<BaseOptions> & options) override;
        void write(QtMsgType type, const MessageLogContext &context, const QString &msg) override;
        void flush() override;
    };

}
