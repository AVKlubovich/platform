﻿#pragma once

#include "BaseDevice.h"


namespace utils
{

    class FileDevice : public BaseDevice
    {
        Q_OBJECT
        friend class QSharedPointer<FileDevice>;

    public:
        class FileOptions : public BaseOptions
        {
            friend class QSharedPointer<FileOptions>;

        public:
            FileOptions();
            virtual ~FileOptions() = default;

        public:
            QString directory;
            QString prefixName;
            qint64 maxSize;
        };

    private:
        FileDevice();
    public:
        ~FileDevice() override = default;

        bool init(const QSharedPointer<BaseOptions> & options) override;
        void write(QtMsgType type, const MessageLogContext &context, const QString &msg) override;
        void flush() override;

    private:
        void createFile();

    private:
        const QString _fileDateTimeFormat = "yyyy-MM-dd_HH-mm-ss-zzz";
        QScopedPointer<QFile> _file;
    };
}

