﻿#include "FileDevice.h"


using namespace utils;

FileDevice::FileOptions::FileOptions()
    : maxSize(-1)
{
}

FileDevice::FileDevice()
    : BaseDevice()
{
}

bool FileDevice::init(const QSharedPointer<BaseOptions> & options)
{
    auto fileOptions = options.dynamicCast<FileOptions>();
    if (fileOptions.isNull() ||
        fileOptions->prefixName.isNull())
    {
        Q_ASSERT(false);
        return false;
    }
    _options = options;

    createFile();

    _isInitialized = true;
    return _isInitialized;
}

void FileDevice::write(QtMsgType type, const MessageLogContext &context, const QString &msg)
{
    QMutexLocker locker(&_mutexWrite);

    if (_file.isNull())
    {
        Q_ASSERT(false);
        return;
    }

    if (!_file->isOpen())
    {
        _file->open(QFile::WriteOnly);
        if (!_file->isOpen())
        {
            Q_ASSERT(false);
            return;
        }
    }

    auto dateTime = QDateTime::currentDateTime();
    QString severity;
    switch (type)
    {
        case QtDebugMsg:
            severity = "debug";
            break;
        case QtWarningMsg:
            severity = "warning";
            break;
        case QtCriticalMsg:
            severity = "critical";
            break;
        case QtFatalMsg:
            severity = "fatal";
            break;
        default:
            severity = "info";
            break;
    }

#ifdef _DEBUG
    _file->write((dateTime.toString(timestampFormat) +
                  QString(" %1: %2 (%3:%4, %5)\n")
                  .arg(severity)
                  .arg(msg)
                  .arg(context.file)
                  .arg(context.line)
                  .arg(context.function)).toUtf8().constData());
#else
    _file->write((dateTime.toString(timestampFormat) +
                  QString(" %1: %2\n")
                  .arg(severity)
                  .arg(msg)).toUtf8().constData());
#endif
}

void FileDevice::createFile()
{
    auto fileOptions = _options.dynamicCast<FileOptions>();
    const QFileInfo info(fileOptions->prefixName);
    QString dir = "";
    if (fileOptions->directory[0] != '/')
    {
        dir = info.absolutePath() + (fileOptions->directory.isEmpty() ? "/" :
                                                                        (fileOptions->directory.mid(0, 2) == "./" ? fileOptions->directory.mid(1) : "/" + fileOptions->directory));
    }
    else
        dir = fileOptions->directory;

    if (!QDir().exists(dir))
    {
        if (!QDir().mkpath(dir))
        {
            Q_ASSERT_X(false, __FUNCTION__, "Log directory do not exists");
            return;
        }
    }

    const auto originalName = info.completeBaseName();
    const auto suffix = info.suffix();
    const auto dateTime = QDateTime::currentDateTime().toString(_fileDateTimeFormat);
    const auto& fileName = QString("%1_%2").arg(originalName).arg(dateTime);
    const auto& fullPath = QString("%1%2.%3").arg(dir).arg(fileName).arg(suffix);

    if (QFileInfo(fullPath).exists())
    {
        Q_ASSERT_X(false, __FUNCTION__, "Log file already exists");
        return;
    }

    if (!_file.isNull() &&
        _file->size() < fileOptions->maxSize)
        return;

    QScopedPointer<QFile> device(new QFile(fullPath));
    if (!device->open(QFile::WriteOnly))
    {
        Q_ASSERT_X(false, __FUNCTION__, "Do not create file to write");
        return;
    }

    _file.reset(device.take());
    _file->moveToThread(thread());
}

void FileDevice::flush()
{
    if (_file.isNull())
        return;

    _file->flush();
    createFile();
}
