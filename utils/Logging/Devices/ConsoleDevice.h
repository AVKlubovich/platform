﻿#pragma once

#include "BaseDevice.h"


namespace utils
{

    class ConsoleDevice : public BaseDevice
    {
        Q_OBJECT
        friend class QSharedPointer<ConsoleDevice>;

    public:
        class ConsoleOptions : public BaseOptions
        {
            friend class QSharedPointer<ConsoleOptions>;

        private:
            ConsoleOptions() = default;
        public:
            ~ConsoleOptions() = default;
        };

    private:
        ConsoleDevice();
    public:
        ~ConsoleDevice() override = default;

        bool init(const QSharedPointer<BaseOptions> & options) override;
        void write(QtMsgType type, const MessageLogContext &context, const QString &msg) override;
        void flush() override;
    };

}
