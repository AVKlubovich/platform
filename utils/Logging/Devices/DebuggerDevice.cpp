﻿#include "Common.h"
#include "DebuggerDevice.h"

#ifdef _MSC_VER
#include <Windows.h>
#endif

namespace utils
{

    DebuggerDevice::DebuggerDevice()
        : BaseDevice()
    {
    }

    bool DebuggerDevice::init(const QSharedPointer<BaseOptions> & options)
    {
        _isInitialized = false;

#ifdef _MSC_VER
        auto debuggerOptions = options.dynamicCast<DebuggerOptions>();
        if (debuggerOptions.isNull())
        {
            Q_ASSERT(false);
            return false;
        }

        _options = options;
#endif
        _isInitialized = true;
        return _isInitialized;
    }

    void DebuggerDevice::write(QtMsgType type, const MessageLogContext &context, const QString &msg)
    {
#ifdef _MSC_VER
        QMutexLocker locker(&_mutexWrite);

        QString severity;
        switch (type)
        {
            case QtDebugMsg:
                severity = "debug";
                break;
            case QtWarningMsg:
                severity = "warning";
                break;
            case QtCriticalMsg:
                severity = "critical";
                break;
            case QtFatalMsg:
                severity = "fatal";
                break;
            default:
                //This is for backward compatibility
                //for Qt 5.5 it is QtInfoMsg
                severity = "info";
                break;
        }

#   ifdef _DEBUG
        OutputDebugString(reinterpret_cast<LPCSTR>(QString("%1(%2): %3: %4\n")
            .arg(context.file)
            .arg(context.line)
            .arg(severity)
            .arg(msg).toLocal8Bit().data()));
#   else
        OutputDebugString(reinterpret_cast<LPCSTR>(QString("%1: %2\n")
            .arg(severity)
            .arg(msg).toLocal8Bit().data()));
#   endif
#endif
    }

    void DebuggerDevice::flush()
    {

    }

}
