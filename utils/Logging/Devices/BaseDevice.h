﻿#pragma once

#include "BaseOptions.h"


namespace utils
{

    struct MessageLogContext
    {
        MessageLogContext() = default;
        MessageLogContext(const QMessageLogContext &old)
            : version(old.version)
            , line(old.line)
            , file(old.file)
            , function(old.function)
            , category(old.category)
        {}

        int version;
        int line;
        QString file;
        QString function;
        QString category;
    };

    class BaseDevice : public QObject
    {
        Q_OBJECT

    public:
        BaseDevice() : QObject() {}
        virtual ~BaseDevice() = default;

        virtual bool init(const QSharedPointer<BaseOptions> & options) = 0;
        virtual void write(QtMsgType type, const MessageLogContext &context, const QString &msg) = 0;
        virtual void flush() = 0;

        bool isInitialized() const
        {
            return _isInitialized;
        }

    public:
        const QString timestampFormat = "yyyy-MM-dd hh:mm:ss:zzz";

    protected:
        QSharedPointer<BaseOptions> _options;
        QMutex _mutexWrite;
        bool _isInitialized = false;
    };

}
