﻿#include "Common.h"
#include "LoggerMessages.h"


using namespace utils;

LoggerMessages::LoggerMessages()
    : QObject(nullptr)
    , _isClose(false)
{
    _timer.setSingleShot(true);
}

LoggerMessages::~LoggerMessages()
{
    close();
}

bool LoggerMessages::init(const QSharedPointer<BaseOptions> & options)
{
    _options = options.dynamicCast<LoggerMessages::Options>();
    if (_options.isNull())
    {
        Q_ASSERT(!_options.isNull());
        return false;
    }

    connect(&_timer, &QTimer::timeout, this, &LoggerMessages::timout);
    _timer.start(_options->timerInterval);

    return true;
}

void LoggerMessages::addMessage(QtMsgType type, const MessageLogContext &context, const QString &msg)
{
    auto message = QSharedPointer<LogMessage>::create();
    message->context = context;
    message->msg = msg;
    message->type = type;

    QMutexLocker locker(&_mutexLock);
    _messages.append(message);
}

void LoggerMessages::close()
{
    QMutexLocker locker(&_mutexIsClose);
    _isClose = true;
}

void LoggerMessages::timout()
{
    QList<QSharedPointer<LogMessage>> messages;
    {
        QMutexLocker locker(&_mutexLock);
        messages = _messages;
        _messages.clear();
    }
    emit sendMessagesOutput(messages);

    {
        QMutexLocker mlocker(&_mutexIsClose);
        if (_isClose)
            return;
    }
    _timer.start(_options->timerInterval);
}
