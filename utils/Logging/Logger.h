﻿#pragma once

#include "Devices/BaseOptions.h"
#include "LoggerMessages.h"


namespace utils
{

    class Logger : public QObject
    {
        Q_OBJECT

    public:
        Logger();
        ~Logger();

        bool addDevice(const QSharedPointer<BaseOptions> & options);
        bool init(const QSharedPointer<BaseOptions> & options);

    private:
        static void messageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);
        QSharedPointer<BaseDevice> deviceFactory(const QSharedPointer<BaseOptions> & options);

    private slots:
        void receiveMessageOutput(QList<QSharedPointer<LoggerMessages::LogMessage>> messages);

    private:
        QList<QSharedPointer<BaseDevice>> _devices;
        LoggerMessages _loggerMessages;
        static Logger *_current;
    };

}

