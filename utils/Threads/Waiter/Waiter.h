﻿#pragma once



namespace utils
{

    class Waiter
        : public QObject
    {
        Q_OBJECT

    private:
        using ObjectMap = QMap < QObject *, int >;

    public:
        explicit Waiter( QObject *parent = nullptr );
        ~Waiter();

        void waitAll();

        template <class T, typename S>
        void subscribe( T object, S signal );

    private slots:
        void onDone();
        void onObjectDestroyed();

    private:
        void testAndRelease();

    private:
        QEventLoop _loop;
        ObjectMap _connections;

    public:
        template< class T, typename S >
        static void subscribe( Waiter *waiter, T object, S signal );

        template< class T, typename S, typename L >
        static void waitOne( T object, S signal, const L& launcher );
    };


    template < class T, typename S >
    void Waiter::subscribe( T object, S signal )
    {
        subscribe( this, object, signal );
    }

    template <class T, typename S>
    void Waiter::subscribe( Waiter* waiter, T object, S signal )
    {
        Q_ASSERT( waiter != nullptr );
        Q_ASSERT( object != nullptr );

        auto& connections = waiter->_connections;
        auto it = connections.find( object );
        if ( it == connections.end() )
            it = connections.insert( object, 0 );
        ++it.value();

        connect( object, signal, waiter, &Waiter::onDone );
        connect( object, &QObject::destroyed, waiter, &Waiter::onObjectDestroyed );
    }

    template <class T, typename S, typename L>
    void Waiter::waitOne( T object, S signal, const L& launcher )
    {
        Waiter waiter;
        Waiter::subscribe( &waiter, object, signal );
        launcher();
        waiter.waitAll();
    }

}
