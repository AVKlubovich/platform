﻿#include "Common.h"
#include "Waiter.h"


namespace utils
{

    Waiter::Waiter( QObject *parent )
        : QObject( parent )
    {
    }

    Waiter::~Waiter()
    {
    }

    void Waiter::waitAll()
    {
        if ( !_connections.isEmpty() )
            _loop.exec();
    }

    void Waiter::onDone()
    {
        auto obj = sender();
        auto it = _connections.find( obj );
        const auto exists = ( it != _connections.end() );

        Q_ASSERT( exists );
        if ( !exists )
            return;

        auto& counter = it.value();
        Q_ASSERT( counter > 0 );
        counter--;
        if ( counter == 0 )
            _connections.erase( it );

        testAndRelease();
    }

    void Waiter::onObjectDestroyed()
    {
        auto obj = sender();
        auto it = _connections.find( obj );
        const auto exists = ( it != _connections.end() );

        Q_ASSERT( exists );
        if ( !exists )
            return;

        _connections.erase( it );
        testAndRelease();
    }

    void Waiter::testAndRelease()
    {
        if ( _connections.isEmpty() )
            _loop.quit();
    }

}
