﻿#pragma once


namespace utils
{

    class Settings
    {
    public:
        struct Options
        {
            QString path;
            bool forceSync;
        };

    public:
        explicit Settings(const Options& options);
        Settings(const Settings& rhs);
        ~Settings();

        bool isValid() const;

        Settings& operator=(const Settings& rhs);

        void sync() const;
        QVariant value(const QString& key, const QVariant& defaultValue = QVariant()) const;
        void setValue(const QString& key, const QVariant& value);
        void setValues(const QVariantMap& values);
        Settings& operator=(const QVariantMap& values);
        QVariant operator[](const QString& key) const;

        void beginGroup(const QString& prefix);
        void endGroup();

    private:
        const QScopedPointer<QSettings>& storage() const;

    private:
        QScopedPointer<QSettings> _storage;
        const Options _options;
    };

}
