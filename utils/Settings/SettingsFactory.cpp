﻿#include "Common.h"
#include "SettingsFactory.h"


using namespace utils;

void SettingsFactory::registerSettings(const QString& key, const Settings::Options& options)
{
    QWriteLocker lock(&_locker);
    _settingsOptions[key] = options;
}

Settings SettingsFactory::settings(const QString& key) const
{
    QReadLocker lock(&_locker);

    const auto it = _settingsOptions.constFind(key);
    if (it == _settingsOptions.constEnd())
        return Settings( {} );

    const auto& options = it.value();
    return Settings(options);
}

Settings SettingsFactory::currentSettings() const
{
    QReadLocker lock(&_locker);

    const auto it = _settingsOptions.constFind(_currentSettingsKey);
    if (it == _settingsOptions.constEnd())
        return Settings( {} );

    const auto& options = it.value();
    return Settings(options);
}

QString SettingsFactory::currentSettingsName() const
{
    return _currentSettingsKey;
}

bool SettingsFactory::setCurrentSettings(const QString& key) const
{
    QReadLocker lock(&_locker);

    const auto it = _settingsOptions.constFind(key);
    if (it == _settingsOptions.constEnd())
        return false;

    _currentSettingsKey = key;
    return true;
}

QStringList SettingsFactory::availableSettings() const
{
    QReadLocker lock(&_locker);

    const auto keys = _settingsOptions.keys();
    return keys;
}
