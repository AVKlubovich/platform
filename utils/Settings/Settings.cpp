﻿#include "Common.h"
#include "Settings.h"


using namespace utils;

Settings::Settings(const Options& options)
    : _options(options)
{
    _storage.reset(new QSettings(_options.path, QSettings::IniFormat));
}

Settings::Settings(const Settings& rhs)
    : _options(rhs._options)
{
    _storage.reset(new QSettings(_options.path, QSettings::IniFormat));
}

Settings::~Settings()
{
}

bool Settings::isValid() const
{
    Q_ASSERT(storage()->thread() == QThread::currentThread());
    const auto status = storage()->status();
    return status == QSettings::NoError;
}

Settings& Settings::operator=(const Settings& rhs)
{
    if (this == &rhs)
        return *this;

    *const_cast<Options *>(&_options) = rhs._options;
    _storage.reset(new QSettings(_options.path, QSettings::IniFormat));

    return *this;
}

void Settings::sync() const
{
    if (_options.forceSync)
        storage()->sync();
}

QVariant Settings::value(const QString& key, const QVariant& defaultValue) const
{
    Q_ASSERT(storage()->thread() == QThread::currentThread());

    sync();
    const auto value = storage()->value(key, defaultValue);

    return value;
}

void Settings::setValue(const QString& key, const QVariant& value)
{
    Q_ASSERT(storage()->thread() == QThread::currentThread());

    storage()->setValue(key, value);
    sync();
}

void Settings::setValues(const QVariantMap& values)
{
    Q_ASSERT(_storage->thread() == QThread::currentThread());

    for (auto it = values.begin(); it != values.end(); ++it)
    {
        const auto key = it.key();
        const auto value = it.value();

        if (!storage()->contains(key))
            storage()->setValue(key, value);
    }
    sync();
}

Settings& Settings::operator=(const QVariantMap& values)
{
    setValues(values);
    return *this;
}

QVariant Settings::operator[](const QString& key) const
{
    return storage()->value(key);
}

void Settings::beginGroup(const QString& prefix)
{
    storage()->beginGroup(prefix);
}

void Settings::endGroup()
{
    storage()->endGroup();
}

const QScopedPointer<QSettings>& Settings::storage() const
{
    Q_ASSERT(!_storage.isNull());
    return _storage;
}
