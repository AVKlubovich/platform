﻿#pragma once

#include "../BaseClasses/Singleton.h"

#include "Settings.h"


namespace utils
{

    class SettingsFactory
        : public Singleton<SettingsFactory>
    {
        friend class Singleton<SettingsFactory>;

    private:
        SettingsFactory() = default;
    public:
        ~SettingsFactory() = default;

        void registerSettings(const QString& key, const Settings::Options& options);

        Settings settings(const QString& key) const;
        Settings currentSettings() const;
        QString currentSettingsName() const;
        bool setCurrentSettings(const QString& key) const;
        QStringList availableSettings() const;

    private:
        QMap<QString, Settings::Options> _settingsOptions;
        mutable QReadWriteLock _locker;

        mutable QString _currentSettingsKey;
    };

}
