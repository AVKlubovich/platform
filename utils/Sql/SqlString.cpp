﻿#include "Common.h"
#include "SqlString.h"


using namespace sql_utils;

void SqlString::addSqlParameters(const QList<quint64> & ids,
                                 const QString & table_name,
                                 const QString & table_name_termination,
                                 QStringList & sqlQueries,
                                 QList<QVariant> & sqlParameters)
{
//    if ( (!ids.count() && (table_name_termination != "_company")) &&
//         (!ids.count() && (table_name_termination != "_source")) )
//        return;

#ifdef QT_DEBUG
    QString stringIndexs = "";
    if (table_name_termination == "_target")
        stringIndexs = " " + table_name + table_name_termination + ".id IN (%1)";
    else if (table_name_termination == "_company" && ids.count())
        stringIndexs = " " + table_name + ".id" + table_name_termination + " IN (0, %1)";
    else
        stringIndexs = " " + table_name + ".id" + table_name_termination + " IN (%1)";
#else
    QString stringIndexs = "";
    if (table_name_termination == "_target")
        stringIndexs = " " + table_name + table_name_termination + ".id IN (%1)";
    else
        stringIndexs = " " + table_name + ".id" + table_name_termination + " IN (%1)";
#endif

    QStringList listStringRealIndexModel;
    for (int i = 0; i < ids.count(); i++)
    {
        listStringRealIndexModel << "?";
        sqlParameters << ids[i];
    }
    const auto stringRealIndexModel = listStringRealIndexModel.join(", ");
    stringIndexs = stringIndexs.arg(stringRealIndexModel);

    sqlQueries.append(stringIndexs);
}

void SqlString::addSqlParametersReview(const QList<quint64> &ids,
                                       const QString &table_name,
                                       const QString &table_name_termination,
                                       QStringList &sqlQuerys,
                                       QList<QVariant> &sqlParameters)
{
//    if ((ids.count() == 0) && (table_name_termination != "_company"))
//        return;

    QString stringIndexs = " " + table_name + ".id" + table_name_termination + " IN (%1)";

    QStringList listStringRealIndexModel;
    for (int i = 0; i < ids.count(); i++)
    {
        listStringRealIndexModel << "?";
        sqlParameters << ids[i];
    }
    const auto stringRealIndexModel = listStringRealIndexModel.join(", ");
    stringIndexs = stringIndexs.arg(stringRealIndexModel);

    sqlQuerys.append(stringIndexs);
}

void SqlString::addSqlLike(const QString& like, const QString& table, QStringList& sqlQueries, QList<QVariant>& sqlParameters)
{
    if (like.isEmpty())
        return;

    sqlQueries.append(QString("(%1.id LIKE ?) OR (%2.id_order LIKE ?)").arg(table).arg(table));
    sqlParameters.append("%" + like + "%");
    sqlParameters.append("%" + like + "%");
}

void SqlString::addSqlLikeReview(const QString& like, const QString& table, QStringList& sqlQueries, QList<QVariant>& sqlParameters)
{
    if (like.isEmpty())
        return;

    sqlQueries.append(QString("(%1.id LIKE ?) OR (%2.id_order LIKE ?)").arg(table).arg(table));
    sqlParameters.append("%" + like + "%");
    sqlParameters.append("%" + like + "%");
}

void SqlString::addSqlLike(const QString& like, const QString& table, const QString& field, QStringList& sqlQueries, QList<QVariant>& sqlParameters)
{
    if (like.isEmpty())
        return;

    sqlQueries.append(QString("(%1.%2 LIKE ?)").arg(table).arg(field));
    sqlParameters.append("%" + like + "%");
}
