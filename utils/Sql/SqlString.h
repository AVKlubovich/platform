#pragma once


namespace sql_utils
{

    class SqlString
    {
    public:
        SqlString() = default;
        ~SqlString() = default;

        void addSqlParameters(const QList<quint64> & ids,
            const QString & table_name,
            const QString & table_name_termination,
            QStringList & sqlQueries,
            QList<QVariant> & sqlParameters);
        void addSqlParametersReview(const QList<quint64> & ids,
                                    const QString & table_name,
                                    const QString & table_name_termination,
                                    QStringList &sqlQuerys,
                                    QList<QVariant> & sqlParameters);
        void addSqlLike(const QString& like, const QString& table, QStringList& sqlQueries, QList<QVariant>& sqlParameters);
        void addSqlLikeReview(const QString& like, const QString& table, QStringList& sqlQueries, QList<QVariant>& sqlParameters);
        void addSqlLike(const QString& like, const QString& table, const QString& field, QStringList& sqlQueries, QList<QVariant>& sqlParameters);
    };

    static const QString stringSqlDate = "strftime('%d ', date_create) ||"
                                         " CASE WHEN strftime('%m', date_create) = '01' THEN 'Январь'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '02' THEN 'Февраль'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '03' THEN 'Март'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '04' THEN 'Апрель'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '05' THEN 'Май'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '06' THEN 'Июнь'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '07' THEN 'Июль'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '08' THEN 'Август'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '09' THEN 'Сентябрь'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '10' THEN 'Октябрь'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '11' THEN 'Ноябрь'"
                                         " ELSE CASE WHEN strftime('%m', date_create) = '12' THEN 'Декабрь'"
                                         " END END END END END END END END END END END END"
                                         " || strftime(' %Y %H:%M:%S', date_create)"
                                         ;
}
