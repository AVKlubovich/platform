﻿#pragma once



template < typename T >
class SharedOnly
{
    friend class QSharedPointer<T>;

    virtual QSharedPointer<T> clone() const;

protected:
    SharedOnly() = default;
    virtual ~SharedOnly() = default;

private:
    Q_DISABLE_COPY( SharedOnly );

public:
    template < typename ... Args >
    static QSharedPointer<T> create( Args&&... args );
};



template < typename T >
QSharedPointer<T> SharedOnly<T>::clone() const
{
    Q_ASSERT( false );
    return QSharedPointer<T>();
}

template < typename T >
template < typename ... Args >
QSharedPointer<T> SharedOnly<T>::create( Args&&... args )
{
    const auto object = QSharedPointer< T >::create( std::forward<Args>( args )... );
    return object;
}
