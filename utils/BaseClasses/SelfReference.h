﻿#pragma once


namespace utils
{
    
    template < typename T >
    class SelfReference
    {
    public:
        virtual ~SelfReference() = default;

    protected:
        SelfReference() = default;
        QSharedPointer< T > self() const;

    private:
        Q_DISABLE_COPY( SelfReference );
        QWeakPointer< T > _self;

    public:
        template< typename ...Args >
        static QSharedPointer< T > createSelfRef( Args &&...args );

        static void initSelf( const QSharedPointer<T>& object );
    };


    template <typename T>
    QSharedPointer<T> SelfReference<T>::self() const
    {
        auto strongRef = _self.toStrongRef();
        Q_ASSERT( !strongRef.isNull() );    // Should never happend
        return strongRef;
    }

    template <typename T>
    template <typename ... Args>
    QSharedPointer<T> SelfReference<T>::createSelfRef(Args&&... args)
    {
        auto object = QSharedPointer< T >::create( std::forward<Args>( args )... );
        object->_self = object.toWeakRef();
        return object;
    }

    template <typename T>
    void SelfReference<T>::initSelf( const QSharedPointer<T>& object )
    {
        object->_self = object.toWeakRef();
    }

}
