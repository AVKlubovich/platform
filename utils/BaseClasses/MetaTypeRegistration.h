﻿#pragma once


namespace
{

    template<typename T>
    struct MetaTypeRegistrator
    {
        MetaTypeRegistrator()
        {
            qRegisterMetaType<T>();
        }

        static MetaTypeRegistrator _inst;
    };

}


#define RegisterMetaType( TYPE )                                \
namespace {                                                     \
    template<>MetaTypeRegistrator<TYPE> MetaTypeRegistrator<TYPE>::_inst; \
}
