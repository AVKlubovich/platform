﻿#include "Common.h"

#include "RunGuard.h"


namespace
{
	
    QString generateKeyHash( const QString& key, const QString& salt )
    {
        QByteArray data;

        data.append( key.toUtf8() );
        data.append( salt.toUtf8() );
        data = QCryptographicHash::hash( data, QCryptographicHash::Sha1 ).toHex();

        return data;
    }

}


namespace utils
{
    
    RunGuard::RunGuard( const QString& key )
        : _memLockKey( generateKeyHash( key, "_memLockKey" ) )
        , _sharedmemKey( generateKeyHash( key, "_sharedmemKey" ) )
        , _sharedMem( _sharedmemKey )
        , _memLock( _memLockKey, 1 )
    {
        _memLock.acquire();
        {
            QSharedMemory fix( _sharedmemKey );    // Fix for *nix: http://habrahabr.ru/post/173281/
            fix.attach();
        }
        _memLock.release();
    }

    RunGuard::~RunGuard()
    {
        release();
    }

    bool RunGuard::isAnotherRunning()
    {
        if ( _sharedMem.isAttached() )
            return false;

        _memLock.acquire();
        const bool isRunning = _sharedMem.attach();
        if ( isRunning )
            _sharedMem.detach();
        _memLock.release();

        return isRunning;
    }

    bool RunGuard::tryToRun()
    {
        if ( isAnotherRunning() )	// Extra check
            return false;

        _memLock.acquire();
        const bool result = _sharedMem.create( sizeof( quint64 ) );
        _memLock.release();
        if ( !result )
        {
            release();
            return false;
        }

        return true;
    }

    void RunGuard::release()
    {
        _memLock.acquire();
        if ( _sharedMem.isAttached() )
            _sharedMem.detach();
        _memLock.release();
    }

}