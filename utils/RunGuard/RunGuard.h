﻿#pragma once


namespace utils
{
    
    class RunGuard
    {

    public:
        explicit RunGuard( const QString& key );
        ~RunGuard();

        bool isAnotherRunning();
        bool tryToRun();
        void release();

    private:
        Q_DISABLE_COPY( RunGuard )

        const QString _memLockKey;
        const QString _sharedmemKey;

        QSharedMemory _sharedMem;
        QSystemSemaphore _memLock;
    };


}
